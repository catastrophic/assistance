package deezer

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/catastrophic/assistance/fs"
)

var (
	arl = ""
	sid = ""
)

func TestDeezer(t *testing.T) {
	fmt.Println("+ Testing deezer...")
	check := assert.New(t)

	//logthis.SetLevel(logthis.VERBOSEST)

	fmt.Println("Testing getting album info.")
	d := New(arl, sid)

	a, err := d.GetRelease(302127)
	check.Nil(err)
	check.Equal("Discovery", a.Title)
	check.Equal("724384960650", a.Upc)

	fmt.Println("Testing getting tracks IDs from and album ID.")
	ids, err := d.GetTrackIDsFromAlbumID(302127)
	check.Nil(err)
	check.Equal(14, len(ids))
	check.Equal(3135553, ids[0])

	if arl != "" && sid != "" {
		fmt.Println("Using private info, fetching cover URL.")
		coverURL, err := d.GetCover(107281512)
		check.Nil(err)
		check.Equal("https://cdns-images.dzcdn.net/images/cover/3385cebe8a8b27f96e2c159f09d314d3/1500x1500.jpg", coverURL)
		check.Nil(fs.DownloadFile("cover.jpg", coverURL))
		check.True(fs.FileExists("cover.jpg"))
		os.Remove("cover.jpg")

		a, err = d.GetRelease(85717802)
		check.Nil(err)
		check.Equal("French Chanson", a.Genres.Data[0].Name)

		// with contributors
		aj, err := d.GetAjaxAlbumInfo(119031092)
		check.Nil(err)
		//fmt.Println(aj)
		fmt.Println(aj.Results.Data[0].SongContributors)
		// without contributors
		aj, err = d.GetAjaxAlbumInfo(4443541)
		check.Nil(err)
		//fmt.Println(aj)
		fmt.Println(aj.Results.Data[0].SongContributors)

		s, err := d.SearchArtist("mattiel")
		check.Nil(err)
		/*for i := 0; i < int(s.Results.Artist.Count); i++ {
			fmt.Println(i, s.Results.Artist.Data[i].ArtName, s.Results.Artist.Data[i].ArtID)
		}*/
		check.Equal("12803797", s.Results.Artist.Data[0].ArtID)

		// 399 radiohead // 4771479 juniore // 12803797 mattiel
		discog, err := d.GetDiscographyInfo(12803797)
		check.Nil(err)

		// Type == 0 -> single
		// Type == 1 -> album or EP
		// type == 3 -> splits
		// type == 3 -> EP
		check.Equal(12803797, int(discog.Results.ArtID))
		//	fmt.Println(discog.Results.Data[0])
		for i := 0; i < int(discog.Results.Count); i++ {
			fmt.Println(i, discog.Results.Data[i].AlbTitle,
				discog.Results.Data[i].NumberDisk, discog.Results.Data[i].NumberTrack,
				discog.Results.Data[i].DigitalReleaseDate,
				discog.Results.Data[i].Copyright, discog.Results.Data[i].Type)
		}
	} else {
		fmt.Println("Cannot run further deezer tests without private info.")
	}
}
