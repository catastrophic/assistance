package deezer

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/logthis"
)

const (
	APIEndPoint            = "https://api.deezer.com"
	inconspicuousUserAgent = "Mozilla/5.0 (Android 14; Mobile; LG-M255; rv:119.0) Gecko/119.0 Firefox/119.0"
	userDataURL            = "https://www.deezer.com/ajax/gw-light.php?method=deezer.getUserData&input=3&api_version=1.0&api_token=null"
	albumInfoURL           = "https://www.deezer.com/ajax/gw-light.php?method=song.getListByAlbum&input=3&api_version=1.0&api_token="
	discographyInfoURL     = "https://www.deezer.com/ajax/gw-light.php?method=album.getDiscography&input=3&api_version=1.0&api_token="
	searchURL              = "https://www.deezer.com/ajax/gw-light.php?method=deezer.pageSearch&input=3&api_version=1.0&api_token="
	coverURLRoot           = "https://cdns-images.dzcdn.net/images/cover/"
	coverFilename          = "1500x1500.jpg"
	deezerToken            = ".deezer-token"
	sidFile                = ".session-id"
	licenseToken           = ".license-token"
	maxTokenAgeSeconds     = 600
	coverPlaceHolder       = "d41d8cd98f00b204e9800998ecf8427e"
	releaseURL             = "https://www.deezer.com/album/%d"
	coverRegexp            = `.*<meta property="og:image" content="https://.*/images/cover/(.{32})/\d*x\d*.jpg">.*`
)

// Deezer structure for querying its API.
type Deezer struct {
	ArlCookie    string
	SidCookie    string
	apiToken     string
	LicenseToken string
	Client       *http.Client
	userAgent    string
	cover        map[int]string
	mux          sync.Mutex
}

// New Deezer structure. api key can be blank for now.
func New(arl string) *Deezer {
	transport := &http.Transport{
		MaxIdleConnsPerHost: 100,
	}
	return &Deezer{ArlCookie: arl, SidCookie: "", Client: &http.Client{Transport: transport}, userAgent: inconspicuousUserAgent, cover: make(map[int]string)}
}

func (d *Deezer) doRequest(req *http.Request) ([]byte, error) {
	resp, err := d.Client.Do(req)
	if err != nil {
		return []byte{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return []byte{}, errors.New("returned status:" + resp.Status)
	}
	return ioutil.ReadAll(resp.Body)
}

func (d *Deezer) getRequest(url string) ([]byte, error) {
	if d.Client == nil {
		return nil, errors.New("uninitialized struct")
	}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("User-Agent", d.userAgent)
	req.Header.Add("Content-Language", "en-US")
	req.Header.Add("accept-language", "en-US,en;q=0.9,en-US;q=0.8,en;q=0.7")
	req.Header.Add("accept-charset", "utf-8,ISO-8859-1;q=0.8,*;q=0.7")
	req.Header.Add("Cookie", fmt.Sprintf("sid=%s; arl=%s", d.SidCookie, d.ArlCookie))
	return d.doRequest(req)
}

// GetRelease from the API. Returns the complete response.
func (d *Deezer) GetRelease(id int) (*Release, error) {
	// getting album info
	data, err := d.getRequest(APIEndPoint + "/album/" + strconv.Itoa(id))
	if err != nil {
		return nil, err
	}
	var r *Release
	if err := json.Unmarshal(data, &r); err != nil {
		logthis.Info("BAD JSON, Received: \n"+string(data), logthis.VERBOSEST)
		return nil, errors.Wrap(err, "error unmarshalling deezer api info")
	}
	// printing response, for debug purposes
	prettyData, err := json.MarshalIndent(r, "", "    ")
	if err == nil {
		logthis.Info("Deezer public API response:", logthis.VERBOSESTEST)
		logthis.Info(string(prettyData), logthis.VERBOSESTEST)
	}
	return r, nil
}

// GetTrackIDsFromAlbumID returns a list of tracks ids.
func (d *Deezer) GetTrackIDsFromAlbumID(id int) ([]int, error) {
	var ids []int
	a, err := d.GetRelease(id)
	if err != nil {
		return []int{}, err
	}
	for _, t := range a.Tracks.Data {
		ids = append(ids, t.ID)
	}
	return ids, nil
}

func (d *Deezer) getAPIToken() error {
	// only fetch it once, and avoid going out to API if we have a local copy
	d.mux.Lock()
	defer d.mux.Unlock()
	if d.apiToken != "" {
		return nil
	}
	if info, err := os.Stat(deezerToken); !os.IsNotExist(err) {
		tokenAge := time.Since(info.ModTime()).Seconds()
		if tokenAge < maxTokenAgeSeconds {
			apiTokenContent, err0 := os.ReadFile(deezerToken)
			licenseTokenContent, err1 := os.ReadFile(licenseToken)
			sidFileContent, err3 := os.ReadFile(sidFile)
			if err0 == nil && err1 == nil && err3 == nil {
				d.apiToken = string(apiTokenContent)
				d.LicenseToken = string(licenseTokenContent)
				d.SidCookie = string(sidFileContent)
				// the 'timer' on token validity resets upon use, so touch(1) the token file
				currentTime := time.Now().Local()
				if err = os.Chtimes(deezerToken, currentTime, currentTime); err != nil {
					logthis.TimedInfo("Unable to update "+deezerToken+" timestamp.", logthis.VERBOSEST)
				}
				logthis.TimedInfo("Fetched Deezer API, Session ID and license tokens from local files.", logthis.VERBOSEST)
				return nil
			}
		}
	}
	data, err := d.getRequest(userDataURL)
	if err != nil {
		return err
	}
	var r UserData
	if err := json.Unmarshal(data, &r); err != nil {
		return err
	}
	d.apiToken = r.Results.CheckForm
	if err := os.WriteFile(deezerToken, []byte(d.apiToken), 0600); err != nil {
		return errors.Wrap(err, "unable to write to deezer token file")
	}
	logthis.TimedInfo("Saved Deezer API token to local file.", logthis.VERBOSEST)
	d.SidCookie = r.Results.SessionID
	if err := os.WriteFile(sidFile, []byte(d.SidCookie), 0600); err != nil {
		return errors.Wrap(err, "unable to write to sid to local file")
	}
	logthis.TimedInfo("Saved Deezer session ID to local file.", logthis.VERBOSEST)
	d.LicenseToken = r.Results.User.Options.LicenseToken
	if err := os.WriteFile(licenseToken, []byte(d.LicenseToken), 0600); err != nil {
		return errors.Wrap(err, "unable to write to license token file")
	}
	logthis.TimedInfo("Saved Deezer License token to local file.", logthis.VERBOSEST)
	return nil
}

func (d *Deezer) getAjaxData(url string, inputData []byte) ([]byte, error) {
	if d.Client == nil {
		return nil, errors.New("uninitialized struct")
	}
	// if d.SidCookie == "" {
	// 	return nil, errors.New("fetching metadata requires sid cookie")
	// }
	// getting api_token from deezer if not available already
	if err := d.getAPIToken(); err != nil {
		return nil, errors.Wrap(err, "cannot find deezer api token")
	}

	// POSTing the request
	d.mux.Lock()
	req, err := http.NewRequest("POST", url+d.apiToken, bytes.NewBuffer(inputData))
	d.mux.Unlock()
	if err != nil {
		logthis.Error(err, logthis.NORMAL)
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Add("Cookie", "sid="+d.SidCookie+"; arl="+d.ArlCookie)
	req.Header.Add("User-Agent", d.userAgent)
	req.Header.Add("Content-Language", "en-US")
	req.Header.Add("accept-language", "en-US,en;q=0.9,en-US;q=0.8,en;q=0.7")
	req.Header.Add("accept-charset", "utf-8,ISO-8859-1;q=0.8,*;q=0.7")
	return d.doRequest(req)
}

func (d *Deezer) GetDiscographyInfo(artistID int) (*Discography, error) {
	// to include compilations, etc: "filter_role_id":[0,5]
	jsonStr := []byte(fmt.Sprintf(`{"art_id":%d,"start":0,"nb":100,"filter_role_id":[0],"nb_songs":0,"discography_mode":"all","lang": "en"}`, artistID))
	data, err := d.getAjaxData(discographyInfoURL, jsonStr)
	if err != nil {
		return nil, err
	}
	// parsing results
	var r *Discography
	if err := json.Unmarshal(data, &r); err != nil {
		fmt.Println(string(data))
		return nil, err
	}
	// printing response, for debug purposes
	prettyData, err := json.MarshalIndent(r, "", "    ")
	if err == nil {
		logthis.Info("Deezer private API response:", logthis.VERBOSESTEST)
		logthis.Info(string(prettyData), logthis.VERBOSESTEST)
	}
	return r, nil
}

func (d *Deezer) SearchArtist(artist string) (*SearchResult, error) {
	jsonStr := []byte(fmt.Sprintf(`{"query":"%s","start":0,"nb":40,"suggest":true,"artist_suggest":true,"top_tracks":false, "lang": "en"}`, artist))
	data, err := d.getAjaxData(searchURL, jsonStr)
	if err != nil {
		return nil, err
	}
	// parsing results
	var r *SearchResult
	if err := json.Unmarshal(data, &r); err != nil {
		fmt.Println(string(data))
		return nil, err
	}
	// printing response, for debug purposes
	prettyData, err := json.MarshalIndent(r, "", "    ")
	if err == nil {
		logthis.Info("Deezer private API response:", logthis.VERBOSESTEST)
		logthis.Info(string(prettyData), logthis.VERBOSESTEST)
	}
	return r, nil
}

func (d *Deezer) GetAjaxAlbumInfo(id int) (*Ajax, error) {
	var jsonStr = []byte(`{"alb_id":` + strconv.Itoa(id) + `, "nb": 500, "lang": "en"}`)
	data, err := d.getAjaxData(albumInfoURL, jsonStr)
	if err != nil {
		return nil, err
	}
	// parsing results to get ALB_PICTURE
	var r *Ajax
	if err := json.Unmarshal(data, &r); err != nil {
		fmt.Println(string(data))
		return nil, err
	}
	// printing response, for debug purposes
	prettyData, err := json.MarshalIndent(r, "", "    ")
	if err == nil {
		logthis.Info("Deezer private API response:", logthis.VERBOSESTEST)
		logthis.Info(string(prettyData), logthis.VERBOSESTEST)
	}
	if len(r.Results.Data) != 0 {
		d.cover[id] = coverURLRoot + r.Results.Data[0].AlbumPicture + "/" + coverFilename
	}
	logthis.Info("MD5: "+r.Results.Data[0].Md5, logthis.VERBOSESTEST)
	return r, nil
}

func (d *Deezer) GetCover(id int) (string, error) {
	if _, ok := d.cover[id]; ok && !strings.Contains(d.cover[id], coverPlaceHolder) {
		return d.cover[id], nil
	}
	r, err := d.GetAjaxAlbumInfo(id)
	if err != nil {
		return "", err
	}
	var cover string
	if r.Results.Data[0].AlbumPicture == coverPlaceHolder {
		// getting the identifier by other means...
		data, err := d.getRequest(fmt.Sprintf(releaseURL, id))
		if err != nil {
			return "", err
		}
		// parsing output
		rxp := regexp.MustCompile(coverRegexp)
		if rxp.MatchString(string(data)) {
			hits := rxp.FindStringSubmatch(string(data))
			r.Results.Data[0].AlbumPicture = hits[1]
		} else {
			return "", errors.New("could not scrape cover identifier")
		}
	}
	cover = coverURLRoot + r.Results.Data[0].AlbumPicture + "/" + coverFilename
	d.cover[id] = cover
	return cover, nil
}
