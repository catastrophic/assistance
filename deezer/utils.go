package deezer

import (
	"regexp"
	"strconv"

	"github.com/pkg/errors"
)

const (
	regexpDeezerURL = `^https\:\/\/.*deezer\.com(\/|\/.{2}\/)album\/(\d*).*$`
)

func ExtractDeezerAlbumID(urlOrID string) (int, error) {
	var deezerID int
	var err error
	// if it's an int, converting
	if deezerID, err = strconv.Atoi(urlOrID); err == nil {
		return deezerID, nil
	}
	// else, trying to extract an album ID
	r := regexp.MustCompile(regexpDeezerURL)
	hits := r.FindStringSubmatch(urlOrID)
	if len(hits) != 3 {
		return -1, errors.New("could not find ID")
	}
	deezerID, _ = strconv.Atoi(hits[2])
	return deezerID, nil
}
