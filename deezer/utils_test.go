package deezer

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUtils(t *testing.T) {
	fmt.Println("+ Testing utils...")
	check := assert.New(t)

	id, err := ExtractDeezerAlbumID("2321")
	check.Nil(err)
	check.Equal(2321, id)

	id, err = ExtractDeezerAlbumID("23k21")
	check.NotNil(err)
	check.Equal(-1, id)

	id, err = ExtractDeezerAlbumID("https://www.deezer.com/fr/album/16650")
	check.Nil(err)
	check.Equal(16650, id)

	id, err = ExtractDeezerAlbumID("https://www.deezer.com/album/16650")
	check.Nil(err)
	check.Equal(16650, id)

	id, err = ExtractDeezerAlbumID("https://www.deezer.com/fr/album/16650#comments")
	check.Nil(err)
	check.Equal(16650, id)

	id, err = ExtractDeezerAlbumID("#https://www.deezer.com/fr/album/16650#comments")
	check.NotNil(err)
	check.Equal(-1, id)

	id, err = ExtractDeezerAlbumID("https://deezer.com/en/album/181927842")
	check.Nil(err)
	check.Equal(181927842, id)
}
