package deezer

import (
	"encoding/json"
	"strconv"
	"time"
)

type Release struct {
	Artist struct {
		ID            int    `json:"id"`
		Name          string `json:"name"`
		Picture       string `json:"picture"`
		PictureBig    string `json:"picture_big"`
		PictureMedium string `json:"picture_medium"`
		PictureSmall  string `json:"picture_small"`
		PictureXl     string `json:"picture_xl"`
		Tracklist     string `json:"tracklist"`
		Type          string `json:"type"`
	} `json:"artist"`
	Available    bool `json:"available"`
	Contributors []struct {
		ID            int    `json:"id"`
		Link          string `json:"link"`
		Name          string `json:"name"`
		Picture       string `json:"picture"`
		PictureBig    string `json:"picture_big"`
		PictureMedium string `json:"picture_medium"`
		PictureSmall  string `json:"picture_small"`
		PictureXl     string `json:"picture_xl"`
		Radio         bool   `json:"radio"`
		Role          string `json:"role"`
		Share         string `json:"share"`
		Tracklist     string `json:"tracklist"`
		Type          string `json:"type"`
	} `json:"contributors"`
	Cover                 string `json:"cover"`
	CoverBig              string `json:"cover_big"`
	CoverMedium           string `json:"cover_medium"`
	CoverSmall            string `json:"cover_small"`
	CoverXl               string `json:"cover_xl"`
	Duration              int    `json:"duration"`
	ExplicitContentCover  int    `json:"explicit_content_cover"`
	ExplicitContentLyrics int    `json:"explicit_content_lyrics"`
	ExplicitLyrics        bool   `json:"explicit_lyrics"`
	Fans                  int    `json:"fans"`
	GenreID               int    `json:"genre_id"`
	Genres                struct {
		Data []struct {
			ID      int    `json:"id"`
			Name    string `json:"name"`
			Picture string `json:"picture"`
			Type    string `json:"type"`
		} `json:"data"`
	} `json:"genres"`
	ID          int    `json:"id"`
	Label       string `json:"label"`
	Link        string `json:"link"`
	NbTracks    int    `json:"nb_tracks"`
	Rating      int    `json:"rating"`
	RecordType  string `json:"record_type"`
	ReleaseDate string `json:"release_date"`
	Share       string `json:"share"`
	Title       string `json:"title"`
	Tracklist   string `json:"tracklist"`
	Tracks      struct {
		Data []struct {
			Artist struct {
				ID        int    `json:"id"`
				Name      string `json:"name"`
				Tracklist string `json:"tracklist"`
				Type      string `json:"type"`
			} `json:"artist"`
			Duration              int    `json:"duration"`
			ExplicitContentCover  int    `json:"explicit_content_cover"`
			ExplicitContentLyrics int    `json:"explicit_content_lyrics"`
			ExplicitLyrics        bool   `json:"explicit_lyrics"`
			ID                    int    `json:"id"`
			Link                  string `json:"link"`
			Preview               string `json:"preview"`
			Rank                  int    `json:"rank"`
			Readable              bool   `json:"readable"`
			Title                 string `json:"title"`
			TitleShort            string `json:"title_short"`
			TitleVersion          string `json:"title_version"`
			Type                  string `json:"type"`
		} `json:"data"`
	} `json:"tracks"`
	Type string `json:"type"`
	Upc  string `json:"upc"`
}

func (r *Release) ReleaseYear() int {
	releaseDate, err := time.Parse("2006-01-02", r.ReleaseDate)
	if err != nil {
		return 0
	}
	year, _ := strconv.Atoi(releaseDate.Format("2006"))
	return year
}

// SongContributors is defined because the response is either a map, or an empty slice.
type SongContributors map[string][]string

// UnmarshalJSON allows to unmarshall JSON with an empty slice as contributors, without throwing errors.
// Instead, the song contributors return an empty map.
func (w *SongContributors) UnmarshalJSON(data []byte) error {
	var v map[string][]string
	// trying to unmarshall normally
	if err := json.Unmarshal(data, &v); err != nil {
		// not a map, now trying to read as a slice
		var t []string
		// if it's not a slice either, really return an error
		if err := json.Unmarshal(data, &t); err != nil {
			return err
		}
		// if it's a slice, it's an empty slice. returning the empty map.
	}
	*w = SongContributors(v)
	return nil
}

type Ajax struct {
	Error   []interface{} `json:"error"`
	Results struct {
		Count int `json:"count"`
		Data  []struct {
			Duration            string           `json:"DURATION"`
			DiskNumber          string           `json:"DISK_NUMBER"`
			DigitalReleaseDate  string           `json:"DIGITAL_RELEASE_DATE"`
			PhysicalReleaseDate string           `json:"PHYSICAL_RELEASE_DATE"`
			ArtistName          string           `json:"ART_NAME"`
			AlbumID             string           `json:"ALB_ID"`
			AlbumPicture        string           `json:"ALB_PICTURE"`
			AlbumTitle          string           `json:"ALB_TITLE"`
			Md5                 string           `json:"MD5_ORIGIN"`
			SongID              string           `json:"SNG_ID"`
			SongTitle           string           `json:"SNG_TITLE"`
			Version             string           `json:"VERSION"`
			SongContributors    SongContributors `json:"SNG_CONTRIBUTORS,omitempty"`
			Status              int              `json:"STATUS"`
			TrackNumber         string           `json:"TRACK_NUMBER"`
			TrackToken          string           `json:"TRACK_TOKEN"`
			FlacSize            string           `json:"FILESIZE_FLAC"`
			Type                int              `json:"TYPE"`
			UserID              int              `json:"USER_ID"`
			MediaVersion        string           `json:"MEDIA_VERSION"`
			TYPE2               string           `json:"__TYPE__"`
			ISRC                string           `json:"ISRC"`
		} `json:"data"`
		FilteredCount int `json:"filtered_count"`
		Total         int `json:"total"`
	} `json:"results"`
}

type UserData struct {
	Error   []interface{} `json:"error"`
	Results struct {
		SessionID string `json:"SESSION_ID"`
		CheckForm string `json:"checkForm"`
		User      struct {
			Options struct {
				LicenseCountry string `json:"license_country"`
				LicenseToken   string `json:"license_token"`
			} `json:"OPTIONS"`
		} `json:"USER"`
	} `json:"results"`
}

const (
	Single = iota
	Album
	Split
	EP
	UnknownType
)

type Discography struct {
	Error   []interface{} `json:"error"`
	Results struct {
		ArtID        int64 `json:"art_id"`
		CacheVersion int64 `json:"cache_version"`
		Count        int64 `json:"count"`
		Data         []struct {
			AlbID      string `json:"ALB_ID"`
			AlbPicture string `json:"ALB_PICTURE"`
			AlbTitle   string `json:"ALB_TITLE"`
			Artists    []struct {
				ArtistsAlbumsOrder string `json:"ARTISTS_ALBUMS_ORDER"`
				ArtistIsDummy      bool   `json:"ARTIST_IS_DUMMY"`
				ArtID              string `json:"ART_ID"`
				ArtName            string `json:"ART_NAME"`
				ArtPicture         string `json:"ART_PICTURE"`
				Rank               string `json:"RANK"`
				RoleID             string `json:"ROLE_ID"`
				Type               string `json:"__TYPE__"`
			} `json:"ARTISTS"`
			ArtistsAlbumsIsOfficial bool   `json:"ARTISTS_ALBUMS_IS_OFFICIAL"`
			ArtID                   string `json:"ART_ID"`
			ArtName                 string `json:"ART_NAME"`
			Copyright               string `json:"COPYRIGHT"`
			DigitalReleaseDate      string `json:"DIGITAL_RELEASE_DATE"`
			ExplicitAlbumContent    struct {
				ExplicitCoverStatus  int64 `json:"EXPLICIT_COVER_STATUS"`
				ExplicitLyricsStatus int64 `json:"EXPLICIT_LYRICS_STATUS"`
			} `json:"EXPLICIT_ALBUM_CONTENT"`
			ExplicitLyrics      string `json:"EXPLICIT_LYRICS"`
			GenreID             string `json:"GENRE_ID"`
			Highlight           string `json:"HIGHLIGHT"`
			MinorGenreID        string `json:"MINOR_GENRE_ID"`
			NumberDisk          string `json:"NUMBER_DISK"`
			NumberTrack         string `json:"NUMBER_TRACK"`
			PhysicalReleaseDate string `json:"PHYSICAL_RELEASE_DATE"`
			ProducerLine        string `json:"PRODUCER_LINE"`
			ProviderID          string `json:"PROVIDER_ID"`
			Rank                string `json:"RANK"`
			RoleID              int64  `json:"ROLE_ID"`
			Status              string `json:"STATUS"`
			Type                string `json:"TYPE"`
			Type_               string `json:"__TYPE__"`
		} `json:"data"`
		FilteredCount int64 `json:"filtered_count"`
		Nb            int64 `json:"nb"`
		Start         int64 `json:"start"`
		Total         int64 `json:"total"`
	} `json:"results"`
}

type SearchResult struct {
	Error   []interface{} `json:"error"`
	Results struct {
		Album struct {
			Count int64 `json:"count"`
			Data  []struct {
				AlbID      string `json:"ALB_ID"`
				AlbPicture string `json:"ALB_PICTURE"`
				AlbTitle   string `json:"ALB_TITLE"`
				Artists    []struct {
					ArtistsAlbumsOrder string `json:"ARTISTS_ALBUMS_ORDER"`
					ArtistIsDummy      bool   `json:"ARTIST_IS_DUMMY"`
					ArtID              string `json:"ART_ID"`
					ArtName            string `json:"ART_NAME"`
					ArtPicture         string `json:"ART_PICTURE"`
					Rank               string `json:"RANK"`
					RoleID             string `json:"ROLE_ID"`
					Type               string `json:"__TYPE__"`
				} `json:"ARTISTS"`
				ArtistIsDummy        bool   `json:"ARTIST_IS_DUMMY"`
				ArtID                string `json:"ART_ID"`
				ArtName              string `json:"ART_NAME"`
				Available            bool   `json:"AVAILABLE"`
				ExplicitAlbumContent struct {
					ExplicitCoverStatus  int64 `json:"EXPLICIT_COVER_STATUS"`
					ExplicitLyricsStatus int64 `json:"EXPLICIT_LYRICS_STATUS"`
				} `json:"EXPLICIT_ALBUM_CONTENT"`
				NumberTrack         string `json:"NUMBER_TRACK"`
				PhysicalReleaseDate string `json:"PHYSICAL_RELEASE_DATE"`
				Type                string `json:"TYPE"`
				Version             string `json:"VERSION"`
				Type_               string `json:"__TYPE__"`
			} `json:"data"`
			FilteredCount int64   `json:"filtered_count"`
			FilteredItems []int64 `json:"filtered_items"`
			Next          int64   `json:"next"`
			Total         int64   `json:"total"`
		} `json:"ALBUM"`
		Artist struct {
			Count int64 `json:"count"`
			Data  []struct {
				ArtistIsDummy bool   `json:"ARTIST_IS_DUMMY"`
				ArtID         string `json:"ART_ID"`
				ArtName       string `json:"ART_NAME"`
				ArtPicture    string `json:"ART_PICTURE"`
				NbFan         int64  `json:"NB_FAN"`
				Type          string `json:"__TYPE__"`
			} `json:"data"`
			FilteredCount int64         `json:"filtered_count"`
			FilteredItems []interface{} `json:"filtered_items"`
			Next          int64         `json:"next"`
			Total         int64         `json:"total"`
		} `json:"ARTIST"`
		Fuzzinness   bool          `json:"FUZZINNESS"`
		Order        []string      `json:"ORDER"`
		Query        string        `json:"QUERY"`
		RevisedQuery string        `json:"REVISED_QUERY"`
		TopResult    []interface{} `json:"TOP_RESULT"`
	} `json:"results"`
}
