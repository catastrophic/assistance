package intslice

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIntSlice(t *testing.T) {
	fmt.Println("+ TestingIntSlice...")
	check := assert.New(t)

	a := []int{1, 2, 3}
	b := []string{"1", "2", "3"}
	c := []int{-1, 0, -4}
	d := []int{2, 3, 4}

	check.True(Contains(a, 1))
	check.False(Contains(a, 10))

	check.True(IsContinuous([]int{1, 2, 3, 4, 5}))
	check.False(IsContinuous([]int{1, 2, 3, 5}))
	check.True(IsContinuous([]int{2, 3, 4, 5}))
	check.False(IsContinuous([]int{1, 3, 2, 5}))

	check.Equal([]int{1, 2, 3, 4, 5}, CreateContinuousIntSlice(1, 5))
	check.Equal([]int{2, 3, 4, 5, 6}, CreateContinuousIntSlice(2, 6))
	check.Equal([]int{1}, Difference(CreateContinuousIntSlice(1, 5), CreateContinuousIntSlice(2, 6)))

	check.Equal(b, ToStringSlice(a))

	check.Equal(1, Min(a))
	check.Equal(3, Max(a))
	check.Equal(-4, Min(c))
	check.Equal(0, Max(c))
	check.Equal(6, Sum(a))
	check.Equal(-5, Sum(c))

	check.Equal([]int{2, 3}, Common(a, d))
	check.Equal([]int{1}, Difference(a, d))
	check.Equal([]int{1, 4}, NotCommon(a, d))

	e := []int{1, 2, 1, 2, 3, 7, 3}
	RemoveDuplicates(&e)
	check.Equal([]int{1, 2, 3, 7}, e)

	f := []int64{1, 2, 1, 2, 3, 7, 3}
	RemoveDuplicates64(&f)
	check.Equal([]int64{1, 2, 3, 7}, f)

	check.True(ContainsDuplicates([]int{1, 2, 2, 3}))
	check.False(ContainsDuplicates([]int{1, 2, 3}))
}
