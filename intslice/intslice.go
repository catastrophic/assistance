// Package intslice provides a few helper functions for dealing with []int.
package intslice

import (
	"strconv"
)

// Contains checks if an int is in a []int, returns bool.
func Contains(list []int, a int) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// IsContinuous checks if an int slice contains a continuous list of incremental integers
func IsContinuous(list []int) bool {
	for i, a := range list {
		if i == 0 {
			continue
		}
		if a != list[i-1]+1 {
			return false
		}
	}
	return true
}

func CreateContinuousIntSlice(start, end int) []int {
	if end < start {
		return nil
	}
	var l []int
	for i := start; i <= end; i++ {
		l = append(l, i)
	}
	return l
}

// Sum of the contents of an []int.
func Sum(list []int) int {
	var sum int
	for _, b := range list {
		sum += b
	}
	return sum
}

// Min for a non-empty int slice.
func Min(list []int) int {
	var m int
	for i, e := range list {
		if i == 0 || e < m {
			m = e
		}
	}
	return m
}

// Max for a non-empty int slice.
func Max(list []int) int {
	var m int
	for i, e := range list {
		if i == 0 || e > m {
			m = e
		}
	}
	return m
}

// ToStringSlice transforms an []int to a []string.
func ToStringSlice(in []int) []string {
	b := make([]string, len(in))
	for i, v := range in {
		b[i] = strconv.Itoa(v)
	}
	return b
}

func Common(a, b []int) []int {
	m := make(map[int]bool)
	for _, y := range b {
		m[y] = true
	}
	var ret []int
	for _, x := range a {
		if m[x] {
			ret = append(ret, x)
		}
	}
	return ret
}

func ContainsDuplicates(a []int) bool {
	m := make(map[int]bool)
	for _, y := range a {
		if m[y] {
			return true
		}
		m[y] = true
	}
	return false
}

func Difference(a, b []int) []int {
	m := make(map[int]bool)
	for _, y := range b {
		m[y] = true
	}
	var ret []int
	// finding elements in a not in b
	for _, x := range a {
		if !m[x] {
			ret = append(ret, x)
		}
	}
	return ret
}

func NotCommon(a, b []int) []int {
	res := Difference(a, b)
	res = append(res, Difference(b, a)...)
	return res
}

// RemoveDuplicates in []int.
func RemoveDuplicates(options *[]int, otherStringsToClean ...int) {
	found := make(map[int]bool)
	// specifically remove other strings from values
	for _, o := range otherStringsToClean {
		found[o] = true
	}
	j := 0
	for i, x := range *options {
		if !found[x] {
			found[x] = true
			(*options)[j] = (*options)[i]
			j++
		}
	}
	*options = (*options)[:j]
}

// RemoveDuplicates64 in []int64.
func RemoveDuplicates64(options *[]int64, otherStringsToClean ...int64) {
	found := make(map[int64]bool)
	// specifically remove other strings from values
	for _, o := range otherStringsToClean {
		found[o] = true
	}
	j := 0
	for i, x := range *options {
		if !found[x] {
			found[x] = true
			(*options)[j] = (*options)[i]
			j++
		}
	}
	*options = (*options)[:j]
}
