GO = GO111MODULE=on go

all: fmt check test-coverage build

prepare:
	${GO} get -u github.com/divan/depscheck
	${GO} get github.com/warmans/golocc
	curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s -- -b $(go env GOPATH)/bin v1.27.0

deps:
	${GO} mod download

fmt:
	${GO} fmt ./...

check: fmt
	golangci-lint run

info: fmt
	depscheck -totalonly -tests .
	golocc .

test:
	${GO} test -p 1 -v ./... -cover

test-coverage:
	${GO} test -p 1 -race -coverprofile=coverage.txt -covermode=atomic ./...

clean:
	rm -f coverage.txt

build:
	${GO} build ./...





