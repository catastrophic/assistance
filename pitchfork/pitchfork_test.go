package pitchfork

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPitchFork(t *testing.T) {
	fmt.Println("+ Testing pitchfork...")
	check := assert.New(t)

	p := New()
	res, err := p.Search("pierre bastien", "mecanoid")
	check.Nil(err)
	review, err := res.FirstAlbumReview()
	check.Nil(err)
	check.Equal("8.6", review.Score)
	check.False(review.BNM)
	fmt.Println(review.BBCode())

	res, err = p.Search("earl sweatshirt", "feet of clay")
	check.Nil(err)
	review, err = res.FirstAlbumReview()
	check.Nil(err)
	check.Equal("8.4", review.Score)
	check.True(review.BNM)
	fmt.Println(review.BBCode())
}
