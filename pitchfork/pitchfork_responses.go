package pitchfork

import (
	"errors"
	"strings"

	"gitlab.com/catastrophic/assistance/logthis"
)

type Search struct {
	Context struct {
		Dispatcher struct {
			Stores struct {
				RouteStore struct {
					CurrentNavigate struct {
						Error         interface{} `json:"error"`
						IsComplete    bool        `json:"isComplete"`
						TransactionID int         `json:"transactionId"`
						URL           string      `json:"url"`
					} `json:"currentNavigate"`
					Routes interface{} `json:"routes"`
				} `json:"RouteStore"`
				SearchStore struct {
					Category  interface{}   `json:"category"`
					Empty     bool          `json:"empty"`
					Genres    []interface{} `json:"genres"`
					ItemPages []interface{} `json:"itemPages"`
					Items     struct{}      `json:"items"`
					Meta      struct {
						HasSearched bool   `json:"has_searched"`
						Query       string `json:"query"`
						Sort        string `json:"sort"`
						URL         string `json:"url"`
					} `json:"meta"`
					Pagination struct {
						Current int `json:"current"`
						End     int `json:"end"`
						Start   int `json:"start"`
						Total   int `json:"total"`
					} `json:"pagination"`
					Results struct {
						Albumreviews struct {
							Count int `json:"count"`
							Items []struct {
								Artists []struct {
									DisplayName string `json:"display_name"`
									Genres      []struct {
										DisplayName string `json:"display_name"`
										Slug        string `json:"slug"`
									} `json:"genres"`
									ID     string `json:"id"`
									Photos struct {
										Lede   bool `json:"lede"`
										Social bool `json:"social"`
										Tout   struct {
											AltText   string `json:"altText"`
											Caption   string `json:"caption"`
											Credit    string `json:"credit"`
											Height    int    `json:"height"`
											ModelName string `json:"modelName"`
											Sizes     struct {
												M  string `json:"m"`
												Sm string `json:"sm"`
											} `json:"sizes"`
											Title string `json:"title"`
											Width int    `json:"width"`
										} `json:"tout"`
									} `json:"photos"`
									Slug string `json:"slug"`
									URL  string `json:"url"`
								} `json:"artists"`
								Authors []struct {
									ID    string `json:"id"`
									Name  string `json:"name"`
									Slug  string `json:"slug"`
									Title string `json:"title"`
									URL   string `json:"url"`
								} `json:"authors"`
								Channel     string `json:"channel"`
								ContentType string `json:"contentType"`
								Dek         string `json:"dek"`
								Genres      []struct {
									DisplayName string `json:"display_name"`
									Slug        string `json:"slug"`
								} `json:"genres"`
								ID                string        `json:"id"`
								ModifiedAt        string        `json:"modifiedAt"`
								Position          int           `json:"position"`
								PrivateTags       []string      `json:"privateTags"`
								PromoDescription  string        `json:"promoDescription"`
								PromoTitle        string        `json:"promoTitle"`
								PubDate           string        `json:"pubDate"`
								SeoDescription    string        `json:"seoDescription"`
								SeoTitle          string        `json:"seoTitle"`
								SocialDescription string        `json:"socialDescription"`
								SocialTitle       string        `json:"socialTitle"`
								SubChannel        string        `json:"subChannel"`
								Tags              []interface{} `json:"tags"`
								Timestamp         int           `json:"timestamp"`
								Title             string        `json:"title"`
								Tombstone         struct {
									Albums []struct {
										Album struct {
											Artists []struct {
												DisplayName string `json:"display_name"`
												Genres      []struct {
													DisplayName string `json:"display_name"`
													Slug        string `json:"slug"`
												} `json:"genres"`
												ID     string `json:"id"`
												Photos struct {
													Lede   bool `json:"lede"`
													Social bool `json:"social"`
													Tout   struct {
														AltText   string `json:"altText"`
														Caption   string `json:"caption"`
														Credit    string `json:"credit"`
														Height    int    `json:"height"`
														ModelName string `json:"modelName"`
														Sizes     struct {
															M  string `json:"m"`
															Sm string `json:"sm"`
														} `json:"sizes"`
														Title string `json:"title"`
														Width int    `json:"width"`
													} `json:"tout"`
												} `json:"photos"`
												Slug string `json:"slug"`
												URL  string `json:"url"`
											} `json:"artists"`
											DisplayName string `json:"display_name"`
											Labels      []struct {
												DisplayName string `json:"display_name"`
												ID          string `json:"id"`
												Name        string `json:"name"`
											} `json:"labels"`
											Photos struct {
												Lede   bool `json:"lede"`
												Social bool `json:"social"`
												Tout   struct {
													AltText string `json:"altText"`
													Caption string `json:"caption"`
													Credit  string `json:"credit"`
													Height  int    `json:"height"`
													Sizes   struct {
														HomepageLarge string `json:"homepageLarge"`
														HomepageSmall string `json:"homepageSmall"`
														List          string `json:"list"`
														Standard      string `json:"standard"`
													} `json:"sizes"`
													Title string `json:"title"`
													Width int    `json:"width"`
												} `json:"tout"`
											} `json:"photos"`
											ReleaseYear int `json:"release_year"`
										} `json:"album"`
										ID             string `json:"id"`
										LabelsAndYears []struct {
											Labels []struct {
												DisplayName string `json:"display_name"`
												ID          string `json:"id"`
												Name        string `json:"name"`
											} `json:"labels"`
											Year int `json:"year"`
										} `json:"labels_and_years"`
										Rating struct {
											Bnm           bool   `json:"bnm"`
											Bnr           bool   `json:"bnr"`
											DisplayRating string `json:"display_rating"`
											Rating        string `json:"rating"`
										} `json:"rating"`
									} `json:"albums"`
									Bnm bool `json:"bnm"`
									Bnr bool `json:"bnr"`
								} `json:"tombstone"`
								URL string `json:"url"`
							} `json:"items"`
						} `json:"albumreviews"`
						Artists struct {
							Count int           `json:"count"`
							Items []interface{} `json:"items"`
						} `json:"artists"`
						Contributors struct {
							Count int           `json:"count"`
							Items []interface{} `json:"items"`
						} `json:"contributors"`
						Features struct {
							Count int           `json:"count"`
							Items []interface{} `json:"items"`
						} `json:"features"`
						Levels struct {
							Count int           `json:"count"`
							Items []interface{} `json:"items"`
						} `json:"levels"`
						News struct {
							Count int `json:"count"`
							Items []struct {
								Artists []interface{} `json:"artists"`
								Authors []struct {
									ID    string      `json:"id"`
									Name  string      `json:"name"`
									Slug  interface{} `json:"slug"`
									Title string      `json:"title"`
									URL   interface{} `json:"url"`
								} `json:"authors"`
								Channel     string        `json:"channel"`
								ContentType string        `json:"contentType"`
								Dek         string        `json:"dek"`
								Genres      []interface{} `json:"genres"`
								ID          string        `json:"id"`
								ModifiedAt  string        `json:"modifiedAt"`
								Photos      struct {
									Lede struct {
										AltText   string `json:"altText"`
										Caption   string `json:"caption"`
										Credit    string `json:"credit"`
										Height    int    `json:"height"`
										ModelName string `json:"modelName"`
										Sizes     struct {
											LargeModule string `json:"largeModule"`
											List        string `json:"list"`
											SmallModule string `json:"smallModule"`
											Standard    string `json:"standard"`
										} `json:"sizes"`
										Title string `json:"title"`
										Width int    `json:"width"`
									} `json:"lede"`
									Social struct {
										AltText   string `json:"altText"`
										Caption   string `json:"caption"`
										Credit    string `json:"credit"`
										Height    int    `json:"height"`
										ModelName string `json:"modelName"`
										Sizes     struct {
											LargeModule string `json:"largeModule"`
											List        string `json:"list"`
											SmallModule string `json:"smallModule"`
											Standard    string `json:"standard"`
										} `json:"sizes"`
										Title string `json:"title"`
										Width int    `json:"width"`
									} `json:"social"`
									Tout struct {
										AltText   string `json:"altText"`
										Caption   string `json:"caption"`
										Credit    string `json:"credit"`
										Height    int    `json:"height"`
										ModelName string `json:"modelName"`
										Sizes     struct {
											LargeModule string `json:"largeModule"`
											List        string `json:"list"`
											SmallModule string `json:"smallModule"`
											Standard    string `json:"standard"`
										} `json:"sizes"`
										Title string `json:"title"`
										Width int    `json:"width"`
									} `json:"tout"`
								} `json:"photos"`
								PrivateTags       []interface{} `json:"privateTags"`
								PromoDescription  string        `json:"promoDescription"`
								PromoTitle        string        `json:"promoTitle"`
								PubDate           string        `json:"pubDate"`
								SeoDescription    string        `json:"seoDescription"`
								SeoTitle          string        `json:"seoTitle"`
								SocialDescription string        `json:"socialDescription"`
								SocialTitle       string        `json:"socialTitle"`
								SubChannel        string        `json:"subChannel"`
								Tags              []struct {
									Name string `json:"name"`
									Slug string `json:"slug"`
								} `json:"tags"`
								Timestamp int    `json:"timestamp"`
								Title     string `json:"title"`
								URL       string `json:"url"`
							} `json:"items"`
						} `json:"news"`
						Thepitch struct {
							Count int           `json:"count"`
							Items []interface{} `json:"items"`
						} `json:"thepitch"`
						Tracks struct {
							Count int           `json:"count"`
							Items []interface{} `json:"items"`
						} `json:"tracks"`
						Video struct {
							Count int           `json:"count"`
							Items []interface{} `json:"items"`
						} `json:"video"`
					} `json:"results"`
				} `json:"SearchStore"`
				TrackerStore struct {
					Canonical string `json:"canonical"`
					Dd        struct {
						Canonical          string `json:"canonical"`
						Display            string `json:"display"`
						NumOfSearchResults int    `json:"numOfSearchResults"`
						PageType           string `json:"pageType"`
						SearchTerms        string `json:"searchTerms"`
						Section            string `json:"section"`
					} `json:"dd"`
					GtmContent struct {
						ContentLength string `json:"contentLength"`
						ContentSource string `json:"contentSource"`
						Display       string `json:"display"`
						Section       string `json:"section"`
					} `json:"gtmContent"`
					GtmSearch struct {
						SearchTerms string `json:"searchTerms"`
					} `json:"gtmSearch"`
					GtmUser     struct{}    `json:"gtmUser"`
					PageTitle   interface{} `json:"pageTitle"`
					PageType    string      `json:"pageType"`
					Params      struct{}    `json:"params"`
					ParselyData struct {
						Context        string `json:"@context"`
						Type           string `json:"@type"`
						ArticleSection string `json:"articleSection"`
						Headline       string `json:"headline"`
						URL            string `json:"url"`
					} `json:"parselyData"`
					Query struct {
						Query string `json:"query"`
					} `json:"query"`
					Section   string `json:"section"`
					StoreName string `json:"storeName"`
					URL       string `json:"url"`
				} `json:"TrackerStore"`
				WindowStore struct {
					FormFactor string `json:"formFactor"`
				} `json:"WindowStore"`
			} `json:"stores"`
		} `json:"dispatcher"`
		Options struct {
			OptimizePromiseCallback bool `json:"optimizePromiseCallback"`
		} `json:"options"`
		Plugins struct{} `json:"plugins"`
	} `json:"context"`
	Plugins struct{} `json:"plugins"`
}

func (ps *Search) FirstAlbumReview() (*Review, error) {
	numberOfHits := ps.Context.Dispatcher.Stores.SearchStore.Results.Albumreviews.Count
	if numberOfHits == 0 {
		return nil, errors.New("no hits")
	} else if numberOfHits >= 2 {
		logthis.Info("More than 1 hit, getting information for the first hit only.", logthis.NORMAL)
	}

	firstResult := ps.Context.Dispatcher.Stores.SearchStore.Results.Albumreviews.Items[0]
	pfr := new(Review)
	pfr.URL = firstResult.URL
	pfr.Score = firstResult.Tombstone.Albums[0].Rating.Rating
	pfr.BNM = firstResult.Tombstone.Albums[0].Rating.Bnm
	pfr.BNR = firstResult.Tombstone.Albums[0].Rating.Bnr
	pfr.Title = firstResult.Tombstone.Albums[0].Album.DisplayName
	for _, t := range firstResult.Tombstone.Albums[0].Album.Artists {
		pfr.Artists = append(pfr.Artists, Artist{Name: t.DisplayName, URL: t.URL})
	}
	for _, t := range firstResult.Genres {
		pfr.Genres = append(pfr.Genres, strings.Replace(strings.ToLower(t.DisplayName), " ", ".", -1))
	}
	for _, t := range firstResult.Tombstone.Albums[0].Album.Labels {
		pfr.Labels = append(pfr.Labels, t.DisplayName)
	}
	pfr.ReleaseYear = firstResult.Tombstone.Albums[0].Album.ReleaseYear
	pfr.Description = firstResult.SeoDescription
	return pfr, nil
}
