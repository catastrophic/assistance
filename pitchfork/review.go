package pitchfork

import (
	"fmt"
	"strings"
)

const (
	reviewTemplate = ` 
[quote]%s - [i]%s[/i] (%s)
[b]%s[/b] [b][i]%s%s[/i][/b] 
%d - %s 
[i]%s..[/i]
[align=right]- [b][url=%s]Pitchfork Review[/url][/b][/align][/quote]
`
)

type Artist struct {
	Name string
	URL  string
}

func (pa *Artist) BBCode() string {
	return "[b][url=" + rootURL + pa.URL + "]" + pa.Name + "[/url][/b]"
}

type Review struct {
	URL         string
	Title       string
	Artists     []Artist
	Score       string
	BNM         bool
	BNR         bool
	Genres      []string
	Labels      []string
	ReleaseYear int
	Description string
}

func (pr *Review) BBCode() string {
	var artistList []string
	for _, a := range pr.Artists {
		artistList = append(artistList, a.BBCode())
	}
	var bnm, bnr string
	if pr.BNM {
		bnm = "[Best New Music]"
	}
	if pr.BNR {
		bnr = "[Best New Reissue]"
	}
	return fmt.Sprintf(reviewTemplate, strings.Join(artistList, ", "), pr.Title, strings.Join(pr.Genres, ", "), pr.Score, bnm, bnr, pr.ReleaseYear, strings.Join(pr.Labels, ", "), pr.Description, rootURL+pr.URL)
}
