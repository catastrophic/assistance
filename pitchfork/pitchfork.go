package pitchfork

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"regexp"

	"gitlab.com/catastrophic/assistance/logthis"
	"golang.org/x/net/publicsuffix"
)

const (
	rootURL           = "http://pitchfork.com"
	searchURL         = "http://pitchfork.com/search/?query="
	userAgent         = "Mozilla/5.0 (X11; Linux i686; rv:64.0) Gecko/20100101 Firefox/64.0"
	jsonExtractRegExp = `<script>window\.App={(.*)};</script>`
)

type PitchFork struct {
	Client *http.Client
}

func New() *PitchFork {
	options := cookiejar.Options{
		PublicSuffixList: publicsuffix.List,
	}
	jar, err := cookiejar.New(&options)
	if err != nil {
		logthis.Error(err, logthis.VERBOSESTEST)
	}
	return &PitchFork{Client: &http.Client{Jar: jar}}
}

func (p *PitchFork) getRequest(url string) (*http.Response, error) {
	if p.Client == nil {
		return nil, errors.New("unconfigured http client")
	}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("User-Agent", userAgent)
	resp, err := p.Client.Do(req)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != http.StatusOK {
		return nil, errors.New("Returned status: " + resp.Status)
	}
	return resp, nil
}

func (p *PitchFork) Search(artist, title string) (*Search, error) {
	queryURL := searchURL + url.QueryEscape(artist+" "+title)
	resp, err := p.getRequest(queryURL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	resultBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	// extract the json
	r := regexp.MustCompile(jsonExtractRegExp)
	if r.MatchString(string(resultBytes)) {
		// cannot error out by construction
		jsonData := "{" + r.FindStringSubmatch(string(resultBytes))[1] + "}"
		var info Search
		err = json.Unmarshal([]byte(jsonData), &info)
		return &info, err
	}
	return nil, errors.New("could not extract json from pitchfork response")
}
