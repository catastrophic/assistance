package goodreads

import "gitlab.com/catastrophic/assistance/ebook"

const apiRoot = "https://www.goodreads.com/"

// response is the top xml element in goodreads response.
type response struct {
	Book   ebook.Metadata `xml:"book"`
	Search searchResults  `xml:"search"`
}

// searchResults is the main xml element in goodreads search.
type searchResults struct {
	ResultsNumber string `xml:"total-results"`
	Works         []work `xml:"results>work"`
}

// works holds the work information in the xml response.
type work struct {
	ID     string `xml:"best_book>id"`
	Author string `xml:"best_book>author>name"`
	Title  string `xml:"best_book>title"`
}
