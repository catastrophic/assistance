package goodreads

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var grBooks = []struct {
	author            string
	title             string
	expectedID        string
	expectedYear      string
	expectedFullTitle string
	isbn              string
	expectedInfo      string
}{
	{
		"Scott Lynch",
		"The Republic of Thieves (Gentleman Bastard, #3)",
		"2890090",
		"2013",
		"Scott Lynch (2013) The Republic of Thieves (Gentleman Bastard, #3) [Gentleman Bastard #3]",
		"9780553804690",
		"Scott Lynch (2013) The Republic of Thieves (Gentleman Bastard, #3) [Hardcover] [First Edition]",
	},
	/*	{
		"George Orwell",
		"Animal Farm",
		"1391951", // wrong first hit by GR...
		"1984",    // should be 1945
		"George Orwell (1945) Animal Farm",
		"9780812034028",
		"David Ball, George Orwell (1984, 2001 edition) George Orwell's Animal Farm []",
	},*/
}

// TestGoodReads tests goodreads search.
func TestGoodReads(t *testing.T) {
	check := assert.New(t)
	// make sure it is set
	key := os.Getenv("GR_API_KEY")
	require.NotEqual(t, 0, len(key), "Cannot get Goodreads API key")
	g := New(key)

	// impossible searches
	md, err := g.GetBook("0")
	check.Nil(md)
	check.NotNil(err)
	idStr, err := g.GetBookIDByQuery("NOOOOPPPEE", "NOPEAGAIN")
	check.Equal("", idStr)
	check.NotNil(err)
	idStr, err = g.GetBookIDByISBN("NOOOOPPPEE")
	check.Equal("", idStr)
	check.NotNil(err)

	for _, book := range grBooks {
		// getting book_id
		bookID, err := g.GetBookIDByQuery(book.author, book.title)
		check.Nil(err, "Unexpected error")
		check.Equal(book.expectedID, bookID, "Bad book id")

		// getting book information from book_id
		b, err := g.GetBook(bookID)
		check.Nil(err, "Unexpected error")

		check.Equal(book.expectedYear, b.OriginalYear, "Bad year")

		// getting book_id by isbn
		bookID, err = g.GetBookIDByISBN(book.isbn)
		check.Nil(err, "Unexpected error")
		check.Equal(book.expectedID, bookID, "Bad book id")

		//getting book info by isbn
		info, err := g.GetBookInfoByISBN(book.isbn)
		check.Nil(err)
		check.Equal(book.expectedInfo, info)
	}

	// testing covers
	apiCover := "https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1549482799l/43459662._SX98_.jpg"
	expectedCover := "https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1549482799l/43459662.jpg"
	check.Equal(expectedCover, LargeImageURL(apiCover))
}
