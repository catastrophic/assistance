package goodreads

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/ebook"
	"gitlab.com/catastrophic/assistance/logthis"
)

// GoodReads retrieves information from goodreads.com.
type GoodReads struct {
	key string
}

// New GoodReads struct.
func New(key string) *GoodReads {
	return &GoodReads{key: key}
}

// GetBook returns a GoodreadsBook from its Goodreads ID.
func (g GoodReads) GetBook(id string) (*ebook.Metadata, error) {
	return g.GetBookSpecificClean(id, nil, nil)
}

// GetBook returns a GoodreadsBook from its Goodreads ID.
func (g GoodReads) GetBookSpecificClean(id string, forbiddenTags []string, tagAliases map[string][]string) (*ebook.Metadata, error) {
	uri := apiRoot + "book/show/" + id + ".xml?key=" + g.key
	r := response{}
	err := getXMLData(uri, &r)
	if r.Book.Title == "" {
		return nil, errors.New("could not find book with id " + id)
	}
	r.Book.CleanSpecific(forbiddenTags, tagAliases)
	return &r.Book, err
}

func (g GoodReads) GetBookID(metadata *ebook.Metadata) (string, error) {
	var grID string
	var err error
	// try looking up goodreads ID using ISBN
	if metadata.ISBN != "" {
		grID, err = g.GetBookIDByISBN(metadata.ISBN)
		if err != nil {
			logthis.Error(err, logthis.VERBOSESTEST)
		}
	}
	// if there is no ISBN or the search failed...
	if grID == "" {
		// TODO generate a complete query with more info
		grID, err = g.GetBookIDByQuery(metadata.Author(), metadata.Title)
		if err != nil {
			logthis.Error(err, logthis.VERBOSESTEST)
		}
	}
	// if still nothing, give up
	if grID == "" {
		return "", errors.New("could not identify epub")
	}
	return grID, err
}

// GetBookIDByQuery gets a Goodreads ID from a query.
func (g GoodReads) GetBookIDByQuery(author, title string) (string, error) {
	uri := apiRoot + "search/index.xml?key=" + g.key + "&q=" + url.QueryEscape(author+" "+title)
	r := response{}
	if err := getXMLData(uri, &r); err != nil {
		return "", err
	}
	// parsing results
	numberOfHits, err := strconv.Atoi(r.Search.ResultsNumber)
	if err != nil {
		return "", err
	}
	if numberOfHits != 0 {
		// TODO: if more than 1 hit, give the user a choice.
		for _, work := range r.Search.Works {
			if work.Author == author && work.Title == title {
				return work.ID, nil
			}
		}
		return r.Search.Works[0].ID, nil
	}
	return "", errors.New("could not find book")
}

// GetBookIDByISBN gets a Goodreads ID from an ISBN.
func (g GoodReads) GetBookIDByISBN(isbn string) (string, error) {
	// checking isbn
	isbnCandidates, err := ebook.CleanISBN(isbn)
	if err != nil {
		return "", errors.Wrap(err, "incorrect ISBN")
	}
	// since the isbn string was provided, assuming it contains only one isbn
	uri := apiRoot + "search/index.xml?key=" + g.key + "&q=" + isbnCandidates[0].ISBN13
	r := response{}
	if err := getXMLData(uri, &r); err != nil {
		return "", err
	}
	// parsing results
	numberOfHits, err := strconv.Atoi(r.Search.ResultsNumber)
	if err != nil {
		return "", err
	}
	if numberOfHits != 0 {
		if numberOfHits > 1 {
			fmt.Println("Got more than 1 hit while searching by ISBN! Returned first hit.")
		}
		return r.Search.Works[0].ID, nil
	}
	return "", errors.New("could not find book with ISBN " + isbnCandidates[0].ISBN13)
}

// GetBookInfoByISBN gets a Goodreads information from an ISBN.
func (g GoodReads) GetBookInfoByISBN(isbn string) (string, error) {
	id, err := g.GetBookIDByISBN(isbn)
	if err != nil {
		return "", err
	}
	metadata, err := g.GetBook(id)
	if err != nil {
		return "", err
	}
	return metadata.Source(), nil
}

// getXMLData retrieves XML responses from online APIs.
func getXMLData(uri string, i interface{}) error {
	currentPass := 0
	const maxTries = 5
	var data []byte
	var err error
	for currentPass < maxTries {
		data, err = getRequest(uri)
		if err != nil {
			currentPass++
			// wait a little
			time.Sleep(5 * time.Second)
		} else {
			break
		}
	}
	// test if the last pass was successful
	if err != nil {
		return err
	}
	return xml.Unmarshal(data, i)
}

func getRequest(uri string) (body []byte, err error) {
	// 10s timeout
	client := http.Client{Timeout: 10 * time.Second}
	res, err := client.Get(uri)
	if err != nil {
		return body, err
	}
	defer res.Body.Close()
	return ioutil.ReadAll(res.Body)
}

// LargeImageURL because the API returns the "medium" version of the cover url. This generates the "large" URL.
func LargeImageURL(url string) string {
	re := regexp.MustCompile(`^(.*)\..+\.jpg$`)
	ids := re.FindAllStringSubmatch(url, -1)
	if len(ids) == 1 && len(ids[0]) == 2 {
		return fmt.Sprintf("%s.jpg", ids[0][1])
	}
	return url
}
