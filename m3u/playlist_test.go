package m3u

import (
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/catastrophic/assistance/fs"
)

func TestPlaylist(t *testing.T) {
	fmt.Println("+ Testing Playlist...")
	check := assert.New(t)

	testPlaylist := "../testing/list.m3u"
	p, err := New(testPlaylist)
	check.Nil(err)
	check.Equal(2, len(p.Contents))
	check.Equal("../testing/sample.flac", p.Contents[0])
	check.Equal(2, len(p.Hashes))
	p.GetHashes("")
	check.Equal("a6a03fc6f643cc110e85ee03029d7426", p.Hashes[0])
	check.Equal("Playlist: ../testing/list.m3u\n- ../testing/sample.flac | a6a03fc6f643cc110e85ee03029d7426\n- ../testing/sample_without_tags.flac | 937d50259c67fd095af5ebca3c0567d4\n", p.String())
	check.Nil(p.Check(""))
	check.NotNil(p.Check("/home"))

	testPlaylist2 := "../testing/list2.m3u"
	_, err2 := New(testPlaylist2)
	check.NotNil(err2)
	// copy reference m3u
	check.Nil(fs.CopyFile(testPlaylist, testPlaylist2, false))
	// load copy
	p2, err2 := New(testPlaylist2)
	check.Nil(err2)

	// add test flac from different root
	check.Nil(p2.AddRelease("..", "testing"))
	check.Equal(9, len(p2.Contents))
	check.Equal("../testing/sample.flac", p2.Contents[0])
	check.Equal("testing/mqa16bit.flac", p2.Contents[2])
	// check m3u update
	p2.Update("testing/", "WHAAT/")
	fakeFile := "WHAAT/mqa16bit.flac"
	check.Equal(9, len(p2.Contents))
	check.Equal("../testing/sample.flac", p2.Contents[0])
	check.Equal(fakeFile, p2.Contents[2])
	// contains
	check.True(p2.Contains("../testing"))
	check.True(p2.Contains("WHAAT"))
	check.False(p2.Contains("testing"))
	check.False(p2.Contains("nope"))
	// save copy m3u
	check.Nil(p2.Save())
	// read and check output
	data, err := ioutil.ReadFile(testPlaylist2)
	check.Nil(err)
	check.Equal("../testing/sample.flac\n../testing/sample_without_tags.flac\nWHAAT/mqa16bit.flac\nWHAAT/mqa24bit.flac\nWHAAT/notmqa.flac\nWHAAT/sample.flac\nWHAAT/sample_with_id3_tags.flac\nWHAAT/sample_with_id3v1_tags.flac\nWHAAT/sample_without_tags.flac", string(data))
	// remove copy
	check.Nil(os.RemoveAll(testPlaylist2))
}
