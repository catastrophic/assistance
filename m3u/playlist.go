package m3u

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/fs"
	"gitlab.com/catastrophic/assistance/music"
	"gitlab.com/catastrophic/assistance/strslice"
)

const (
	ErrorFindingMusic = "directory %s does not contain music files"
)

type Playlist struct {
	Filename string
	Contents []string
	Hashes   []string
}

func New(filename string) (*Playlist, error) {
	if !fs.FileExists(filename) {
		return nil, errors.New("playlist " + filename + " does not exist")
	}
	pl := &Playlist{Filename: filename}

	// open file and get strings
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	files := strings.Split(string(content), "\n")
	for _, f := range files {
		f = strings.TrimSpace(f)
		if f != "" {
			pl.Contents = append(pl.Contents, f)
		}
	}
	pl.Hashes = make([]string, len(pl.Contents))
	return pl, nil
}

func (p *Playlist) String() string {
	txt := fmt.Sprintf("Playlist: %s\n", p.Filename)
	for i, f := range p.Contents {
		txt += fmt.Sprintf("- %s | %s\n", f, p.Hashes[i])
	}
	return txt
}

// Check all files exist, using root if paths are relative.
func (p *Playlist) Check(root string) error {
	for _, f := range p.Contents {
		path := f
		if !filepath.IsAbs(f) {
			path = filepath.Join(root, f)
		}
		if !fs.FileExists(path) {
			return errors.New("at least one file does not exist: " + f)
		}
	}
	return nil
}

// GetHashes from all files exist, using root if paths are relative.
func (p *Playlist) GetHashes(root string) {
	for i, f := range p.Contents {
		path := f
		if !filepath.IsAbs(f) {
			path = filepath.Join(root, f)
		}
		if fs.FileExists(path) {
			p.Hashes[i] = fs.CalculateMD5First4KB(path)
		}
	}
}

func (p *Playlist) Save() error {
	// write if everything is good.
	return ioutil.WriteFile(p.Filename, []byte(strings.Join(p.Contents, "\n")), 0777)
}

func (p *Playlist) Contains(release string) bool {
	// assumes that release is the release folder, relative to the library directory, not a parent.
	for _, f := range p.Contents {
		if strings.HasPrefix(f, release) {
			return true
		}
	}
	return false
}

func (p *Playlist) AddRelease(root, path string) error {
	if !music.ContainsMusic(filepath.Join(root, path)) {
		return fmt.Errorf(ErrorFindingMusic, path)
	}

	// walk path and list all music files, get relative to library directory
	e := filepath.Walk(filepath.Join(root, path), func(subPath string, fileInfo os.FileInfo, walkError error) error {
		if os.IsNotExist(walkError) {
			return nil
		}
		// load all music files
		if strslice.Contains([]string{music.FlacExt, music.Mp3Ext}, filepath.Ext(subPath)) {
			// MPD wants relative paths
			relativePath, err := filepath.Rel(root, subPath)
			if err != nil {
				return err
			}
			p.Contents = append(p.Contents, relativePath)
		}
		return nil
	})
	return e
}

func (p *Playlist) Update(old, new string) {
	// assumes that release is the release folder, relative to the library directory, not a parent.
	for i, f := range p.Contents {
		if strings.HasPrefix(f, old) {
			p.Contents[i] = strings.Replace(f, old, new, -1)
		}
	}
}
