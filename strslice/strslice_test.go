package strslice

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStrSlice(t *testing.T) {
	fmt.Println("+ Testing StrSlice...")
	check := assert.New(t)

	a := []string{"1", "2", "3"}
	b := []string{"2", "3"}
	c := []string{"4", "8", "3"}
	d := []string{"9"}
	e := []string{"A", "b"}
	f := []int{1, 2, 3}
	g := []string{"1", "2", "3", "2", "1", "1", "WRONG"}
	h := []string{"b"}
	i := []string{"B"}
	j := []string{"b", "B"}
	k := []string{"eao", "b", "éâö"}
	l := []string{"b", "éâö", "eao"}
	m := []string{"éâö", "jam"}
	n := []string{"jam"}

	check.True(Contains(a, "1"))
	check.False(Contains(a, "10"))

	check.False(Contains(e, "a"))
	check.True(ContainsCaseInsensitive(e, "a"))
	check.True(ContainsCaseDiacriticsInsensitive(m, "eao"))
	check.False(ContainsCaseDiacriticsInsensitive(m, "fãm"))
	check.True(Contains(e, "A"))
	check.True(ContainsCaseInsensitive(e, "A"))
	check.False(ContainsCaseInsensitive(e, "c"))

	check.True(HasDuplicates(g))
	RemoveDuplicates(&g, "WRONG")
	check.False(HasDuplicates(g))
	check.Equal(a, g)

	RemoveDuplicates(&j)
	check.Equal([]string{"b", "B"}, j)
	RemoveDuplicatesCaseInsensitive(&j)
	check.Equal([]string{"b"}, j)
	RemoveDuplicatesCaseDiacriticsInsensitive(&k)
	check.Equal([]string{"eao", "b"}, k)
	RemoveDuplicatesCaseDiacriticsInsensitive(&l)
	check.Equal([]string{"b", "éâö"}, l)

	intSlice, err := ToIntSlice(a)
	check.Nil(err)
	check.Equal(f, intSlice)
	_, err = ToIntSlice(e)
	check.NotNil(err)

	res1 := Common(a, b)
	check.Equal([]string{"2", "3"}, res1)
	res2 := Common(a, c)
	check.Equal([]string{"3"}, res2)
	res3 := Common(a, d)
	check.Nil(res3)

	t1 := Remove(a, "4")
	check.Equal(a, t1)
	check.Len(t1, 3)
	t2 := Remove(a, "1")
	check.Equal(b, t2)
	check.Len(t2, 2)

	t3 := Remove(e, "a")
	check.Equal(e, t3)
	t4 := RemoveCaseInsensitive(e, "a")
	check.Equal(h, t4)

	t5 := Remove(m, "eao")
	check.Equal(m, t5)
	t6 := RemoveCaseDiacriticsInsensitive(m, "eao")
	check.Equal(n, t6)

	check.True(Equal(a, a))
	check.False(Equal(a, b))
	check.True(EqualCaseInsensitive(a, a))
	check.False(EqualCaseInsensitive(a, b))

	check.True(EqualCaseInsensitive(a, a))
	check.True(EqualCaseInsensitive(h, i))
	check.False(Equal(h, i))
}
