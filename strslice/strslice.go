// Package strslice provides a few helper functions for dealing with []string.
package strslice

import (
	"strconv"
	"strings"
	"unicode"

	"golang.org/x/text/runes"
	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
)

// Contains checks if a string is in a []string, returns bool.
func Contains(list []string, a string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// ContainsCaseInsensitive checks if a string is in a []string, regardless of case.
func ContainsCaseInsensitive(list []string, a string) bool {
	for _, b := range list {
		if strings.EqualFold(a, b) {
			return true
		}
	}
	return false
}

// ContainsCaseDiacriticsInsensitive checks if a string is in a []string, regardless of case or diacritics.
func ContainsCaseDiacriticsInsensitive(list []string, a string) bool {
	for _, b := range list {
		if strings.EqualFold(removeDiacritics(a), removeDiacritics(b)) {
			return true
		}
	}
	return false
}

func ToIntSlice(in []string) ([]int, error) {
	var err error
	b := make([]int, len(in))
	for i, v := range in {
		b[i], err = strconv.Atoi(v)
		if err != nil {
			return []int{}, err
		}
	}
	return b, nil
}

func Common(a, b []string) []string {
	m := make(map[string]bool)
	for _, y := range b {
		m[y] = true
	}
	var ret []string
	for _, x := range a {
		if m[x] {
			ret = append(ret, x)
		}
	}
	return ret
}

// RemoveDuplicates in []string.
func RemoveDuplicates(options *[]string, otherStringsToClean ...string) {
	found := make(map[string]bool)
	// specifically remove other strings from values
	for _, o := range otherStringsToClean {
		found[o] = true
	}
	j := 0
	for i, x := range *options {
		if !found[x] && x != "" {
			found[x] = true
			(*options)[j] = (*options)[i]
			j++
		}
	}
	*options = (*options)[:j]
}

// RemoveDuplicatesCaseInsensitive in []string.
func RemoveDuplicatesCaseInsensitive(options *[]string, otherStringsToClean ...string) {
	found := make(map[string]bool)
	// specifically remove other strings from values
	for _, o := range otherStringsToClean {
		found[strings.ToLower(o)] = true
	}
	j := 0
	for i, x := range *options {
		if !found[strings.ToLower(x)] && x != "" {
			found[strings.ToLower(x)] = true
			(*options)[j] = (*options)[i]
			j++
		}
	}
	*options = (*options)[:j]
}

func removeDiacritics(s string) string {
	t := transform.Chain(norm.NFD, runes.Remove(runes.In(unicode.Mn)), norm.NFC)
	res, _, _ := transform.String(t, s)
	return res
}

// RemoveDuplicatesCaseDiacriticsInsensitive in []string.
func RemoveDuplicatesCaseDiacriticsInsensitive(options *[]string, otherStringsToClean ...string) {
	found := make(map[string]bool)
	// specifically remove other strings from values
	for _, o := range otherStringsToClean {
		found[strings.ToLower(o)] = true
	}
	j := 0
	for i, x := range *options {
		if !found[strings.ToLower(x)] && !found[strings.ToLower(removeDiacritics(x))] && x != "" {
			found[strings.ToLower(x)] = true
			found[strings.ToLower(removeDiacritics(x))] = true
			(*options)[j] = (*options)[i]
			j++
		}
	}
	*options = (*options)[:j]
}

// Remove a particular string.
func Remove(s []string, r string) []string {
	for i, v := range s {
		if v == r {
			return append(s[:i], s[i+1:]...)
		}
	}
	return s
}

// RemoveCaseInsensitive a particular string.
func RemoveCaseInsensitive(s []string, r string) []string {
	for i, v := range s {
		if strings.EqualFold(v, r) {
			return append(s[:i], s[i+1:]...)
		}
	}
	return s
}

// RemoveCaseDiacriticsInsensitive a particular string.
func RemoveCaseDiacriticsInsensitive(s []string, r string) []string {
	for i, v := range s {
		if strings.EqualFold(removeDiacritics(v), removeDiacritics(r)) {
			return append(s[:i], s[i+1:]...)
		}
	}
	return s
}

// Equal tells whether a and b contain the same elements.
func Equal(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

// Equal tells whether a and b contain the same elements, regardless of case.
func EqualCaseInsensitive(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if !strings.EqualFold(v, b[i]) {
			return false
		}
	}
	return true
}

// HasDuplicates in []string.
func HasDuplicates(options []string) bool {
	found := make(map[string]int)
	for _, x := range options {
		found[x]++
		if found[x] > 1 {
			return true
		}
	}
	return false
}
