module gitlab.com/catastrophic/assistance

require (
	github.com/bogem/id3v2 v1.1.1
	github.com/btcsuite/btcutil v1.0.2
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/disintegration/imaging v1.6.2
	github.com/go-flac/flacpicture v0.2.0
	github.com/go-flac/go-flac v0.3.1
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0 // indirect
	github.com/kennygrant/sanitize v1.2.4
	github.com/kr/text v0.2.0 // indirect
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/mewkiz/flac v1.0.6
	github.com/mewkiz/pkg v0.0.0-20190919212034-518ade7978e2
	github.com/mgutz/ansi v0.0.0-20170206155736-9520e82c474b
	github.com/moraes/isbn v0.0.0-20151007102746-e6388fb1bfd5
	github.com/mozillazg/go-unidecode v0.1.1
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/pkg/errors v0.8.1
	github.com/sevlyar/go-daemon v0.1.5
	github.com/stretchr/testify v1.5.1
	github.com/tj/go-spin v1.1.0
	gitlab.com/catastrophic/gotabulate v0.0.0-20190228104527-d3d77fbbb3a1
	golang.org/x/crypto v0.0.0-20200115085410-6d4e4cb37c7d
	golang.org/x/image v0.0.0-20191009234506-e7c1f5e7dbb8
	golang.org/x/net v0.0.0-20200226121028-0de0cce0169b
	golang.org/x/sys v0.0.0-20191120155948-bd437916bb0e // indirect
	golang.org/x/text v0.3.2
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)

go 1.13
