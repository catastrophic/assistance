package discogs

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/logthis"
	"golang.org/x/net/publicsuffix"
)

const (
	inconspicuousUserAgent = "Mozilla/5.0 (X11; Linux i686; rv:64.0) Gecko/20100101 Firefox/64.0"
	discogsAPISearchURL    = "https://api.discogs.com/database/search"
	discogsAPIReleaseURL   = "https://api.discogs.com/releases/"
	discogsAPIMasterURL    = "https://api.discogs.com/masters/"
	discogsMasterURL       = "https://www.discogs.com/master/"
	discogsReleaseURL      = "https://www.discogs.com/release/"

	errorLogin = "could not log in, check API key"
)

// Discogs retrieves information about a release on Discogs.
type Discogs struct {
	Token  string
	Client *http.Client
}

// New set up with Discogs API authorization info.
func New(token string) (*Discogs, error) {
	options := cookiejar.Options{
		PublicSuffixList: publicsuffix.List,
	}
	jar, err := cookiejar.New(&options)
	if err != nil {
		logthis.Error(errors.Wrap(err, errorLogin), logthis.NORMAL)
		return nil, err
	}
	return &Discogs{Token: token, Client: &http.Client{Jar: jar}}, nil
}

func (d *Discogs) getRequest(url string) ([]byte, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("User-Agent", inconspicuousUserAgent)
	req.Header.Add("Authorization", "Discogs token="+d.Token)
	resp, err := d.Client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, errors.New("Returned status: " + resp.Status)
	}
	return ioutil.ReadAll(resp.Body)
}

// GetRelease on Discogs and retrieve its information.
func (d *Discogs) GetRelease(id int) (*Release, error) {
	resultBytes, err := d.getRequest(fmt.Sprintf("%s/%d", discogsAPIReleaseURL, id))
	if err != nil {
		return nil, err
	}
	var info Release
	err = json.Unmarshal(resultBytes, &info)
	return &info, err
}

// GetMaster on Discogs and retrieve its information.
func (d *Discogs) GetMaster(id int) (*Master, error) {
	resultBytes, err := d.getRequest(fmt.Sprintf("%s/%d", discogsAPIMasterURL, id))
	if err != nil {
		return nil, err
	}
	var info Master
	err = json.Unmarshal(resultBytes, &info)
	return &info, err
}

// Search releases on Discogs and retrieve their information.
func (d *Discogs) Search(artist, release string, year int, label, catalogNumber, source, releaseType string) (*Results, error) {
	// search
	searchURL, err := url.Parse(discogsAPISearchURL)
	if err != nil {
		return nil, err
	}
	q := searchURL.Query()
	q.Set("type", "release")
	q.Set("artist", artist)
	q.Set("release_title", release)
	if year != 0 {
		q.Set("year", strconv.Itoa(year))
	}
	if label != "" {
		q.Set("label", label)
	}
	if catalogNumber != "" {
		q.Set("catno", fmt.Sprintf("\"%s\"", catalogNumber))
	}
	var format []string
	if source != "" {
		format = append(format, source)
	}
	if releaseType != "" {
		// anthology and compilation have different meanings on discogs...
		if strings.EqualFold(releaseType, "anthology") {
			releaseType = "compilation"
		}
		format = append(format, releaseType)
	}
	q.Set("format", strings.Join(format, "|"))
	searchURL.RawQuery = q.Encode()

	resultDCBytes, err := d.getRequest(searchURL.String())
	if err != nil {
		return nil, err
	}
	var info Results
	err = json.Unmarshal(resultDCBytes, &info)
	return &info, err
}
