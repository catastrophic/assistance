package discogs

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/catastrophic/assistance/deezer"
	"gitlab.com/catastrophic/assistance/logthis"
)

func TestDiscogs(t *testing.T) {
	fmt.Println("+ Testing Discogs...")
	check := assert.New(t)
	// setup logger
	logthis.SetLevel(3)

	// get token from env
	// token can be generated in Discogs user settings
	key := os.Getenv("DISCOGS_TOKEN")
	if key == "" {
		fmt.Println("could not retrieve discogs token from env, not able to run the associated tests.")
		return
	}

	d, err := New(key)
	check.Nil(err)

	fmt.Println("Getting info for release 249504")
	r, err := d.GetRelease(249504)
	check.Nil(err)
	check.Equal("Never Gonna Give You Up", r.Title)
	check.Equal("https://www.discogs.com/master/96559", r.MainURL())
	/*
		s, err := json.MarshalIndent(r, "", "    ")
		check.Nil(err)
		fmt.Println(string(s))
	*/
	fmt.Println("Searching for an Alice Coltraine album")
	res, err := d.Search("Alice Coltrane", "Spiritual Eternal: The Complete Warner Bros. Studio Recordings", 2018, "Real Gone Music", "RGM-0692", "CD", "Album")
	check.Nil(err)
	check.Equal(1, res.Pagination.Items) // got 1 result
	check.Equal(12595789, res.Results[0].ID)
	/*
		result := res.Results[0]
		s, err = json.MarshalIndent(result, "", "    ")
		check.Nil(err)
		fmt.Println(string(s))
	*/
	// compilation
	fmt.Println("Getting info for compilation 8364615")
	r, err = d.GetRelease(8364615)
	check.Nil(err)
	check.Equal("Un Printemps 2016 - Volume 2", r.Title)
	check.Equal("https://www.discogs.com/release/8364615", r.MainURL())

	// testing using a deezer ID as source
	fmt.Println("Getting info from deezer ID")
	dz := deezer.New("", "")
	a, err := dz.GetRelease(14880741)
	check.Nil(err)

	//res, err = d.Search(a.Artist.Name, a.Title, a.ReleaseYear(), a.Label, a.Upc, "", a.RecordType)
	// searching using limited information, because deezer and discogs info don't really match
	res, err = d.Search(a.Artist.Name, a.Title, a.ReleaseYear(), "", "", "", "")
	check.Nil(err)
	check.Len(res.Results, 50)
	// getting release info from discogs
	fmt.Println(res.Results[0])
	fmt.Println(res.Results[0].MasterID)
	m, err := d.GetMaster(res.Results[0].MasterID)
	check.Nil(err)
	fmt.Println(m.BBCode())

	r, err = d.GetRelease(res.Results[0].ID)
	check.Nil(err)
	check.Equal("https://www.discogs.com/master/21501", r.MainURL())
	fmt.Println(r.BBCode())
}
