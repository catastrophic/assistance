package ebook

import (
	"errors"
	"fmt"
	"html"
	"regexp"
	"strings"

	"github.com/moraes/isbn"
	"gitlab.com/catastrophic/assistance/strslice"
	"gitlab.com/catastrophic/assistance/ui"
)

var (
	// replacing this match until the string stays unchanged will remove all spaces and - between numbers & between
	// numbers and the letter X.
	// it should ready the string for matching with the second regexp.
	cleanUpRegExp = regexp.MustCompile(`(\d)[ -]+([\dX])`)
	// cleans up embedded CSS which certainly does not contain ISBN and can contain false positives
	cssCleanUpRegExp = regexp.MustCompile(`(?s)<style type="text/css">.*</style>`)
	// cleans up HTML tags
	htmlTagsCleanUpRegExp = regexp.MustCompile(`(?i)</*[p|div|table|tr|td|h1|h2|h3|h4|h5|a|svg][^>]*>`)
	// cleans up UUIDs
	uuidCleanUpRegExp = regexp.MustCompile("[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}")
	// cleans up whitespaces
	whitespaceCleanUpRegExp = regexp.MustCompile(`\s+`)
	// detecting ISBNs
	isbnRegExp = regexp.MustCompile(`(97(8|9))?\d{9}(\d|X)`)
)

const (
	MaxISBNContextLines = 20
)

type ISBNs []ISBN

func (is *ISBNs) HasISBN13(isbn string) (bool, int) {
	for idx, i := range *is {
		if i.ISBN13 == isbn {
			return true, idx
		}
	}
	return false, -1
}

func (is *ISBNs) Len() int {
	return len(*is)
}

func (is *ISBNs) Add(i ISBN) {
	found, idx := is.HasISBN13(i.ISBN13)
	if found {
		// fuse entries for a sane number of examples from the same sources
		if len((*is)[idx].Context) < MaxISBNContextLines {
			(*is)[idx].Context = append((*is)[idx].Context, i.Context...)
		}
		if i.Location != "" && !strings.Contains((*is)[idx].Location, i.Location) {
			(*is)[idx].Location += ", " + i.Location
		}
	} else {
		*is = append(*is, i)
	}
}

//-----------------------------------------------------------------------------

type ISBN struct {
	ISBN10   string
	ISBN13   string
	Location string
	Context  []string
}

func (i ISBN) String() string {
	return ui.GreenBold(i.ISBN13) + " -- " + i.Info()
}

func (i ISBN) Info() string {
	var txt string
	var context string
	if len(i.Context) != 0 {
		context = strings.Join(i.Context, "...\n\t ...")
	}
	if i.Location == "" && context == "" {
		return txt
	}
	if context == "" {
		return txt + "\n\t " + ui.YellowUnderlined("Found in: "+i.Location)
	}
	return txt + "\n\t " + ui.YellowUnderlined("Found in: "+i.Location) + "\n\t ..." + context + "..."
}

// CleanISBN from a string.
func CleanISBN(full string) ([]ISBN, error) {
	candidate := html.UnescapeString(full)
	// removing html tags
	candidate = htmlTagsCleanUpRegExp.ReplaceAllString(candidate, " ")
	// removing css styles, if any
	candidate = cssCleanUpRegExp.ReplaceAllString(candidate, "")
	// removing uuids
	candidate = uuidCleanUpRegExp.ReplaceAllString(candidate, "")
	// replacing until isbn should have the right form
	for candidate != cleanUpRegExp.ReplaceAllString(candidate, "$1$2") {
		candidate = cleanUpRegExp.ReplaceAllString(candidate, "$1$2")
	}
	// removing extra whitespaces
	candidate = whitespaceCleanUpRegExp.ReplaceAllString(candidate, " ")

	// trying all candidates
	var candidates []ISBN
	var found []string
	for _, c := range isbnRegExp.FindAllString(candidate, -1) {
		// validate and convert to ISBN13 if necessary
		if isbn.Validate(c) {
			var context []string

			// getting context, ignoring cases where the ISBN candidate is surrounded by / or _ (probably filenames inside a file)
			// such cases are still added, but without context.
			contextRegExp, err := regexp.Compile("(?s).{0,30}[^/_]" + c + "[^/_].{0,30}")
			if err != nil {
				return candidates, err
			}

			for _, r := range contextRegExp.FindAllString(candidate, -1) {
				if len(r) != 0 && r != c {
					t := strings.Replace(r, c, ui.GreenBold(c), -1)
					t = strings.Replace(t, "\n", " ", -1)
					t = strings.Replace(t, "\r", " ", -1)
					context = append(context, t)
				}
			}
			switch len(c) {
			case 10:
				// an ISBN10 with all similar number is valid. It's very probably not this epub's ISBN though.
				if strings.Count(c, fmt.Sprintf("%c", c[0])) == 10 {
					continue
				}
				// convert to isbn13
				isbn13, err := isbn.To13(c)
				if err == nil && !strslice.Contains(found, isbn13) {
					candidates = append(candidates, ISBN{ISBN10: c, ISBN13: isbn13, Context: context})
					found = append(found, isbn13)
				}
			case 13:
				if !strslice.Contains(found, c) {
					candidates = append(candidates, ISBN{ISBN13: c, Context: context})
					found = append(found, c)
				}
			}
		}
	}
	if len(candidates) == 0 {
		return candidates, errors.New("ISBN-13 not found")
	}
	return candidates, nil
}

// AskForISBN when not found in epub.
func AskForISBN() (string, error) {
	if ui.Accept("Do you want to enter an ISBN manually") {
		errs := 0
		for {
			fmt.Print("Enter ISBN: ")
			choice, scanErr := ui.GetInput(nil)
			if scanErr != nil {
				return "", scanErr
			}
			// check valid ISBN
			isbnCandidate, err := CleanISBN(choice)
			if err != nil {
				errs++
				ui.Warning("Warning: Invalid value.")
			} else {
				confirmed := ui.Accept("Confirm: " + isbnCandidate[0].ISBN13)
				if confirmed {
					return isbnCandidate[0].ISBN13, nil
				}
				errs++
				fmt.Println("Manual entry not confirmed, trying again.")
			}
			if errs > 5 {
				ui.Warning("Too many errors, continuing without ISBN.")
				break
			}
		}
	}
	return "", errors.New("ISBN not set")
}
