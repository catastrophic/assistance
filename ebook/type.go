package ebook

import (
	"errors"
	"strings"

	"gitlab.com/catastrophic/assistance/strslice"
)

const (
	// type values
	essay         = "essay"
	biography     = "biography"
	autobiography = "autobiography"
	novel         = "novel"
	shortstory    = "shortstory"
	anthology     = "anthology"
	poetry        = "poetry"
)

var validTypes = []string{essay, biography, autobiography, novel, shortstory, anthology, poetry}

// typeAliases replaces category tags with the canonical version.
var typeAliases = map[string][]string{
	essay:         {"essay"},
	biography:     {"biography"},
	autobiography: {"autobiography"},
	novel:         {"novel"},
	shortstory:    {"shortstory", "short story", "short-story", "novella", "short-stories", "shortstories"},
	anthology:     {"anthology", "anthologies"},
	poetry:        {"poetry", "poems"},
}

func CleanType(typ string) (string, error) {
	clean := strings.TrimSpace(strings.ToLower(typ))
	// reducing to main alias
	for mainAlias, aliasList := range typeAliases {
		if strslice.Contains(aliasList, clean) {
			clean = mainAlias
			break
		}
	}
	// testing if valid
	if !strslice.Contains(validTypes, clean) {
		return "", errors.New("Invalid type " + typ)
	}
	return clean, nil
}
