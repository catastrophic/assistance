package ebook

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"

	"gitlab.com/catastrophic/assistance/strslice"
	"gitlab.com/catastrophic/assistance/ui"
)

const (
	titleField       = "title"
	descriptionField = "description"
	isbnField        = "isbn"
	yearField        = "year"
	editionYearField = "edition_year"
	editionInfoField = "edition_info"
	authorField      = "author"
	translatorField  = "translator"
	editorField      = "editor"
	contributorField = "contributor"
	formatField      = "format"
	publisherField   = "publisher"
	tagsField        = "tags"
	seriesField      = "series"
	languageField    = "language"
	categoryField    = "category"
	typeField        = "type"
	genreField       = "genre"
	numPagesField    = "num_pages"
	// user metadata
	progressField = "progress"
	readDateField = "readdate"
	ratingField   = "rating"
	reviewField   = "review"

	authorUsage      = "Authors can be edited as a comma-separated list of strings."
	translatorUsage  = "Translators can be edited as a comma-separated list of strings."
	editorUsage      = "Editors can be edited as a comma-separated list of strings."
	contributorUsage = "Contributors can be edited as a comma-separated list of strings."
	categoryUsage    = "A book can be either fiction or nonfiction."
	typeUsage        = "The nature of this book."
	tagsUsage        = "Tags can be edited as a comma-separated list of strings."
	seriesUsage      = "Series can be edited as a comma-separated list of 'series name:index' strings. Index can be empty, or a range."
	yearUsage        = "The year in which the book was written."
	editionYearUsage = "The year in which this edition was published."
	publisherUsage   = "Publisher of this edition."
	languageUsage    = "Language of this edition."
	genreUsage       = "Main genre of this book."
	isbnUsage        = "ISBN13 for this edition."
	titleUsage       = "Title, without series information."
	descriptionUsage = "Description for this edition."
	progressUsage    = "Your progress for this book: unread, shortlisted, reading or read."
	readDateUsage    = "When you finished reading this book."
	ratingUsage      = "Give a rating between 0 and 5."
	reviewUsage      = "Your review of this book."

	invalidFieldError          = "invalid field: %s"
	couldNotRetrieveValueError = "could not retrieve value"

	LocalSource  = "Epub"
	OnlineSource = "Online"
)

// MetadataFieldNames is a list of valid field names.
var MetadataFieldNames = []string{authorField, translatorField, editorField, contributorField, titleField, yearField, editionYearField, editionInfoField, isbnField, publisherField, descriptionField, languageField, categoryField, typeField, genreField, tagsField, seriesField, formatField, numPagesField}
var metadataFieldMap = map[string]string{
	authorField:      "Authors",
	translatorField:  "Translators",
	editorField:      "Editors",
	contributorField: "Contributors",
	titleField:       "Title",
	yearField:        "OriginalYear",
	editionYearField: "EditionYear",
	editionInfoField: "EditionInfo",
	publisherField:   "Publisher",
	descriptionField: "Description",
	languageField:    "Language",
	categoryField:    "Category",
	typeField:        "Type",
	genreField:       "Genre",
	tagsField:        "Tags",
	seriesField:      "Series",
	isbnField:        "ISBN",
	numPagesField:    "NumPages",
	formatField:      "Format",
}

var usageMap = map[string]string{
	tagsField:        tagsUsage,
	seriesField:      seriesUsage,
	authorField:      authorUsage,
	translatorField:  translatorUsage,
	editorField:      editorUsage,
	contributorField: contributorUsage,
	yearField:        yearUsage,
	editionYearField: editionYearUsage,
	languageField:    languageUsage,
	categoryField:    categoryUsage,
	typeField:        typeUsage,
	genreField:       genreUsage,
	isbnField:        isbnUsage,
	titleField:       titleUsage,
	descriptionField: descriptionUsage,
	publisherField:   publisherUsage,
	progressField:    progressUsage,
	readDateField:    readDateUsage,
	ratingField:      ratingUsage,
	reviewField:      reviewUsage,
}

// GetInfo returns a table with relevant information about a book.
func (m *Metadata) GetInfo(fields ...string) [][]string {
	if len(fields) == 0 {
		// select all fields
		fields = MetadataFieldNames
	}
	var rows [][]string
	for _, field := range fields {
		switch field {
		case numPagesField:
			if m.NumPages != "" {
				rows = append(rows, []string{"Number of pages", m.NumPages})
			}
		case yearField, editionYearField:
			value, err := m.Get(field)
			if err != nil {
				value = unknownYear
			}
			title := "Publication Year"
			if field == yearField {
				title = "Original " + title
			}
			rows = append(rows, []string{title, value})
		default:
			value, err := m.Get(field)
			if err != nil {
				value = couldNotRetrieveValueError
			}
			rows = append(rows, []string{metadataFieldMap[field], value})
		}
	}
	return rows
}

// Get Metadata field value.
func (m *Metadata) Get(field string) (string, error) {
	// get value
	publicFieldName, structField, err := getField(m, metadataFieldMap, field)
	if err != nil {
		return "", err
	}
	// make sure to only return a string
	switch publicFieldName {
	case tagsField:
		return m.Tags.String(), nil
	case seriesField:
		return m.Series.rawString(), nil
	case authorField:
		return m.Author(), nil
	case translatorField:
		return m.Translator(), nil
	case editorField:
		return m.Editor(), nil
	case contributorField:
		return m.Contributor(), nil
	default:
		return structField.String(), nil
	}
}

// return public field name, field, canSet(), error.
func getField(i interface{}, fieldMap map[string]string, name string) (string, reflect.Value, error) {
	structFieldName := ""
	publicFieldName := ""

	// try to find struct name from public name
	for k, v := range fieldMap {
		if v == name || k == name {
			structFieldName = v
			publicFieldName = k
		}
	}
	if structFieldName == "" {
		// nothing was found, invalid field
		return "", reflect.Value{}, fmt.Errorf(invalidFieldError, name)
	}
	structField := reflect.ValueOf(i).Elem().FieldByName(structFieldName)
	return publicFieldName, structField, nil
}

// Diff returns differences between Metadatas.
func (m *Metadata) Diff(o *Metadata, diffOnly bool) [][]string {
	var rows [][]string
	for _, field := range MetadataFieldNames {
		iValue, err := m.Get(field)
		if err != nil {
			iValue = unknown
		}
		oValue, err := o.Get(field)
		if err != nil {
			oValue = unknown
		}
		if !diffOnly || iValue != oValue {
			rows = append(rows, []string{metadataFieldMap[field], iValue, oValue})
		}
	}
	return rows
}

// Merge with another Metadata.
func (m *Metadata) Merge(o *Metadata, diffOnly bool) (err error) {
	for _, field := range MetadataFieldNames {
		err = m.MergeField(o, field, diffOnly)
		if err != nil {
			return
		}
	}
	// automatically fill fields usually not found in epubs.
	m.ImageURL = o.ImageURL
	m.NumPages = o.NumPages
	m.Clean()
	return
}

func (m *Metadata) replaceAuthorsWithRole(newAuthorNames []string, role string) {
	var newAuthors []Author
	// keeping Authors with different roles (translator, etc)
	for _, a := range m.Authors {
		if a.Role != role {
			newAuthors = append(newAuthors, a)
		}
	}
	// adding new author names
	for _, a := range newAuthorNames {
		name := strings.TrimSpace(a)
		if name != "" {
			newAuthors = append(newAuthors, Author{Name: name, Role: role})
		}
	}
	m.Authors = newAuthors
}

// Set Metadata field with a string value.
func (m *Metadata) Set(field, value string) error {
	publicFieldName, structField, err := getField(m, metadataFieldMap, field)
	if err != nil {
		return err
	}
	// set value
	switch publicFieldName {
	case tagsField:
		value = strings.ToLower(value)
		m.Tags = Tags{}
		m.Tags.AddFromNames(strings.Split(value, ",")...)
	case seriesField:
		m.Series = Series{}
		if value != "" {
			for _, s := range strings.Split(value, ",") {
				if _, err := m.Series.AddFromString(s); err != nil {
					return err
				}
			}
		}
	case authorField:
		m.replaceAuthorsWithRole(strings.Split(value, ","), "")
	case translatorField:
		m.replaceAuthorsWithRole(strings.Split(value, ","), "Translator")
	case editorField:
		m.replaceAuthorsWithRole(strings.Split(value, ","), "Editor")
	case contributorField:
		m.replaceAuthorsWithRole(strings.Split(value, ","), "Contributor")
	case yearField, editionYearField:
		// check it's a correct year
		_, err := strconv.Atoi(value)
		if err != nil {
			return errors.New("invalid year value: " + value)
		}
		structField.SetString(value)
	case isbnField:
		// check it's a correct isbn
		isbn, err := CleanISBN(value)
		if err != nil {
			return err
		}
		// using first value
		// TODO check
		structField.SetString(isbn[0].ISBN13)
	case categoryField:
		cleanCategory, err := CleanCategory(value)
		if err != nil {
			return err
		}
		structField.SetString(cleanCategory)
	case typeField:
		cleanType, err := CleanType(value)
		if err != nil {
			return err
		}
		structField.SetString(cleanType)
	case descriptionField:
		structField.SetString(CleanDescription(value))
	case languageField:
		structField.SetString(CleanLanguage(value))
	default:
		structField.SetString(value)
	}
	return nil
}

// MergeField with another Metadata.
func (m *Metadata) MergeField(o *Metadata, field string, diffOnly bool) error {
	var userInput string
	var options []string
	usage, ok := usageMap[field]
	if !ok {
		usage = ""
	}
	currentValue, err := m.Get(field)
	if err != nil {
		return err
	}
	otherValue, err := o.Get(field)
	if err != nil {
		return err
	}
	// if only merging the differences and values are the same, just return
	if diffOnly && currentValue == otherValue {
		return nil
	}

	switch field {
	case yearField, editionYearField:
		listLocalAndRemoteOnly(currentValue, otherValue, &options, unknownYear)
		title := "Publication year"
		if field == yearField {
			title = "Original " + title
		}
		userInput, err = ui.SelectOptionalValue(title, usage, options)
	case languageField:
		listLocalAndRemoteOnly(CleanLanguage(m.Language), CleanLanguage(o.Language), &options, unknown)
		userInput, err = ui.SelectOptionalValue(strings.Title(field), usage, options)
	case categoryField:
		tags := make(map[string]string, 2)
		if m.Category == o.Category {
			tags[m.Category] = ui.Green(LocalSource + " & " + OnlineSource)
		} else {
			tags[m.Category] = ui.Green(LocalSource)
			tags[o.Category] = ui.Green(OnlineSource)
		}
		options = append(options, validCategories...)
		strslice.RemoveDuplicates(&options, unknown)
		userInput, err = ui.SelectAmongOptions(strings.Title(field), usage, options, tags)
	case typeField:
		tags := make(map[string]string, 2)
		if m.Type == o.Type {
			tags[m.Type] = ui.Green(LocalSource + " & " + OnlineSource)
		} else {
			tags[m.Type] = ui.Green(LocalSource)
			tags[o.Type] = ui.Green(OnlineSource)
		}
		options = append(options, validTypes...)
		strslice.RemoveDuplicates(&options, unknown)
		userInput, err = ui.SelectAmongOptions(strings.Title(field), usage, options, tags)
	case descriptionField:
		listLocalAndRemoteOnly(CleanDescription(m.Description), CleanDescription(o.Description), &options, unknown)
		userInput, err = ui.SelectLongOptionalValue(strings.Title(field), usage, options)
	case isbnField:
		listLocalAndRemoteOnly(currentValue, otherValue, &options, unknown)
		userInput, err = ui.SelectValue(strings.Title(field), usage, options)
	default:
		listLocalAndRemoteOnly(currentValue, otherValue, &options, unknown)
		userInput, err = ui.SelectOptionalValue(strings.Title(field), usage, options)
	}
	// checking selectOption err
	if err != nil {
		return err
	}

	// set the field
	err = m.Set(field, userInput)
	if err != nil {
		return err
	}
	m.Clean()
	return nil
}

func listLocalAndRemoteOnly(local, online string, options *[]string, thingsToClean ...string) {
	*options = append(*options, local, online)
	strslice.RemoveDuplicates(options, thingsToClean...)
}
