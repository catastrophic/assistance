package ebook

import (
	"errors"
	"strings"

	"gitlab.com/catastrophic/assistance/strslice"
)

const (
	// category values
	fiction    = "fiction"
	nonfiction = "nonfiction"
)

var validCategories = []string{fiction, nonfiction}

// categoryAliases replaces category tags with the canonical version.
var categoryAliases = map[string][]string{
	fiction:    {"fiction", "fic"},
	nonfiction: {"non fiction", "non-fiction", "nonfiction", "nonfic"},
}

func CleanCategory(category string) (string, error) {
	clean := strings.TrimSpace(strings.ToLower(category))
	// reducing to main alias
	for mainAlias, aliasList := range categoryAliases {
		if strslice.Contains(aliasList, clean) {
			clean = mainAlias
			break
		}
	}
	// testing if valid
	if !strslice.Contains(validCategories, clean) {
		return "", errors.New("Invalid category " + category)
	}
	return clean, nil
}
