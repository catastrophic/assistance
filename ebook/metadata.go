package ebook

import (
	"fmt"
	"strings"
)

const (
	unknown     = "Unknown"
	unknownYear = "XXXX"
)

type Author struct {
	Name string `json:"name" xml:"name"`
	Role string `json:"role" xml:"role"`
}

// Metadata contains all of the known book metadata.
type Metadata struct {
	Title        string   `json:"title" xml:"title"`
	Authors      []Author `json:"authors" xml:"authors>author"`
	ISBN         string   `json:"isbn" xml:"isbn13"`
	Format       string   `json:"format" xml:"format"`
	OriginalYear string   `json:"year" xml:"work>original_publication_year"`
	EditionYear  string   `json:"edition_year" xml:"publication_year"`
	EditionInfo  string   `json:"edition_information" xml:"edition_information"`
	NumPages     string   `json:"num_pages" xml:"num_pages"`
	Language     string   `json:"language" xml:"language_code"`
	Publisher    string   `json:"publisher" xml:"publisher"`
	Description  string   `json:"description" xml:"description"`
	Series       Series   `json:"series" xml:"series_works>series_work"`
	Tags         Tags     `json:"tags" xml:"popular_shelves>shelf"`
	Category     string   `json:"category"`
	Type         string   `json:"type"`
	Genre        string   `json:"genre"`
	ImageURL     string   `json:"image_url" xml:"image_url"`
}

// String returns a representation of Metadata.
func (m *Metadata) String() string {
	if len(m.Series) != 0 {
		return fmt.Sprintf("%s (%s) %s [%s]", m.Author(), m.OriginalYear, m.Title, m.MainSeries().String())
	}
	return fmt.Sprintf("%s (%s) %s", m.Author(), m.OriginalYear, m.Title)
}

// Source returns a representation of Metadata.
func (m *Metadata) Source() string {
	edition := m.EditionYear
	if m.OriginalYear != m.EditionYear {
		edition = m.OriginalYear + ", " + m.EditionYear + " edition"
	}
	txt := fmt.Sprintf("%s (%s) %s [%s]", m.Author(), edition, m.Title, m.Format)
	if m.EditionInfo != "" {
		return txt + " [" + m.EditionInfo + "]"
	}
	return txt
}

func (m *Metadata) filterAuthors(role string) string {
	if len(m.Authors) != 0 {
		var filtered []string
		for _, a := range m.Authors {
			if a.Role == role {
				filtered = append(filtered, a.Name)
			}
		}
		return strings.Join(filtered, ", ")
	}
	return ""
}

// Author returns Metadata's main author.
func (m *Metadata) Author() string {
	a := m.filterAuthors("")
	if a == "" {
		return unknown
	}
	return a
}

// Contributor returns Metadata's Contributor(s).
func (m *Metadata) Contributor() string {
	return m.filterAuthors("Contributor")
}

// Editor returns Metadata's Editor(s).
func (m *Metadata) Editor() string {
	return m.filterAuthors("Editor")
}

// Translator returns Metadata's Translator(s).
func (m *Metadata) Translator() string {
	return m.filterAuthors("Translator")
}

// MainSeries return the main Series of Metadata.
func (m *Metadata) MainSeries() SingleSeries {
	if len(m.Series) != 0 {
		return m.Series[0]
	}
	return SingleSeries{}
}

// Clean cleans up the Metadata.
func (m *Metadata) Clean() {
	m.CleanSpecific(nil, nil)
}

// CleanSpecific cleans up the Metadata with specific lists of forbidden tags or aliases.
func (m *Metadata) CleanSpecific(forbiddenTags []string, tagAliases map[string][]string) {
	// default year
	if m.OriginalYear == "" {
		if m.EditionYear != "" {
			m.OriginalYear = m.EditionYear
		} else {
			m.OriginalYear = unknownYear
		}
	}
	if m.EditionYear == "" {
		if m.OriginalYear != "" {
			m.EditionYear = m.OriginalYear
		} else {
			m.EditionYear = unknownYear
		}
	}
	// clean description
	m.Description = CleanDescription(m.Description)
	// clean language
	m.Language = CleanLanguage(m.Language)

	// clean tags
	m.Tags.Clean(forbiddenTags, tagAliases)

	// autofill category
	if m.Category == "" {
		for _, possibleCategory := range validCategories {
			if m.Tags.HasTag(possibleCategory) {
				m.Category = possibleCategory
				m.Tags.RemoveFromNames(possibleCategory)
				break
			}
		}
	}
	// if nothing valid found...
	if m.Category == "" {
		m.Category = unknown
	}
	if cat, err := CleanCategory(m.Category); err == nil {
		m.Category = cat
		m.Tags.RemoveFromNames(cat)
	}

	// autofill type
	if m.Type == "" {
		for _, possibleType := range validTypes {
			if m.Tags.HasTag(possibleType) {
				m.Type = possibleType
				m.Tags.RemoveFromNames(possibleType)
				break
			}
		}
	}
	// if nothing found, unknown.
	if m.Type == "" {
		m.Type = unknown
	}
	if tp, err := CleanType(m.Type); err == nil {
		m.Type = tp
		m.Tags.RemoveFromNames(tp)
	}

	// MainGenre
	if m.Genre == "" && len(m.Tags) != 0 {
		//if cleanName, err := cleanTagName(m.Tags[0].Name); err == nil {
		//m.Genre = cleanName
		m.Genre = m.Tags[0].Name
		m.Tags.RemoveFromNames(m.Genre)
		//}
	}
	// if nothing valid found...
	if m.Genre == "" {
		m.Genre = unknown
	}

	// clean series
	for j := range m.Series {
		m.Series[j].Name = strings.TrimSpace(m.Series[j].Name)
	}
	// clean publisher
	m.Publisher = strings.TrimSpace(m.Publisher)
}
