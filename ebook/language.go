package ebook

import (
	"strings"

	"gitlab.com/catastrophic/assistance/strslice"
)

// languageAliases defines language and redundant ways to describe them.
var languageAliases = map[string][]string{
	"en": {"en-US", "en-GB", "eng", "en-CA"},
	"fr": {"fr-FR", "fre"},
	"es": {"spa"},
}

func CleanLanguage(language string) string {
	clean := strings.TrimSpace(strings.ToLower(language))
	// reducing to main alias
	for mainAlias, aliasList := range languageAliases {
		if strslice.Contains(aliasList, language) {
			return mainAlias
		}
	}
	return clean
}
