package ebook

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

// TestTags tests Add, Remove, Has and HasAny.
func TestTags(t *testing.T) {
	fmt.Println("+ Testing Tags...")
	assert := assert.New(t)
	tag1 := Tag{Name: "test_é!/?*èç1"}
	tag2 := Tag{Name: "t54*ßèç©@1ϽƉ"}
	tag3 := Tag{Name: "forthcoming"}
	tag4 := Tag{Name: "sci-fi"}
	tag5 := Tag{Name: "science-fiction"}
	m := Metadata{}

	// check empty tags
	hasAny := m.Tags.HasAny()
	assert.False(hasAny, "book does not have any tags")
	isIn, _ := m.Tags.Has(tag1)
	assert.False(isIn, "Error: did not expect to have tag1.")
	// check adding 2 tags
	added := m.Tags.Add(tag1, tag2)
	assert.True(added, "Error: book should have tags now.")
	hasAny = m.Tags.HasAny()
	assert.True(hasAny, "book has tags now.")
	isIn, _ = m.Tags.Has(tag1)
	assert.True(isIn, "Error: expected to have tag1.")
	isIn, _ = m.Tags.Has(tag2)
	assert.True(isIn, "Error: expected to have tag2.")
	// check string()
	assert.Equal(tag1.Name+", "+tag2.Name, m.Tags.String(), "Error generating String()")
	// adding more tags
	added = m.Tags.Add(tag3, tag4)
	assert.True(added, "Error: book should have more tags now.")
	// check Add only new tags
	added = m.Tags.AddFromNames(tag3.Name)
	assert.False(added, "Error: book should not have added already known tag.")
	// check clean
	isIn, _ = m.Tags.Has(tag3)
	assert.True(isIn, "Error: expected to have tag3.")
	m.Tags.Clean(nil, nil)
	isIn, _ = m.Tags.Has(tag3)
	assert.False(isIn, "Error: expected tag3 to have been cleaned.")
	// test remove
	removed := m.Tags.RemoveFromNames(tag5.Name, tag1.Name)
	assert.True(removed, "Error: expecteds to be removed.")
	isIn, _ = m.Tags.Has(tag5)
	assert.False(isIn, "Error: expected tag5 to have been removed.")

	// test cleaning with more forbidden things and additional aliases
	added = m.Tags.Add(tag1, tag5, tag3)
	assert.True(added, "Error: book should have more tags now.")

	alias := make(map[string][]string)
	alias["science-fiction"] = []string{"sci-fi"}

	m.Tags.Clean([]string{tag1.Name}, alias)
	isIn, _ = m.Tags.Has(tag1)
	assert.False(isIn, "Error: expected tag1 to have been cleaned.")
	isIn, _ = m.Tags.Has(tag4)
	assert.False(isIn, "Error: expected tag4 to have been cleaned.")
	isIn, _ = m.Tags.Has(tag3)
	assert.False(isIn, "Error: expected tag3 to have been cleaned.")
}
