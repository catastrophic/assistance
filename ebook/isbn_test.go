package ebook

import (
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

var isbns = []struct {
	candidate     string
	expectedISBN  []ISBN
	expectedError error
}{
	{
		"urn:ISBN: 9-780-5750-8-365-3  ",
		[]ISBN{{ISBN13: "9780575083653", Context: []string{"urn:ISBN: \x1b[1;92m9780575083653\x1b[0m "}}},
		nil,
	},
	{
		"9780575083653",
		[]ISBN{{ISBN13: "9780575083653"}},
		nil,
	},
	{
		"9780575083653111AZADFEF",
		[]ISBN{{ISBN13: "9780575083653", Context: nil}},
		nil,
	},
	{
		" 9780753553114111AZADFEF0575083654 v&é&é)àdiz)i<9781466861039>",
		[]ISBN{
			{ISBN13: "9780753553114", Location: "", Context: []string{" \x1b[1;92m9780753553114\x1b[0m111AZADFEF0575083654 v&é&é)àdiz"}},
			{ISBN10: "0575083654", ISBN13: "9780575083653", Context: []string{" 9780753553114111AZADFEF\x1b[1;92m0575083654\x1b[0m v&é&é)àdiz)i<9781466861039>"}},
			{ISBN13: "9781466861039", Context: []string{"AZADFEF0575083654 v&é&é)àdiz)i<\x1b[1;92m9781466861039\x1b[0m>"}}},
		nil,
	},
	{
		"9780575083652",
		nil,
		errors.New("ISBN-13 not found"),
	},
	{
		"0575083654",
		[]ISBN{{ISBN10: "0575083654", ISBN13: "9780575083653"}},
		nil,
	},
	{
		"0575083655",
		nil,
		errors.New("ISBN-13 not found"),
	},
	{
		"A223234333432",
		nil,
		errors.New("ISBN-13 not found"),
	},
	{
		"urn:isbn: 12-23-4-333-432  ",
		nil,
		errors.New("ISBN-13 not found"),
	},
	{
		"urn:uuid:0adf2006-7812-4675-9c27-47699d21c4a2",
		nil,
		errors.New("ISBN-13 not found"),
	},
	{
		`<p class="centered">Epub ISBN: 9780753553114</p>okpôkpokpkpokpokpok 2765410058   Paperback ISBN: 9780575083653 This is completely normal.   080442957X `,
		[]ISBN{
			{ISBN13: "9780753553114", Context: []string{" Epub ISBN: \x1b[1;92m9780753553114\x1b[0m okpôkpokpkpokpokpok 2765410058"}},
			{ISBN13: "9780575083653", Context: []string{"pok 2765410058 Paperback ISBN: \x1b[1;92m9780575083653\x1b[0m This is completely normal. 080"}},
			{ISBN10: "080442957X", ISBN13: "9780804429573", Context: []string{"653 This is completely normal. \x1b[1;92m080442957X\x1b[0m "}}},
		nil,
	},
}

func TestEpubCleanISBN(t *testing.T) {
	fmt.Println("+ Testing Info/CleanISBN()...")
	check := assert.New(t)

	for _, c := range isbns {
		isbn, err := CleanISBN(c.candidate)
		check.Equal(c.expectedError, err)

		for _, i := range isbn {
			i.Location = "Test.epub"
			fmt.Println(i.String())
		}
		switch {
		case err == nil && c.expectedError != nil:
			t.Errorf("Unexpected error cleaning isbn %s", c.candidate)
		case err != nil && c.expectedError == nil:
			t.Errorf("Unexpected error cleaning isbn %s", c.candidate)
		case err != nil && c.expectedError != nil && c.expectedError.Error() != err.Error():
			t.Errorf("Unexpected error cleaning isbn %s: got %s, expected %s", c.candidate, c.expectedError.Error(), err.Error())
		}
		check.Equal(c.expectedISBN, isbn, "Error cleaning isbn")
	}
}
