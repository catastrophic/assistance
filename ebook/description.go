package ebook

import (
	"regexp"
	"strings"

	"github.com/kennygrant/sanitize"
)

var (
	// cleans up newlines
	newLinesCleanUpRegExp = regexp.MustCompile(`\n+`)
)

func CleanDescription(desc string) string {
	sanitized := strings.TrimSpace(sanitize.HTML(desc))
	// removing extra newlines
	sanitized = newLinesCleanUpRegExp.ReplaceAllString(sanitized, "\n")
	return sanitized
}
