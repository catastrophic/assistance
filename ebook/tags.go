package ebook

import (
	"strings"

	"gitlab.com/catastrophic/assistance/strslice"
)

const (
	maxNumberOfTags = 10
)

// remove shelf names that are obviously not genres.
var forbiddenTags = []string{"own", "school", "favorite", "favourite", "book", "adult", "read", "kindle", "borrowed",
	"classic", "buy", "star", "release", "wait", "soon", "wish", "published", "want", "tbr", "series", "finish", "to-",
	"not-", "library", "audible", "coming", "anticipated", "default", "recommended", "-list", "sequel", "general",
	"have", "bundle", "maybe", "audio", "podcast", "calibre", "bks", "moved-on", "record", "arc", "z-", "livre",
	"unsorted", "gave-up", "abandoned", "retelling", "middle-grade", "collection", "english", "netgalley", "available",
	"list", "stand-alone", "meh", "amazon", "paperback", "giveaways", "review-copy", "check", "queue", "dnf", "aa",
	"aaa", "forthcoming", "to-read", "currently-reading", "reading", "recommended-to-me", "read-next", "owned", "ebooks",
	"favorites", "books-i-own"}

// Tag holds the name of a tag.
type Tag struct {
	Name string `json:"name" xml:"name,attr"`
}

// Tags can track a book's Tags.
type Tags []Tag

// String give a string representation of Tags.
func (t *Tags) String() string {
	tagNames := make([]string, len(*t))
	for i, tag := range *t {
		tagNames[i] = tag.Name
	}
	return strings.Join(tagNames, ", ")
}

// Add Tags to the list.
func (t *Tags) Add(tags ...Tag) (added bool) {
	for _, tag := range tags {
		if isIn, _ := t.Has(tag); !isIn {
			*t = append(*t, tag)
			added = true
		}
	}
	return
}

// AddFromNames Tags to the list, from []string.
func (t *Tags) AddFromNames(tags ...string) bool {
	newTags := Tags{}
	for _, tag := range tags {
		tag = strings.TrimSpace(tag)
		if tag != "" {
			newTags = append(newTags, Tag{Name: tag})
		}
	}
	return t.Add(newTags...)
}

// Remove Tags from the list.
func (t *Tags) Remove(tags ...Tag) (removed bool) {
	for _, tag := range tags {
		if isIn, i := t.Has(tag); isIn {
			*t = append((*t)[:i], (*t)[i+1:]...)
			removed = true
		}
	}
	return
}

// RemoveFromNames Tags to the list, from []string.
func (t *Tags) RemoveFromNames(tags ...string) bool {
	newTags := Tags{}
	for _, tag := range tags {
		newTags = append(newTags, Tag{Name: tag})
	}
	return t.Remove(newTags...)
}

// Has finds out if a Tag is already in list.
func (t *Tags) Has(o Tag) (isIn bool, index int) {
	for i, tag := range *t {
		if o.Name == tag.Name {
			return true, i
		}
	}
	return
}

// HasTag finds out if a Tag name is already in list.
func (t *Tags) HasTag(tagName string) bool {
	for _, tag := range *t {
		if tag.Name == tagName {
			return true
		}
	}
	return false
}

// HasAny checks if epub is part of any series.
func (t *Tags) HasAny() bool {
	return len(*t) != 0
}

func (t *Tags) Slice() []string {
	tagNames := make([]string, len(*t))
	for i, tag := range *t {
		tagNames[i] = strings.ToLower(strings.TrimSpace(tag.Name))
	}
	return tagNames
}

func (t *Tags) Clean(otherForbidden []string, otherAliases map[string][]string) {
	tagNames := t.Slice()

	// remove duplicates & forbidden things
	strslice.RemoveDuplicates(&tagNames, forbiddenTags...)
	strslice.RemoveDuplicates(&tagNames, otherForbidden...)

	// use category alias
	for mainAlias, aliasList := range categoryAliases {
		for i, t := range tagNames {
			if strslice.Contains(aliasList, t) {
				tagNames[i] = mainAlias
			}
		}
	}
	// use type alias
	for mainAlias, aliasList := range typeAliases {
		for i, t := range tagNames {
			if strslice.Contains(aliasList, t) {
				tagNames[i] = mainAlias
			}
		}
	}
	// use otherAlias
	for mainAlias, aliasList := range otherAliases {
		for i, t := range tagNames {
			if strslice.Contains(aliasList, t) {
				tagNames[i] = mainAlias
			}
		}
	}

	// remove duplicates
	strslice.RemoveDuplicates(&tagNames)

	// removing the less useful tags
	if len(tagNames) > maxNumberOfTags {
		tagNames = tagNames[:maxNumberOfTags]
	}

	// recreate []Tag
	newTags := Tags{}
	for _, tag := range tagNames {
		newTags = append(newTags, Tag{Name: tag})
	}
	*t = newTags
}
