package ebook

import (
	"fmt"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

const (
	shouldBeModifiedError    = "series should be modified"
	shouldNotBeModifiedError = "series should not have been modified"
	wrongFormatError         = "adding series with wrong format should have failed"
	expectedSeriesError      = "expected epub to have series %s"
	addingSeriesError        = "error adding Series %s - %f"
	seriesAtIndex0Error      = "expected epub to have series %s at index 0"
)

// TestSeries tests Add, Remove, Has and HasAny.
func TestSeries(t *testing.T) {
	fmt.Println("+ Testing Series...")
	var err error
	var seriesModified bool
	assert := assert.New(t)

	i := 1

	m := Metadata{}

	seriesName := "test_é!/?*èç1"
	seriesName2 := "test2"

	hasAny := m.Series.HasAny()
	assert.False(hasAny, "Error: did not expect to have any series.")

	// testing adding series
	seriesModified = m.Series.add(seriesName, float64(i))
	assert.True(seriesModified, fmt.Sprintf(addingSeriesError, seriesName, float32(i)))

	// testing adding second series
	seriesModified = m.Series.add(seriesName2, float64(i))
	assert.True(seriesModified, fmt.Sprintf(addingSeriesError, seriesName2, float32(i)))

	hasAny = m.Series.HasAny()
	assert.True(hasAny, "Error: expected to have at least one series.")
	expectedString := fmt.Sprintf("%s #%d, %s #%d", seriesName, i, seriesName2, i)
	assert.Equal(m.Series.String(), expectedString, "Error printing series info")

	// testing having series
	hasSeries, index, seriesIndex := m.Series.Has(seriesName)
	assert.True(hasSeries, fmt.Sprintf(expectedSeriesError, seriesName))
	assert.Equal(index, 0, fmt.Sprintf(seriesAtIndex0Error, seriesName))
	assert.Equal(seriesIndex, strconv.Itoa(i))

	hasSeries, index, seriesIndex = m.Series.Has(seriesName2)
	assert.True(hasSeries, fmt.Sprintf(expectedSeriesError, seriesName2))
	assert.Equal(index, 1, fmt.Sprintf(seriesAtIndex0Error, seriesName2))
	assert.Equal(seriesIndex, strconv.Itoa(i))

	hasSeries, _, _ = m.Series.Has(seriesName + "ç")
	assert.False(hasSeries)

	// testing updating series index
	seriesModified = m.Series.add(seriesName, float64(i)+0.5)
	assert.True(seriesModified, fmt.Sprintf(addingSeriesError, seriesName, float32(i)+0.5))

	// testing having modified series
	hasSeries, index, seriesIndex = m.Series.Has(seriesName)
	assert.True(hasSeries, fmt.Sprintf(expectedSeriesError, seriesName))
	assert.Equal(0, index, fmt.Sprintf(seriesAtIndex0Error, seriesName))
	expected := fmt.Sprintf("%s,%s",
		strconv.FormatFloat(float64(i), 'f', -1, 32),
		strconv.FormatFloat(float64(i)+0.5, 'f', -1, 32))
	assert.Equal(expected, seriesIndex)

	// testing adding from string
	m.Series = Series{}
	entries := []string{"test:1.5", "test:2.5", "test2:", "test3", "test4:7-9", "test4:1", "series: with a semicolon :7-9"}
	for _, s := range entries {
		seriesModified, err = m.Series.AddFromString(s)
		assert.True(seriesModified, shouldBeModifiedError)
		assert.Nil(err)
	}
	// testing adding already known index
	seriesModified, err = m.Series.AddFromString("test4:8")
	assert.False(seriesModified, shouldNotBeModifiedError)
	assert.Nil(err)

	// testing output
	assert.Equal("test #1.5,2.5, test2 #0, test3 #0, test4 #1,7,8,9, series: with a semicolon #7,8,9", m.Series.String())
	assert.Equal("test:1.5,2.5, test2:0, test3:0, test4:1,7,8,9, series: with a semicolon:7,8,9", m.Series.rawString())

	// testing having modified series
	hasSeries, _, seriesIndex = m.Series.Has("series: with a semicolon")
	assert.True(hasSeries)
	assert.Equal("7,8,9", seriesIndex)

	// testing wrong inputs
	wrongEntries := []string{"test5:aoqoj", "test5:1-2-3", "test5:1-a", "test5:a-a"}
	for _, w := range wrongEntries {
		seriesModified, err = m.Series.AddFromString(w)
		assert.NotNil(err, wrongFormatError)
		assert.False(seriesModified, shouldNotBeModifiedError)
	}
}
