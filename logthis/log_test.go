package logthis

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestLog(t *testing.T) {
	fmt.Println("+ Testing Log...")
	check := assert.New(t)

	l = newLog()

	// unconfigured
	check.False(l.configured)
	check.True(l.stdOutput)
	check.Equal("+\n", captureOutput(func() { Info("+", NORMAL) }))

	// setting level
	SetLevel(VERBOSEST)
	check.Equal(VERBOSEST, l.level)

	// checking level
	check.Nil(CheckLevel(NORMAL))
	err := CheckLevel(555)
	if !errors.Is(err, ErrInvalidLogLevel) {
		t.Errorf("unexpected error: %v instead of %v", err, ErrInvalidLogLevel)
	}

	// to log only
	SetStdOutput(false)
	check.True(l.configured)
	check.False(l.stdOutput)
	logOutput := captureOutput(func() { Info("+", NORMAL) })
	check.NotEqual("+\n", logOutput)
	// first two parts are the timestamp (ex: 2019/01/02 00:16:10 +\n)
	parts := strings.Split(logOutput, " ")
	check.Equal(3, len(parts))
	check.Equal("+\n", parts[2])

	// to stdout
	SetStdOutput(true)
	check.True(l.configured)
	check.True(l.stdOutput)

	// Info
	check.Equal("A\n", captureOutput(func() { Info("A", NORMAL) }))
	check.Equal("A\n", captureOutput(func() { Info("A", VERBOSE) }))
	check.Equal("A\n", captureOutput(func() { Info("A", VERBOSEST) }))
	check.Equal("", captureOutput(func() { Info("A", VERBOSESTEST) }))
	// Error
	check.Equal("A\n", captureOutput(func() { Error(errors.New("A"), NORMAL) }))
	check.Equal("A\n", captureOutput(func() { Error(errors.New("A"), VERBOSE) }))
	check.Equal("A\n", captureOutput(func() { Error(errors.New("A"), VERBOSEST) }))
	check.Equal("", captureOutput(func() { Error(errors.New("A"), VERBOSESTEST) }))

	// Create and subscribe 3 clients:
	clientFunc := func(id int) {
		msgCh := Subscribe()
		fmt.Printf("Client %d subscribed.\n", id)
		cpt := 1
		for msg := range msgCh {
			fmt.Printf("Client %d got message: %v\n", id, msg)
			check.Equal(fmt.Sprintf("%d", cpt), msg)
			check.NotEqual("4", msg, "message should not be seen, too verbose.")
			cpt++
			if cpt == 4 {
				break
			}
		}
		Unsubscribe(msgCh)
		fmt.Printf("Client %d unsubscribed.\n", id)
	}

	for i := 0; i < 3; i++ {
		go clientFunc(i)
	}
	time.Sleep(5 * time.Millisecond)

	check.Equal("", captureOutput(func() { Info("4", VERBOSESTEST) }))
	check.Equal("1\n", captureOutput(func() { Info("1", NORMAL) }))
	check.Equal("2\n", captureOutput(func() { Info("2", VERBOSE) }))
	check.Equal("3\n", captureOutput(func() { Info("3", VERBOSEST) }))

	time.Sleep(5 * time.Millisecond)
	l.brocker.stop()
}

// captureOutput redirects fmt/log to a string.
func captureOutput(f func()) string {
	var buf bytes.Buffer
	log.SetOutput(&buf)
	SetOutputWriter(&buf)
	f()
	log.SetOutput(os.Stderr)
	SetOutputWriter(defaultOutputWriter)
	return buf.String()
}
