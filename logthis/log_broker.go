package logthis

import (
	"fmt"
	"log"
	"sync"
)

// thanks https://stackoverflow.com/a/49877632

type brocker struct {
	sync.Mutex
	stopCh      chan struct{}
	publishCh   chan interface{}
	subCh       chan chan interface{}
	unsubCh     chan chan interface{}
	subscribers map[chan interface{}]struct{}
}

func newBroker() *brocker {
	return &brocker{
		stopCh:      make(chan struct{}),
		publishCh:   make(chan interface{}, 1),
		subCh:       make(chan chan interface{}, 1),
		unsubCh:     make(chan chan interface{}, 1),
		subscribers: make(map[chan interface{}]struct{}, 2),
	}
}

func (b *brocker) start() {
	for {
		select {
		case <-b.stopCh:
			b.Lock()
			for msgCh := range b.subscribers {
				close(msgCh)
			}
			b.Unlock()
			return
		case msgCh := <-b.subCh:
			b.Lock()
			b.subscribers[msgCh] = struct{}{}
			b.Unlock()
		case msgCh := <-b.unsubCh:
			b.Lock()
			delete(b.subscribers, msgCh)
			b.Unlock()
		case msg := <-b.publishCh:
			b.Lock()
			for msgCh := range b.subscribers {
				if err := publish(msgCh, msg); err != nil {
					log.Printf("error publishing to channel: %v\n", err)
				}
			}
			b.Unlock()
		}
	}
}

func publish(out chan interface{}, msg interface{}) (err error) {
	defer func() {
		// recover from panic caused by writing to a closed channel
		if r := recover(); r != nil {
			err = fmt.Errorf("%v", r)
		}
	}()
	// msgCh is buffered, use non-blocking send to protect the broker:
	select {
	case out <- msg:
	default:
	}
	return err
}

func (b *brocker) stop() {
	close(b.stopCh)
}

func (b *brocker) subscribe() chan interface{} {
	msgCh := make(chan interface{}, 5)
	b.subCh <- msgCh
	return msgCh
}

func (b *brocker) unsubscribe(msgCh chan interface{}) {
	b.unsubCh <- msgCh
	close(msgCh)
}

func (b *brocker) publish(msg interface{}) {
	b.Lock()
	subs := len(b.subscribers)
	b.Unlock()
	if subs != 0 {
		b.publishCh <- msg
	}
}
