// Package logthis provides a way to either print, log, and/or send a string to channels.
// Provides a default logger initiated on import.
package logthis

import (
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"sync"
	"time"

	"gitlab.com/catastrophic/assistance/ui"
)

const (
	NORMAL = iota
	VERBOSE
	VERBOSEST
	VERBOSESTEST
)

var (
	defaultOutputWriter = os.Stdout

	ErrInvalidLogLevel = errors.New("invalid log level")
)

func init() {
	l = newLog()
	startTime = time.Now()
}

// ----------------------------------------------------------------------------

var (
	onceLog   sync.Once
	l         *LogThis
	startTime time.Time
)

type LogThis struct {
	level        int
	configured   bool
	stdOutput    bool
	outputWriter io.Writer
	brocker      *brocker
}

func newLog() *LogThis {
	onceLog.Do(func() {
		l = &LogThis{outputWriter: defaultOutputWriter, brocker: newBroker(), stdOutput: true}
		go l.brocker.start()
	})
	return l
}

// ----------------------------------------------------------------------------

func CheckLevel(value int) error {
	if value < NORMAL || value > VERBOSESTEST {
		return ErrInvalidLogLevel
	}
	return nil
}

func SetLevel(value int) {
	l.configured = true
	l.level = value
}

func SetOutputWriter(writer io.Writer) {
	l.outputWriter = writer
}

func SetStdOutput(value bool) {
	l.stdOutput = value
}

func Subscribe() chan interface{} {
	return l.brocker.subscribe()
}

func Unsubscribe(msgCh chan interface{}) {
	l.brocker.unsubscribe(msgCh)
}

// ----------------------------------------------------------------------------

// Error logs errors with a given level.
func Error(err error, level int) {
	Info(err.Error(), level)
}

// Info logs strings with a given level, optionally sending info to chans.
func Info(msg string, level int) {
	if !l.configured {
		// configuration was not loaded, printing error message
		_, err := fmt.Fprintln(l.outputWriter, msg)
		if err != nil {
			fmt.Println("could not print msg to writer: " + err.Error())
		}
		return
	}
	if l.level >= level {
		// to std output or log file
		if l.stdOutput {
			var err error
			if l.outputWriter == defaultOutputWriter {
				_, err = fmt.Fprintln(l.outputWriter, msg)
			} else {
				_, err = fmt.Fprintln(l.outputWriter, ui.RemoveColor(msg))
			}
			if err != nil {
				fmt.Println("could not print msg to writer: " + err.Error())
			}
		} else {
			// remove color for writing to log file
			log.Println(ui.RemoveColor(msg))
		}

		// to subscribers
		l.brocker.publish(msg)
	}
}

// TimedInfo logs a message prefixed with the time elapsed since the program started.
func TimedInfo(msg string, level int) {
	elapsed := time.Since(startTime).Seconds()
	Info(fmt.Sprintf("%8.03fs", elapsed)+" | "+msg, level)
}

// TimedError logs an error prefixed with the time elapsed since the program started.
func TimedError(err error, level int) {
	elapsed := time.Since(startTime).Seconds()
	Info(fmt.Sprintf("%8.03fs", elapsed)+" | /!\\ "+err.Error(), level)
}

// ErrorIfNotNil logs non-nil errors from a slice.
func ErrorIfNotNil(errs []error, level int) {
	for _, e := range errs {
		if e != nil {
			Error(e, level)
		}
	}
}
