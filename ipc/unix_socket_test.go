package ipc

import (
	"fmt"
	"io/ioutil"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestIPCUnixSocket(t *testing.T) {
	fmt.Println("+ Testing SocketCom...")
	check := assert.New(t)

	socketFile := "test.sock"
	socketFileFake := "test.sock.fake"
	check.Nil(ioutil.WriteFile(socketFileFake, []byte("NOT A UNIX DOMAIN SOCKET"), 0777))

	ipcServer := NewUnixSocketServer(socketFile)
	ipcClient := NewUnixSocketClient(socketFile)

	// testing socket file errors
	ipcServer.socket = socketFileFake
	check.NotNil(ipcServer.RunServer())
	ipcServer.socket = socketFile
	check.Nil(os.Remove(socketFileFake))

	ipcClient.socket = socketFileFake
	check.NotNil(ipcClient.RunClient())
	ipcClient.socket = socketFile
	// testing launch errors
	check.NotNil(ipcClient.RunServer())
	check.NotNil(ipcServer.RunClient())

	// launching server
	go func() {
		check.Nil(ipcServer.RunServer())
	}()
	<-ipcServer.ServerUp
	go func() {
		check.Nil(ipcClient.RunClient())
	}()
	<-ipcClient.ClientConnected
	<-ipcServer.ClientConnected

	// goroutines to process what the server / client receives
	// the server routines echoes what it gets back to the client
	go func() {
		for {
			a := <-ipcServer.Incoming
			fmt.Println("SERVER received: " + string(a))
			outMessage := append([]byte("echo: "), a...)
			ipcServer.Outgoing <- outMessage
			if string(a) == "command before stop" {
				time.Sleep(1 * time.Millisecond)
				ipcServer.Outgoing <- []byte(StopCommand)
			}
		}
	}()
	go func() {
		for {
			a := <-ipcClient.Incoming
			fmt.Println("CLIENT received: " + string(a))
		}
	}()

	// connection is active and messages are exchanges
	check.True(ipcClient.IsActive)
	check.True(ipcServer.IsActive)
	ipcServer.Outgoing <- []byte("one")
	ipcClient.Outgoing <- []byte("two")
	ipcClient.Outgoing <- []byte("three !!!")
	veryLong := make([]byte, 3000)
	for i := range veryLong {
		veryLong[i] = 'O'
	}
	veryLong[0] = 'a'
	veryLong[len(veryLong)-1] = 'b'
	ipcClient.Outgoing <- veryLong

	// stopping programmatically
	time.Sleep(1 * time.Millisecond)
	fmt.Println("- Stopping client.")
	ipcClient.StopCurrent()
	<-ipcClient.ClientDisconnected
	<-ipcServer.ClientDisconnected
	check.False(ipcClient.IsActive)

	for i := 0; i < 20; i++ {
		time.Sleep(1 * time.Millisecond)
		fmt.Println("- Start client again.")
		go func() {
			check.Nil(ipcClient.RunClient())
		}()
		<-ipcClient.ClientConnected
		<-ipcServer.ClientConnected

		ipcClient.Outgoing <- []byte("this is a quite formidable test.")
		time.Sleep(1 * time.Millisecond)
		ipcClient.Outgoing <- []byte("command before stop")

		<-ipcClient.ClientDisconnected
		<-ipcServer.ClientDisconnected
		fmt.Println("- Client should have been stopped by command.")
		check.False(ipcClient.IsActive)
	}

	check.True(ipcServer.IsListening)
	fmt.Println("- Stopping server")
	ipcServer.StopCurrent()

	time.Sleep(1 * time.Millisecond)
	check.False(ipcServer.IsListening)
}
