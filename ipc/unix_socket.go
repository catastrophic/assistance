// Package ipc provides a way for a process to communicate with a daemon, using a unix domain socket.
package ipc

import (
	"fmt"
	"io"
	"net"
	"strings"
	"sync"

	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/logthis"
)

const (
	errorCreatingSocket      = "could not create unix socket"
	errorDialingSocket       = "could not dial unix socket"
	errorWritingToSocket     = "could not write to unix socket"
	errorAcceptingFromSocket = "could not accept from unix socket"
	errorReadingFromSocket   = "could not read from unix socket"

	unixSocketProtocol         = "unix"
	unixSocketMessageSeparator = "↑" // because it looks nice
	StopCommand                = "stop"
)

// UnixSocket is a standalone struct to handle communication between processes, using a unix domain socket.
type UnixSocket struct {
	IsServer           bool
	IsActive           bool
	IsListening        bool
	Incoming           chan []byte
	Outgoing           chan []byte
	ServerUp           chan struct{}
	ClientConnected    chan struct{}
	ClientDisconnected chan struct{}
	socket             string
	endThisConnection  chan struct{}
	err                chan error
	connection         *net.Conn
	listener           *net.Listener
	sync.RWMutex
}

func NewUnixSocket(socketFile string, server bool) *UnixSocket {
	in := make(chan []byte)
	out := make(chan []byte)
	err := make(chan error)
	end := make(chan struct{})
	disconnect := make(chan struct{})
	connect := make(chan struct{})
	serverUp := make(chan struct{})
	return &UnixSocket{
		socket:             socketFile,
		Incoming:           in,
		Outgoing:           out,
		err:                err,
		endThisConnection:  end,
		ServerUp:           serverUp,
		ClientDisconnected: disconnect,
		ClientConnected:    connect,
		IsServer:           server}
}

func NewUnixSocketServer(socketFile string) *UnixSocket {
	return NewUnixSocket(socketFile, true)
}

func NewUnixSocketClient(socketFile string) *UnixSocket {
	return NewUnixSocket(socketFile, false)
}

func (dc *UnixSocket) RunServer() error {
	// to be run as a goroutine
	if !dc.IsServer {
		return errors.New("not a server")
	}

	listener, err := net.Listen(unixSocketProtocol, dc.socket)
	if err != nil {
		return errors.Wrap(err, errorCreatingSocket)
	}
	dc.listener = &listener
	dc.IsListening = true
	dc.ServerUp <- struct{}{}

	// it sends things from the socket to in
	go dc.waitForErrors(false)
	dc.serverReceive()

	return nil
}

func (dc *UnixSocket) RunClient() error {
	// to be run as a goroutine
	if dc.IsServer {
		return errors.New("not a client")
	}
	conn, err := net.Dial(unixSocketProtocol, dc.socket)
	if err != nil {
		return errors.Wrap(err, errorDialingSocket)
	}
	dc.Lock()
	dc.connection = &conn
	dc.IsActive = true
	dc.Unlock()
	dc.ClientConnected <- struct{}{}

	// it writes things from out to the socket
	go dc.waitForErrors(true)
	go dc.send()
	go dc.clientReceive()

	return nil
}

func (dc *UnixSocket) waitForErrors(exitOnError bool) {
	for {
		<-dc.err
		dc.endThisConnection <- struct{}{}
		if exitOnError {
			break
		}
	}
}

func (dc *UnixSocket) serverReceive() {
	// goroutine to read from socket
	for { // dc.IsListening {
		conn, err := (*dc.listener).Accept()
		if err != nil {
			if dc.IsListening {
				logthis.Info(errorAcceptingFromSocket+": "+err.Error(), logthis.VERBOSEST)
				continue
			} else {
				break
			}
		}
		dc.Lock()
		dc.connection = &conn
		dc.IsActive = true
		dc.Unlock()
		dc.ClientConnected <- struct{}{}

		go dc.send()
		go dc.read()

		// waiting for the other instance to be warned that communication is over
		<-dc.endThisConnection
		dc.Lock()
		dc.IsActive = false
		dc.Unlock()
		(*dc.connection).Close()
		dc.ClientDisconnected <- struct{}{}
	}
}

func (dc *UnixSocket) read() {
	for {
		buf := make([]byte, 2048)
		n, err := (*dc.connection).Read(buf)
		if err != nil {
			dc.RLock()
			if dc.IsActive {
				if err != io.EOF {
					logthis.Error(errors.Wrap(err, errorReadingFromSocket+fmt.Sprintf(" (server:%v)", dc.IsServer)), logthis.NORMAL)
				}
				dc.err <- err
			}
			dc.RUnlock()
			break
		}

		stopAfterThis := false
		for _, part := range strings.Split(string(buf[:n]), unixSocketMessageSeparator) {
			if part == "" {
				continue
			}
			dc.Incoming <- []byte(part)

			// if we are the client and receive stop, the server just told us it's the end of the communication.
			// -1 because of unixSocketMessageSeparator
			if part == StopCommand && !dc.IsServer {
				stopAfterThis = true
			}
		}
		if stopAfterThis {
			dc.endThisConnection <- struct{}{}
			break
		}
	}
}

func (dc *UnixSocket) clientReceive() {
	// goroutine to read from socket
	go dc.read()

	// waiting for the other instance to be warned that communication is over
	<-dc.endThisConnection
	dc.Lock()
	dc.IsActive = false
	dc.Unlock()
	(*dc.connection).Close()
	dc.ClientDisconnected <- struct{}{}
}

func (dc *UnixSocket) send() {
	// goroutine to write to socket
	for {
		messageToLog := <-dc.Outgoing
		messageToLog = append(messageToLog, []byte(unixSocketMessageSeparator)...)
		// writing to socket with a separator, so that the other instance, reading more slowly,
		// can separate messages that might have been written one after the other
		if _, err := (*dc.connection).Write(messageToLog); err != nil {
			dc.Lock()
			active := dc.IsActive
			dc.Unlock()
			if active {
				if err != io.EOF {
					logthis.Error(errors.Wrap(err, errorWritingToSocket), logthis.NORMAL)
				}
				dc.err <- err
			}
			break
		}
		// we've just told the other instance talking was over, ending this connection.
		if string(messageToLog) == StopCommand && dc.IsServer {
			dc.endThisConnection <- struct{}{}
			dc.IsActive = false
			break
		}
	}
}

func (dc *UnixSocket) StopCurrent() {
	if dc.IsActive {
		dc.endThisConnection <- struct{}{}
	}

	if dc.IsServer {
		dc.IsListening = false
		(*dc.listener).Close()
	}
}
