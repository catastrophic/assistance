package flac

import (
	"math"
	"regexp"
	"strings"
	"unicode"

	"github.com/mozillazg/go-unidecode"
)

const (
	// specifically ignore (EP) [EP] - EP at the end of strings.
	regexpTrackBeginning = `(?U)^(.*)( \(.*\))( (\(|\[|\- )?EP[\)\]]?)?$`
	regexpEP             = `^(.*)ep$`
)

func OnlyKeepLettersAndNumbers(s string) string {
	var out string
	for _, c := range s {
		if unicode.IsLetter(c) || unicode.IsNumber(c) {
			out += string(c)
		}
	}
	return out
}

func StringContainsStartOfAnother(mainString, subString string, minTitleSize int) bool {
	// if the title contains extra info (feat. XXX), (XXX Remix), only consider the actual title
	// if the actual title contains parenthesis, the check isn't as good, though.
	re := regexp.MustCompile(regexpTrackBeginning)
	re2 := regexp.MustCompile(regexpEP)
	hits := re.FindAllStringSubmatch(subString, -1)
	if len(hits) != 0 {
		subString = hits[0][1]
	}
	subString = OnlyKeepLettersAndNumbers(strings.ToLower(subString))
	if re2.MatchString(subString) {
		hits := re2.FindAllStringSubmatch(subString, -1)
		subString = hits[0][1]
	}
	mainString = OnlyKeepLettersAndNumbers(strings.ToLower(mainString))

	// if is contained completely, return true
	if strings.Contains(mainString, subString) {
		return true
	}

	// minimum size of title to be checked in filename
	// ie if title is truncated, expecting at least minTitleSize chars if title is longer
	minSize := int(math.Min(float64(minTitleSize), float64(len(subString))))
	for i := minSize; i < len(subString); i++ {
		if strings.Contains(mainString, subString[:i]) {
			return true
		}
	}

	if subString != unidecode.Unidecode(subString) {
		return StringContainsStartOfAnother(mainString, unidecode.Unidecode(subString), minTitleSize)
	}
	return false
}
