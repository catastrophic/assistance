package flac

import (
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/go-flac/flacpicture"
	"github.com/go-flac/go-flac"
	"gitlab.com/catastrophic/assistance/fs"
)

const (
	FlacExt = ".flac"
)

var (
	ErrNoFlacHeader   = errors.New("fLaC head incorrect, possibly because of id3v2 tags")
	ErrorUncompressed = errors.New("flac seems to be uncompressed")
	ErrorID3v1Tag     = errors.New("id3v1 tag detected at end of file")
	ErrorMetadataSize = errors.New("excessive metadata blocks size")
)

type Flac struct {
	Path            string
	file            *flac.File
	metadata        *MetaDataBlockVorbisTags
	OriginalVendor  string
	ChannelCount    int
	SampleRate      int
	BitDepth        int
	SampleCount     int64
	DurationSeconds float32
	Duration        string
	Size            int64
	AverageBitRate  int
	MD5             string
	HasCover        bool
	CoverSize       int
}

func New(path string) (*Flac, error) {
	if !fs.FileExists(path) {
		return nil, errors.New("file does not exist: " + path)
	}
	if strings.ToLower(filepath.Ext(path)) != FlacExt {
		return nil, errors.New("file is not a FLAC file")
	}

	info, err := os.Stat(path)
	if err != nil {
		return nil, err
	}
	f, err := flac.ParseFile(path)
	if err != nil {
		if errors.Is(err, flac.ErrorNoFLACHeader) {
			return nil, CheckFLACHead(path)
		}
		return nil, err
	}
	flacFile := &Flac{Path: path, file: f, Size: info.Size()}
	if err := flacFile.parseAudioInfo(); err != nil {
		return nil, err
	}
	if err := flacFile.parseTags(); err != nil {
		return nil, err
	}
	if err := flacFile.parsePictures(); err != nil {
		return nil, err
	}
	return flacFile, nil
}

func NewFromReader(r io.Reader, path string) (*Flac, error) {
	if fs.FileExists(path) {
		return nil, errors.New("file already exists: " + path)
	}
	if strings.ToLower(filepath.Ext(path)) != FlacExt {
		return nil, errors.New("target file is not a FLAC filename")
	}

	f, err := flac.ParseBytes(r)
	if err != nil {
		if errors.Is(err, flac.ErrorNoFLACHeader) {
			return nil, CheckFLACHeadFromReader(r)
		}
		return nil, err
	}
	flacFile := &Flac{Path: path, file: f, Size: int64(len(f.Marshal()))}
	if err := flacFile.parseAudioInfo(); err != nil {
		return nil, err
	}
	if err := flacFile.parseTags(); err != nil {
		return nil, err
	}
	if err := flacFile.parsePictures(); err != nil {
		return nil, err
	}
	return flacFile, nil
}

// ClearMemory since all of the FLAC contents were loaded into []byte.
func (f *Flac) ClearMemory() {
	f.file.Meta = nil
	f.file.Frames = f.file.Frames[:0]
	f.file.Frames = nil
}

func (f *Flac) String() string {
	var tags string
	for k, v := range f.metadata.Tags {
		for _, v2 := range v {
			tags += k + "=" + v2 + " "
		}
	}
	return fmt.Sprintf("%s: FLAC%d/%dHz (%ss), %d bytes, %d avgBitRate, %s", f.Path, f.BitDepth, f.SampleRate, f.Duration, f.Size, f.AverageBitRate, tags)
}

func (f *Flac) parseAudioInfo() error {
	si, err := f.file.GetStreamInfo()
	if err != nil {
		return err
	}
	f.MD5 = hex.EncodeToString(si.AudioMD5)
	f.ChannelCount = si.ChannelCount
	f.SampleRate = si.SampleRate
	f.BitDepth = si.BitDepth
	f.SampleCount = si.SampleCount
	// duration = total samples / sample rate
	f.DurationSeconds = float32(f.SampleCount) / float32(f.SampleRate)
	f.AverageBitRate = int(float32(f.Size*8) / f.DurationSeconds)
	f.Duration = fmt.Sprintf("%.3f", f.DurationSeconds)
	return nil
}

func (f *Flac) parseTags() error {
	for _, meta := range f.file.Meta {
		if meta.Type == flac.VorbisComment {
			tags, err := parseTags(*meta)
			if err != nil {
				return err
			}
			f.OriginalVendor = tags.Vendor
			f.metadata = tags
			return nil
		}
	}
	// if not returned, no relevant block found, starting one
	f.OriginalVendor = "MysteriousVendor"
	f.metadata = NewTags(f.OriginalVendor)
	return nil
}

func (f *Flac) parsePictures() error {
	for _, meta := range f.file.Meta {
		if meta.Type == flac.Picture {
			f.HasCover = true
			// getting picture size
			pic, err := flacpicture.ParseFromMetaDataBlock(*meta)
			if err != nil {
				return err
			}
			// getting total cover size, there might be more than one
			f.CoverSize += len(pic.ImageData)
		}
	}
	return nil
}

func (f *Flac) RawTags() map[string][]string {
	if f.metadata == nil {
		if err := f.parseTags(); err != nil {
			return map[string][]string{}
		}
	}
	return f.metadata.Tags
}

func (f *Flac) SaveTags() error {
	tagsBlock, err := f.metadata.Marshal()
	if err != nil {
		return err
	}
	var foundBlock bool
	for i, meta := range f.file.Meta {
		if meta.Type == flac.VorbisComment {
			f.file.Meta[i] = &tagsBlock
			foundBlock = true
			break
		}
	}
	if !foundBlock {
		f.file.Meta = append(f.file.Meta, &tagsBlock)
	}
	return f.file.Save(f.Path)
}

func (f *Flac) ClearMetadata() {
	// remove everything but streams: tags, padding, seek table, etc
	var streamInfo *flac.MetaDataBlock
	for _, meta := range f.file.Meta {
		if meta.Type == flac.StreamInfo {
			streamInfo = meta
			break
		}
	}
	f.file.Meta = []*flac.MetaDataBlock{streamInfo}
	f.metadata = NewTags(f.Vendor())
}

func (f *Flac) AddCover(cover string, maxSizeKb int) error {
	// resizing cover if necessary
	embeddedArtworkFile, resized, err := fs.ResizeJPGIfNecessary(cover, maxSizeKb)
	if err != nil {
		return err
	}
	if resized {
		defer os.Remove(embeddedArtworkFile)
	}

	imgData, err := ioutil.ReadFile(embeddedArtworkFile)
	if err != nil {
		return err
	}
	return f.AddCoverFromBytes(imgData)
}

// AddCoverFromBytes assumes the cover is of valid size.
func (f *Flac) AddCoverFromBytes(coverData []byte) error {
	picture, err := flacpicture.NewFromImageData(flacpicture.PictureTypeFrontCover, "cover", coverData, "image/jpeg")
	if err != nil {
		panic(err)
	}
	pictureBlock := picture.Marshal()

	f.file.Meta = append(f.file.Meta, &pictureBlock)
	return nil
}

func (f *Flac) Vendor() string {
	return f.metadata.Vendor
}

func (f *Flac) TotalPaddingSize() int {
	var paddingSize int
	for _, m := range f.file.Meta {
		if m.Type == flac.Padding {
			paddingSize += len(m.Data)
		}
	}
	return paddingSize
}
