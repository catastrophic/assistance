package flac

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/catastrophic/assistance/fs"
)

func TestFlac(t *testing.T) {
	fmt.Println("+ Testing Flac...")
	check := assert.New(t)

	f, err := New("../testing/sample.flac")
	check.Nil(err)

	rawTags := f.RawTags()
	check.NotEmpty(rawTags)
	check.Equal([]string{"Album þþ«ł€"}, rawTags["ALBUM"])
	check.Equal(11025, f.SampleRate)
	check.Equal(16, f.BitDepth)
	check.Equal(37478, int(f.SampleCount))
	check.Equal("3.399", f.Duration)
	check.Equal(int64(68443), f.Size)
	check.Equal(161072, f.AverageBitRate)
	check.Equal("23b7c181bbd2de0cb48218bde52c0b1d", f.MD5)
	tags := f.CommonTags()
	check.Equal([]string{"Composer!"}, tags.Composer)
	check.Equal("Gangsta", tags.Genre)
	check.Equal("02", tags.DiscNumber)
	check.Equal("05", tags.TrackNumber)
	check.Equal("09", tags.TotalTracks)
	check.Equal([]string{"Album Artist €«ðøßđŋ"}, tags.AlbumArtist)
	check.Equal([]string{"Original artist."}, tags.Performer)
	check.Equal("Mildly interesting comment.", tags.Description)
	check.Equal([]string{"Best artist àß€«đ"}, tags.Artist)
	check.Equal("Album þþ«ł€", tags.Album)
	check.Equal("2018", tags.Date)
	check.Equal("A title ê€$éèç\"&!!", tags.Title)
	/*check.Equal(3, len(tags.OtherTags))
	check.Equal("FLAC", track.Tags.OtherTags["ENCODED-BY"])
	check.Equal("copyright þæ", track.Tags.OtherTags["COPYRIGHT"])
	check.Equal("http://bestartist.com", track.Tags.OtherTags["CONTACT"])*/
	check.False(f.HasCover)
	check.Equal(0, f.CoverSize)
	check.True(f.HasMinimalTags())
	check.Nil(f.CheckMinimalTags())
	_, err = exec.LookPath("flac")
	if err == nil {
		check.Nil(f.CheckCompression())
		check.Nil(f.Check())
	}
	check.False(f.CheckTrackNumberInFilename())
	check.False(f.CheckDiscNumberInFilename())
	check.False(f.CheckFilenameContainsStartOfTitle(5))
	fmt.Println(f.String())

	f, err = New("../testing/sample_without_tags.flac")
	check.Nil(err)
	check.False(f.HasMinimalTags())
	check.NotNil(f.CheckMinimalTags())
	check.True(errors.Is(f.CheckMinimalTags(), ErrorMinimalTags))
	check.Equal("missing minimal tag(s) ARTIST, TITLE, ALBUM, TRACKNUMBER", f.CheckMinimalTags().Error())

	// checking filenames
	check.Nil(fs.CopyFile("../testing/sample.flac", "../testing/02.05 A title sample_temp.flac", false))
	f, err = New("../testing/02.05 A title sample_temp.flac")
	check.Nil(err)
	check.True(f.CheckTrackNumberInFilename())
	check.True(f.CheckDiscNumberInFilename())
	check.True(f.CheckFilenameContainsStartOfTitle(5))
	check.False(f.CheckFilenameContainsStartOfTitle(15))
	check.Nil(f.CheckForID3v1Tags())
	check.Nil(os.Remove("../testing/02.05 A title sample_temp.flac"))

	// checking invalid FLACs
	_, err = New("../testing/sample_with_id3_tags.flac")
	check.Equal(ErrorID3v2Header, err)
	// checking id3v1
	f, err = New("../testing/sample_with_id3v1_tags.flac")
	check.Nil(err)
	check.NotNil(f.CheckForID3v1Tags())

	// TODO ADD CHECK
	//f, err = New("../testing/sample_with_id3v1_tags.flac")
	//check.NotNil(err)

	check.Nil(fs.CopyFile("../testing/sample.flac", "../testing/sample_temp.flac", false))
	defer os.Remove("../testing/sample_temp.flac")
	f, err = New("../testing/sample_temp.flac")
	check.Nil(err)
	check.NotEqual(0, len(f.metadata.Tags))
	fmt.Println(f.String())
	check.Equal("Lavf52.64.2", f.OriginalVendor)
	check.Nil(f.SetTitle("édofihrezoihTITILE!!!", "versionnnn"))
	check.Nil(f.SetArtists([]string{"ARtist&é&àçé_ç!!!", "the other guy"}, true))
	check.Nil(f.SaveTags())
	fmt.Println(f.String())

	// checking clearing all metadata
	f, err = New("../testing/sample_temp.flac")
	check.Nil(err)
	check.NotEqual(0, len(f.metadata.Tags))
	fmt.Println(f.String())
	f.ClearMetadata()
	check.Nil(f.SaveTags())
	f, err = New("../testing/sample_temp.flac")
	check.Nil(err)
	check.Equal(0, len(f.metadata.Tags))
	fmt.Println(f.String())

	// checking mqa
	f, err = New("../testing/mqa16bit.flac")
	check.Nil(err)
	isMQA, _, err := f.CheckForMQASyncword()
	check.Nil(err)
	check.True(isMQA)
	check.True(f.CheckForMQAMetadata())

	f, err = New("../testing/mqa24bit.flac")
	check.Nil(err)
	isMQA, _, err = f.CheckForMQASyncword()
	check.Nil(err)
	check.True(isMQA)
	check.True(f.CheckForMQAMetadata())

	f, err = New("../testing/notmqa.flac")
	check.Nil(err)
	isMQA, _, err = f.CheckForMQASyncword()
	check.Nil(err)
	check.False(isMQA)
	check.False(f.CheckForMQAMetadata())
}
