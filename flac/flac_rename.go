package flac

import (
	"bytes"
	"errors"
	"fmt"
	"strings"
	"text/template"

	"gitlab.com/catastrophic/assistance/fs"
)

func (f *Flac) generateName(filenameTemplate string) (string, error) {
	// TODO input: other sources, if tags not sufficient?

	tags := f.CommonTags()

	discNumber := tags.DiscNumber
	if discNumber == "" {
		// TODO do better...
		discNumber = "01"
	}
	totalTracks := tags.TotalTracks
	if totalTracks == "" {
		// TODO do better...
		totalTracks = "01"
	}
	trackNumber := tags.TrackNumber
	if trackNumber == "" {
		return "", errors.New("could not find track number tag for " + f.Path)
	}
	trackArtist := SmartArtistList(tags.Artist)
	if trackArtist == "" {
		return "", errors.New("could not find track artist tag for " + f.Path)
	}
	trackTitle := tags.Title
	if trackTitle == "" {
		return "", errors.New("could not find track title tag for " + f.Path)
	}
	albumTitle := tags.Album
	if albumTitle == "" {
		return "", errors.New("could not find album title tag for " + f.Path)
	}
	albumArtist := SmartArtistList(tags.AlbumArtist)
	if albumArtist == "" {
		// TODO do better...
		albumArtist = trackArtist
	}
	trackYear := tags.Date
	if trackYear == "" {
		// TODO do better...
		trackYear = "0000"
	}

	r := strings.NewReplacer(
		"$dn", "{{$dn}}",
		"$dt", "{{$dt}}",
		"$tn", "{{$tn}}",
		"$ta", "{{$ta}}",
		"$tt", "{{$tt}}",
		"$aa", "{{$aa}}",
		"$td", "{{$td}}",
		"$t", "{{$t}}",
		"$y", "{{$y}}",
		"{", "ÆÆ", // otherwise golang's template throws a fit if '{' or '}' are in the user pattern
		"}", "¢¢", // assuming these character sequences will probably not cause conflicts.
	)

	// replace with all valid epub parameters
	tmpl := fmt.Sprintf(`{{$dn := %q}}{{$dt := %q}}{{$tn := %q}}{{$ta := %q}}{{$tt := %q}}{{$aa := %q}}{{$td := %q}}{{$t := %q}}{{$y := %q}}%s`,
		fs.SanitizePath(discNumber),
		fs.SanitizePath(totalTracks),
		fs.SanitizePath(trackNumber),
		fs.SanitizePath(trackArtist),
		fs.SanitizePath(trackTitle),
		fs.SanitizePath(albumArtist),
		fs.SanitizePath(f.Duration), // TODO min:sec or hh:mm:ss
		fs.SanitizePath(albumTitle),
		fs.SanitizePath(trackYear),
		r.Replace(filenameTemplate))

	var doc bytes.Buffer
	te := template.Must(template.New("hop").Parse(tmpl))
	if err := te.Execute(&doc, nil); err != nil {
		return f.Path, err
	}
	newName := strings.TrimSpace(doc.String())
	// trim spaces around all internal folder names
	var trimmedParts = strings.Split(newName, "/")
	for i, part := range trimmedParts {
		trimmedParts[i] = strings.TrimSpace(part)
	}
	// recover brackets
	r2 := strings.NewReplacer(
		"ÆÆ", "{",
		"¢¢", "}",
	)

	// TODO !!!!!!!!!!! only does filename, not parent folders!!!

	return r2.Replace(strings.Join(trimmedParts, "/")) + FlacExt, nil
}
