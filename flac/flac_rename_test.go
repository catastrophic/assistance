package flac

import (
	"fmt"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/catastrophic/assistance/fs"
	"gitlab.com/catastrophic/assistance/logthis"
)

func TestFlacRename(t *testing.T) {
	fmt.Println("+ Testing flac rename...")
	check := assert.New(t)
	// setup logger
	logthis.SetLevel(3)

	path := "../testing"
	flac := "sample.flac"
	flacTagless := "sample_without_tags.flac"

	track, err := New(filepath.Join(path, flac))
	check.Nil(err)

	// testing filename generation
	name, err := track.generateName("$dn | $dt | $tn | $ta | $aa | $tt | $td | $t | $y")
	check.Nil(err)
	check.Equal("02 | 09 | 05 | Best artist àß€«đ | Album Artist €«ðøßđŋ | A title ê€$éèç\"&!! | 3.399 | Album þþ«ł€ | 2018.flac", name)
	name, err = track.generateName("$dn.$tn. $ta - $tt ($td)")
	check.Nil(err)
	check.Equal("02.05. Best artist àß€«đ - A title ê€$éèç\"&!! (3.399).flac", name)
	name, err = track.generateName("$dn.$tn. $tt ($td)")
	check.Nil(err)
	check.Equal("02.05. A title ê€$éèç\"&!! (3.399).flac", name)
	fmt.Println(name)

	track2, err := New(filepath.Join(path, flacTagless))
	check.Nil(err)
	check.False(track2.HasMinimalTags())

	fmt.Println(track.Path)
	fmt.Println(track.CommonTags().TrackNumber)
	check.False(track.checkNumberInFilename(true))

	withPaddedNumber := filepath.Join(path, "05 "+flac)
	check.Nil(fs.CopyFile(filepath.Join(path, flac), withPaddedNumber, false))
	defer os.Remove(withPaddedNumber)
	track3, err := New(filepath.Join(path, withPaddedNumber))
	check.Nil(err)
	check.True(track3.checkNumberInFilename(true))

	withNumber := filepath.Join(path, "5. "+flac)
	check.Nil(fs.CopyFile(filepath.Join(path, flac), withNumber, false))
	defer os.Remove(withNumber)
	track4, err := New(filepath.Join(path, withNumber))
	check.Nil(err)
	check.True(track4.checkNumberInFilename(true))

	check.False(track.CheckFilenameContainsStartOfTitle(10))
	withTitle := filepath.Join(path, "A title ê€$éèç.flac")
	check.Nil(fs.CopyFile(filepath.Join(path, flac), withTitle, false))
	defer os.Remove(withTitle)
	track5, err := New(filepath.Join(path, withTitle))
	check.Nil(err)
	check.True(track5.CheckFilenameContainsStartOfTitle(10))
}
