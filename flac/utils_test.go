package flac

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFlacUtils(t *testing.T) {
	fmt.Println("+ Testing Flac/Utils...")
	check := assert.New(t)

	check.Equal("abcd1234", OnlyKeepLettersAndNumbers("  !!!a-b`c|d €@1°2?3/4§**"))
	check.True(StringContainsStartOfAnother("01. Hi!! a-b, f__k ''hop'' dud3", "a/b f**k ``hop''", 10))
	check.False(StringContainsStartOfAnother("01. Hi!! a-b, f__k ''hop'' dud3", "a/b f**k ``hkop''", 10))
	check.False(StringContainsStartOfAnother("01. Hi!! a-b, f__k", "a/b f**k ``hkop''", 5))
	check.True(StringContainsStartOfAnother("01. Hi!! a-bbbb, ", "a/bbbb (feat. This Guy)", 5))
	check.True(StringContainsStartOfAnother("01. Hi!! a-bbbb, ", "a/bbbb (feat. This Guy)", 10))
	check.True(StringContainsStartOfAnother("01. Hi!! a-bbbb, ", "a/bbbb (feat. This Guy) EP", 10))
	check.True(StringContainsStartOfAnother("01. Hi!! a-bbbb, ", "a/bbbb EP", 10))
	check.True(StringContainsStartOfAnother("01. Hi!! a-bbbb, ", "a/bbbb (EP)", 10))
	check.True(StringContainsStartOfAnother("01. Hi!! a-bbbb, ", "a/bbbb [EP]", 10))
	check.True(StringContainsStartOfAnother("01. Hi!! a-bbbbece, ", "a/bbbbècé [EP]", 10))
}
