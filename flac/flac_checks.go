package flac

import (
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"

	"github.com/mewkiz/flac"
	"github.com/pkg/errors"
)

const (
	regexpTrackNumber         = `.*?(\w?\d+).*?`
	regexpCombinedTrackNumber = `^(\d+)[-/](\d+)$`
	blankMD5                  = "00000000000000000000000000000000"
	mqaSyncword               = 0xbe0498c88
	mqaEncoder                = "mqaencode"
	errorMD5                  = "MD5 signature mismatch"
	errorNoMD5                = "cannot check MD5 signature since it was unset in the STREAMINFO"
	minRatioForPadded         = .9
)

var ErrorMinimalTags = errors.New("missing minimal tag(s)")

func CheckFLACHeadFromReader(r io.Reader) error {
	buffer := make([]byte, 3)
	_, err := io.ReadFull(r, buffer)
	if err != nil {
		return err
	}
	switch string(buffer) {
	case "fLaC":
		return nil
	case "ID3":
		return ErrorID3v2Header
	default:
		return ErrorFlacHeader
	}
}

func CheckFLACHead(path string) error {
	fFile, err := os.Open(path)
	if err != nil {
		return err
	}
	defer fFile.Close()
	return CheckFLACHeadFromReader(fFile)
}

func (f *Flac) HasMinimalTags() bool {
	t := f.CommonTags()
	if len(t.Artist) == 0 || t.Album == "" || t.TrackNumber == "" || t.Title == "" {
		return false
	}
	return true
}

func (f *Flac) CheckMinimalTags() error {
	var missing []string
	t := f.CommonTags()
	if len(t.Artist) == 0 {
		missing = append(missing, TagArtist)
	}
	if t.Title == "" {
		missing = append(missing, TagTitle)
	}
	if t.Album == "" {
		missing = append(missing, TagAlbum)
	}
	if t.TrackNumber == "" {
		missing = append(missing, TagTrackNumber)
	}
	if len(missing) != 0 {
		return fmt.Errorf("%w %s", ErrorMinimalTags, strings.Join(missing, ", "))
	}
	return nil
}

func (f *Flac) CheckMinimalTagsForSingle() error {
	var missing []string
	t := f.CommonTags()
	if len(t.Artist) == 0 {
		missing = append(missing, TagArtist)
	}
	if t.Title == "" {
		missing = append(missing, TagTitle)
	}
	if len(missing) != 0 {
		return fmt.Errorf("%w %s", ErrorMinimalTags, strings.Join(missing, ", "))
	}
	return nil
}

func (f *Flac) CompareEncoding(o *Flac) bool {
	return f.CompareBitDepth(o) && f.CompareSampleRate(o)
}

func (f *Flac) CompareSampleRate(o *Flac) bool {
	return f.SampleRate == o.SampleRate
}

func (f *Flac) CompareBitDepth(o *Flac) bool {
	return f.BitDepth == o.BitDepth
}

func (f *Flac) CheckCompression() error {
	uncompressedSize := int64(f.ChannelCount) * (f.SampleCount * int64(f.BitDepth)) / 8
	if f.Size > uncompressedSize {
		return ErrorUncompressed
	}
	return nil
}

// Check flac integrity.
func (f *Flac) Check() error {
	output, err := exec.Command("flac", "-wt", f.Path).CombinedOutput()
	if err != nil {
		strOutput := string(output)
		if strings.Contains(strOutput, errorMD5) {
			return errors.New(f.Path + ": " + errorMD5)
		}
		if strings.Contains(strOutput, errorNoMD5) {
			// known flac bug: https://sourceforge.net/p/flac/bugs/478/
			// checking if the md5 is truly unset or if just the first byte
			if strings.EqualFold(f.MD5, blankMD5) {
				return errors.New(f.Path + ": " + errorNoMD5)
			} else {
				// if it was corrupt flac would have returned this first, so if we're here then everything is fine
				return nil
			}
		}
		return errors.Wrap(err, "flac "+f.Path+" failed integrity check")
	}
	return nil
}

func getOriginalMQASampleRate(c uint32) uint32 {
	// with help from https://github.com/purpl3F0x/MQA_identifier
	var base, multiplier uint32
	if (c & 1) == 1 {
		base = 48000
	} else {
		base = 44100
	}
	multiplier = 1 << (((c >> 3) & 1) | (((c >> 2) & 1) << 1) | (((c >> 1) & 1) << 2))
	// double for DSD
	if multiplier > 16 {
		multiplier *= 2
	}
	return base * multiplier
}

func (f *Flac) CheckForMQASyncword() (bool, uint32, error) {
	var numSyncwordsFound int
	var numSamplesInspected, originalSampleRate uint32
	// with help from https://github.com/purpl3F0x/MQA_identifier
	stream, err := flac.ParseFile(f.Path)
	if err != nil {
		return numSyncwordsFound > 1, originalSampleRate, err
	}
out:
	for {
		frame, err := stream.ParseNext()
		if err == io.EOF {
			break
		}
		if err != nil {
			return numSyncwordsFound > 1, originalSampleRate, err
		}
		// only stereo files can be MQA
		if frame.Channels.Count() != 2 {
			return numSyncwordsFound > 1, originalSampleRate, err
		}

		// trying for different offsets BECAUSE REASONS
		for k := 0; k <= 16; k++ {
			var xor uint64
			for i := range frame.Subframes[0].Samples {
				// checking for the MQA syncword by doing XOR on L + R channels
				a := frame.Subframes[0].Samples[i]
				b := frame.Subframes[1].Samples[i]
				pos := frame.BitsPerSample - uint8(k)
				xor |= uint64((uint32(a) ^ uint32(b)) >> pos & 1)

				if xor == mqaSyncword {
					numSyncwordsFound++
					if originalSampleRate == 0 {
						// getting original sample rate
						var orsf uint32
						for j := 3; j < 7; j++ {
							c := frame.Subframes[0].Samples[i+j]
							d := frame.Subframes[1].Samples[i+j]
							k := ((uint32(c) ^ uint32(d)) >> pos) & 1
							orsf |= k << (6 - j)
						}
						originalSampleRate = getOriginalMQASampleRate(orsf)
					}
				} else {
					xor = (xor << 1) & 0xFFFFFFFFF
				}
				numSamplesInspected++
				// enough frames were checked (2 seconds), getting out
				if numSamplesInspected >= 2*frame.SampleRate {
					break out
				}
			}
		}
	}
	// checking if the syncword was found more than once, and not by some crazy chance
	return numSyncwordsFound > 1, originalSampleRate, err
}
func (f *Flac) CheckForMQAMetadata() bool {
	// checking all tag names & values for the name of the MQA encoder
	for k, v := range f.RawTags() {
		if strings.Contains(strings.ToLower(k), mqaEncoder) {
			return true
		}
		for _, t := range v {
			if strings.Contains(strings.ToLower(t), mqaEncoder) {
				return true
			}
		}
	}
	return false
}

func (f *Flac) CheckForPaddedBits() (bool, int, error) {
	var paddedSamples, subframesCount, wastedBits int
	stream, err := flac.ParseFile(f.Path)
	if err != nil {
		return false, -1, err
	}

	for {
		fr, err := stream.ParseNext()
		if err == io.EOF {
			break
		}
		if err != nil {
			return false, -1, err
		}

		for i := range fr.Subframes {
			subframesCount++
			wastedBits += int(fr.Subframes[i].Wasted)
			if fr.Subframes[i].Wasted != 0 {
				paddedSamples++
			}
		}
	}

	if paddedSamples != 0 {
		// average true bit depth
		trueBitDepth := f.BitDepth - wastedBits/paddedSamples
		// ratio of padded samples
		paddedRatio := float64(paddedSamples) / float64(subframesCount)
		// only counting if all samples have wasted bits
		return paddedRatio >= minRatioForPadded, trueBitDepth, nil
	}
	// no padded bits
	return false, f.BitDepth, nil
}

func (f *Flac) checkNumberInFilename(trackNb bool) bool {
	var toCheck string
	tags := f.CommonTags()
	if trackNb {
		toCheck = tags.TrackNumber
	} else {
		toCheck = tags.DiscNumber
	}

	filename := filepath.Base(f.Path)
	re := regexp.MustCompile(regexpTrackNumber)
	hits := re.FindAllStringSubmatch(filename, -1)
	if len(hits) == 0 {
		return false
	}
	var isNumber bool
	number, err := strconv.Atoi(toCheck)
	if err == nil {
		isNumber = true
	}
	for _, h := range hits {
		capturedNumber := h[1]
		if isNumber && capturedNumber == fmt.Sprintf("%d", number) || capturedNumber == fmt.Sprintf("%02d", number) {
			return true
		}
		if !isNumber && capturedNumber == toCheck {
			return true
		}
	}
	return false
}

func (f *Flac) CheckTrackNumberInFilename() bool {
	return f.checkNumberInFilename(true)
}

func (f *Flac) CheckDiscNumberInFilename() bool {
	return f.checkNumberInFilename(false)
}

func (f *Flac) CheckFilenameContainsStartOfTitle(minTitleSize int) bool {
	filename := filepath.Base(f.Path)
	tags := f.CommonTags()
	return StringContainsStartOfAnother(filename, tags.Title, minTitleSize)
}

func (f *Flac) CheckForID3v1Tags() error {
	// id3v1 tags are appended at the end of the files
	// they begin by a marker "TAG" and have a length of 128 bytes
	fi, err := os.Open(f.Path)
	if err != nil {
		return err
	}
	defer fi.Close()

	_, err = fi.Seek(-128, 2)
	if err != nil {
		return err
	}
	tagMarker := make([]byte, 3)
	n, err := io.ReadFull(fi, tagMarker)
	if err != nil {
		return err
	}
	if n != 3 {
		return errors.New("could not read 3 bytes at offset -128")
	}
	if string(tagMarker) == "TAG" {
		return ErrorID3v1Tag
	}
	return nil
}

func (f *Flac) CheckMetadataSize(maxSize int) error {
	metadataSize := int(f.Size) - len(f.file.Frames)
	if metadataSize > maxSize {
		return fmt.Errorf("%w (%dKiB)", ErrorMetadataSize, metadataSize/1024)
	}
	return nil
}

func (f *Flac) CheckNotCombinedTrackNumber() bool {
	// tag titles are case insensitive. To correctly identify existing tags,
	// building a map of CANONICAL:FOUND IN TRACK tag titles
	tagTitlesMap := make(map[string]string)
	for tagTitle := range f.metadata.Tags {
		for _, t := range validTags {
			if strings.EqualFold(tagTitle, t) {
				tagTitlesMap[t] = tagTitle
				break
			}
		}
	}
	re := regexp.MustCompile(regexpCombinedTrackNumber)
	if len(f.metadata.Tags[tagTitlesMap[TagTrackNumber]]) != 0 {
		trackNumber := f.metadata.Tags[tagTitlesMap[TagTrackNumber]][0]
		if re.MatchString(trackNumber) {
			return false
		}
	}
	return true
}
