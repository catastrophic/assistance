package flac

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/catastrophic/assistance/strslice"
)

const (
	// TagTitle Track/Work name
	TagTitle = "TITLE"
	// TagVersion The version field may be used to differentiate multiple versions of the same track title in a single collection. (e.g. remix info)
	TagVersion = "VERSION"
	// TagRemixer is the tracker remixer
	TagRemixer = "REMIXER"
	// TagAlbum The collection name to which this track belongs
	TagAlbum = "ALBUM"
	// TagArtist The artist generally considered responsible for the work.
	TagArtist = "ARTIST"
	// TagAlbumArtist is the main album artist
	TagAlbumArtist = "ALBUMARTIST"
	// TagTrackNumber The track number of this piece if part of a specific larger collection or album
	TagTrackNumber = "TRACKNUMBER"
	// TagDiscNumber is the disc number, for multi-disc releases
	TagDiscNumber = "DISCNUMBER"
	// TagDiscTotal is the total number of discs, for multi-disc releases
	TagDiscTotal = "DISCTOTAL"
	// TagDiscTotal2 is an alternative to DISCTOTAL
	TagDiscTotal2 = "TOTALDISCS"
	// TagTrackTotal is the total number of tracks for the current disc
	TagTrackTotal = "TRACKTOTAL"
	// TagTrackTotal2 is an alternative to TRACKTOTAL
	TagTrackTotal2 = "TOTALTRACKS"
	// TagPerformer The artist(s) who performed the work. In classical music this would be the conductor, orchestra, soloists.
	TagPerformer = "PERFORMER"
	// TagComposer is the composer (mainly for classical music)
	TagComposer = "COMPOSER"
	// TagAuthor is the author of the release
	TagAuthor = "AUTHOR"
	// TagConductor is the conductor (mainly for classical music)
	TagConductor = "CONDUCTOR"
	// TagMedia is the type of media (Digital WEB release, CD, etc)
	TagMedia = "MEDIA"
	// TagCopyright Copyright attribution
	TagCopyright = "COPYRIGHT"
	// TagLicense License information, eg, 'All Rights Reserved', 'Any Use Permitted', a URL to a license such as a Creative Commons license.
	TagLicense = "LICENSE"
	// TagRecordLabel Name of the organization producing the track (i.e. the 'record label')
	TagRecordLabel = "ORGANIZATION"
	// TagRecordLabel2 is an alternative for TagRecordLabel
	TagRecordLabel2 = "LABEL"
	// TagDescription A short text description of the contents
	TagDescription = "DESCRIPTION"
	// TagGenre A short text indication of music genre
	TagGenre = "GENRE"
	// TagDate Date the track was released
	TagDate = "DATE"
	// TagYear Year the track was released
	TagYear = "YEAR"
	// TagLocation Location where track was recorded
	TagLocation = "LOCATION"
	// TagContact Contact information for the creators or distributors of the track.
	TagContact = "CONTACT"
	// TagISRC ISRC number for the track.
	TagISRC = "ISRC"
	// TagUPC Universal Product Code for this release
	TagUPC = "UPC"
	// TagBarcode for this release
	TagBarcode = "BARCODE"
	// TagDuration of the track
	TagDuration = "DURATION"
)

var validTags = []string{TagTitle, TagVersion, TagRemixer, TagAlbum, TagArtist, TagAlbumArtist, TagTrackNumber, TagDiscNumber, TagDiscTotal, TagDiscTotal2, TagTrackTotal, TagTrackTotal2, TagPerformer, TagComposer, TagAuthor, TagConductor, TagMedia, TagRecordLabel, TagRecordLabel2, TagDescription, TagGenre, TagDate, TagYear, TagISRC, TagUPC, TagBarcode, TagDuration}

type Tags struct {
	TrackNumber string
	TotalTracks string
	DiscNumber  string
	TotalDiscs  string
	Artist      []string
	AlbumArtist []string
	Performer   []string
	Conductor   []string
	Author      []string
	Composer    []string
	Remixer     []string
	Guest       []string
	Title       string
	Description string
	Date        string
	Year        string
	Genre       string
	Album       string
	Label       string
	Version     string
	ISRC        string
	UPC         string
	Media       string
	Duration    string
	OtherTags   map[string]string
}

func (t *Tags) String() string {
	normalTags := fmt.Sprintf("Disc#/Total: %s/%s | Track#/Total: %s/%s | Artist: %s | Title: %s | AlbumArtist: %s | Album: %s | Year: %s | Genre: %s | Performer: %s | Composer: %s | Description: %s | Label: %s", t.DiscNumber, t.TotalDiscs, t.TrackNumber, t.TotalTracks, SmartArtistList(t.Artist), t.Title, SmartArtistList(t.AlbumArtist), t.Album, t.Date, t.Genre, SmartArtistList(t.Performer), SmartArtistList(t.Composer), t.Description, t.Label)
	var extraTags string
	for k, v := range t.OtherTags {
		extraTags += fmt.Sprintf("%s: %s | ", k, v)
	}
	if extraTags != "" {
		return normalTags + " | " + extraTags
	}
	return normalTags
}

func IsValidTag(tagName string) bool {
	return strslice.ContainsCaseInsensitive(validTags, tagName)
}

func (t *Tags) SpecificTagsOnly(tagsWanted []string) {
	var specific Tags
	for _, tag := range tagsWanted {
		switch strings.ToUpper(tag) {
		case TagVersion:
			specific.Version = t.Version
		case TagRemixer:
			specific.Remixer = t.Remixer
		case TagAlbumArtist:
			specific.AlbumArtist = t.AlbumArtist
		case TagDiscNumber:
			specific.DiscNumber = t.DiscNumber
		case TagDiscTotal, TagDiscTotal2:
			specific.TotalDiscs = t.TotalDiscs
		case TagTrackTotal, TagTrackTotal2:
			specific.TotalTracks = t.TotalTracks
		case TagPerformer:
			specific.Performer = t.Performer
		case TagComposer:
			specific.Composer = t.Composer
		case TagAuthor:
			specific.Author = t.Author
		case TagConductor:
			specific.Conductor = t.Conductor
		case TagMedia:
			specific.Media = t.Media
		case TagRecordLabel, TagRecordLabel2:
			specific.Label = t.Label
		case TagDescription:
			specific.Description = t.Description
		case TagGenre:
			specific.Genre = t.Genre
		case TagDate, TagYear:
			specific.Date = t.Date
		case TagISRC:
			specific.ISRC = t.ISRC
		case TagUPC, TagBarcode:
			specific.UPC = t.UPC
		case TagDuration:
			specific.Duration = t.Duration
		}
	}
	// these are always set, even if not user-specified
	specific.Artist = t.Artist
	specific.Album = t.Album
	specific.Title = t.Title
	specific.TrackNumber = t.TrackNumber
	// these two are needed for the creation of accurate artist descriptions
	specific.Guest = t.Guest
	specific.Remixer = t.Remixer
	*t = specific
}

// SmartArtistList returns a "a1, a2, & a3" list of artists.
func SmartArtistList(artists []string) string {
	if len(artists) <= 2 {
		return strings.Join(artists, " & ")
	}
	return strings.Join(artists[:len(artists)-1], ", ") + ", & " + artists[len(artists)-1]
}

// PaddedNumber for track.
func (t *Tags) PaddedNumber() string {
	number, err := strconv.Atoi(t.TrackNumber)
	if err != nil {
		// error, using the original.
		return t.TrackNumber
	}
	return fmt.Sprintf("%02d", number)
}
