package flac

// some code forked from https://github.com/go-flac/flacvorbis

import (
	"bytes"
	"encoding/binary"
	"errors"
	"io"
	"sort"
	"strings"

	"github.com/go-flac/go-flac"
)

var (
	ErrorNotVorbisComment = errors.New("not a vorbis comment metadata block")
	ErrorUnexpectedEof    = errors.New("unexpected end of stream")
	ErrorInvalidFieldName = errors.New("malformed Field Name")
	ErrorID3v2Header      = errors.New("found id3v2 tags, invalid FLAC file")
	ErrorFlacHeader       = errors.New("invalid fLaC header")
)

func encodeUint32(n uint32) []byte {
	buf := bytes.NewBuffer([]byte{})
	if err := binary.Write(buf, binary.LittleEndian, n); err != nil {
		panic(err)
	}
	return buf.Bytes()
}

func readUint32(r io.Reader) (res uint32, err error) {
	err = binary.Read(r, binary.LittleEndian, &res)
	return
}

func packStr(w io.Writer, s string) error {
	data := []byte(s)
	if _, err := w.Write(encodeUint32(uint32(len(data)))); err != nil {
		return err
	}
	_, err := w.Write(data)
	return err
}

func isValidKey(key string) bool {
	for _, char := range key {
		if char < 0x20 || char > 0x7d || char == '=' {
			return false
		}
	}
	return true
}

type MetaDataBlockVorbisTags struct {
	Vendor string
	Tags   map[string][]string
}

// New creates a new MetaDataBlockVorbisTags.
func NewTags(vendor string) *MetaDataBlockVorbisTags {
	return &MetaDataBlockVorbisTags{
		vendor,
		make(map[string][]string),
	}
}

// Get get all comments with field name specified by the key parameter.
func (c *MetaDataBlockVorbisTags) Get(key string) ([]string, error) {
	if !isValidKey(key) {
		return []string{}, ErrorInvalidFieldName
	}
	if val, ok := c.Tags[key]; ok {
		return val, nil
	}
	return []string{}, nil
}

// Set adds a key-val pair to the comments.
func (c *MetaDataBlockVorbisTags) Set(key string, val []string) error {
	if !isValidKey(key) {
		return ErrorInvalidFieldName
	}
	c.Tags[key] = val
	return nil
}

// Add adds a key-val pair to the comments.
func (c *MetaDataBlockVorbisTags) Add(key string, val string) error {
	for _, char := range key {
		if char < 0x20 || char > 0x7d || char == '=' {
			return ErrorInvalidFieldName
		}
	}
	c.Tags[key] = append(c.Tags[key], val)
	return nil
}

// Marshal marshals this block back into a flac.MetaDataBlock.
func (c MetaDataBlockVorbisTags) Marshal() (flac.MetaDataBlock, error) {
	f := flac.MetaDataBlock{
		Type: flac.VorbisComment,
	}
	data := bytes.NewBuffer([]byte{})
	if err := packStr(data, c.Vendor); err != nil {
		return f, err
	}
	var tags []string
	for k, v := range c.Tags {
		for _, l := range v {
			tags = append(tags, k+"="+l)
		}
	}
	// ensure that tags are always written in same order
	sort.Strings(tags)
	data.Write(encodeUint32(uint32(len(tags))))
	for _, cmt := range tags {
		if err := packStr(data, cmt); err != nil {
			return f, err
		}
	}
	f.Data = data.Bytes()
	return f, nil
}

// parseTags parses an existing picture MetaDataBlock.
func parseTags(meta flac.MetaDataBlock) (*MetaDataBlockVorbisTags, error) {
	if meta.Type != flac.VorbisComment {
		return nil, ErrorNotVorbisComment
	}

	reader := bytes.NewReader(meta.Data)
	res := new(MetaDataBlockVorbisTags)

	vendorLen, err := readUint32(reader)
	if err != nil {
		return nil, err
	}
	vendorBytes := make([]byte, vendorLen)
	nn, err := reader.Read(vendorBytes)
	if err != nil {
		return nil, err
	}
	if nn != int(vendorLen) {
		return nil, ErrorUnexpectedEof
	}
	res.Vendor = string(vendorBytes)

	cmtCount, err := readUint32(reader)
	if err != nil {
		return nil, err
	}
	res.Tags = make(map[string][]string, cmtCount)
	for i := 0; i < int(cmtCount); i++ {
		tagLen, err := readUint32(reader)
		if err != nil {
			return nil, err
		}
		tagBytes := make([]byte, tagLen)
		nn, err := reader.Read(tagBytes)
		if err != nil {
			return nil, err
		}
		if nn != int(tagLen) {
			return nil, ErrorUnexpectedEof
		}
		parts := strings.SplitN(string(tagBytes), "=", 2)
		res.Tags[parts[0]] = append(res.Tags[parts[0]], parts[1])
	}
	return res, nil
}
