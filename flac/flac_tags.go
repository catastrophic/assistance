package flac

import (
	"regexp"
	"strings"
	"time"
)

func (f *Flac) SetTag(field string, value []string) error {
	if len(value) == 1 && value[0] == "" {
		return nil
	}
	return f.metadata.Set(field, value)
}

func (f *Flac) AddTag(field string, value string) error {
	if value != "" {
		return f.metadata.Add(field, value)
	}
	return nil
}

func (f *Flac) SetTitle(title, version string) error {
	if err := f.SetTag(TagTitle, []string{title}); err != nil {
		return err
	}
	return f.SetTag(TagVersion, []string{version})
}

func (f *Flac) SetAlbum(value string) error {
	return f.SetTag(TagAlbum, []string{value})
}

func (f *Flac) SetGenre(value string) error {
	return f.SetTag(TagGenre, []string{value})
}

func (f *Flac) setArtistTag(field string, values []string, separateTags bool) error {
	if separateTags {
		return f.metadata.Set(field, values)
	}
	return f.SetTag(field, []string{SmartArtistList(values)})
}

func (f *Flac) SetArtists(values []string, separateTags bool) error {
	return f.setArtistTag(TagArtist, values, separateTags)
}

func (f *Flac) SetAlbumArtists(values []string, separateTags bool) error {
	return f.setArtistTag(TagAlbumArtist, values, separateTags)
}

func (f *Flac) SetPerformers(values []string, separateTags bool) error {
	return f.setArtistTag(TagPerformer, values, separateTags)
}

func (f *Flac) SetConductors(values []string, separateTags bool) error {
	return f.setArtistTag(TagConductor, values, separateTags)
}

func (f *Flac) SetAuthors(values []string, separateTags bool) error {
	return f.setArtistTag(TagAuthor, values, separateTags)
}

func (f *Flac) SetComposers(values []string, separateTags bool) error {
	return f.setArtistTag(TagComposer, values, separateTags)
}

func (f *Flac) SetRemixers(values []string, separateTags bool) error {
	return f.setArtistTag(TagRemixer, values, separateTags)
}

func (f *Flac) SetTrackNumber(current, total string) error {
	if err := f.SetTag(TagTrackNumber, []string{current}); err != nil {
		return err
	}
	if err := f.SetTag(TagTrackTotal, []string{total}); err != nil {
		return err
	}
	return f.SetTag(TagTrackTotal2, []string{total})
}

func (f *Flac) SetDiscNumber(current, total string) error {
	if err := f.SetTag(TagDiscNumber, []string{current}); err != nil {
		return err
	}
	if err := f.SetTag(TagDiscTotal, []string{total}); err != nil {
		return err
	}
	return f.SetTag(TagDiscTotal2, []string{total})
}

func (f *Flac) SetRecordLabel(label string) error {
	if err := f.SetTag(TagRecordLabel, []string{label}); err != nil {
		return err
	}
	return f.SetTag(TagRecordLabel2, []string{label})
}

func (f *Flac) SetUPC(upc string) error {
	if err := f.SetTag(TagUPC, []string{upc}); err != nil {
		return err
	}
	return f.SetTag(TagBarcode, []string{upc})
}

func (f *Flac) SetISRC(isrc string) error {
	return f.SetTag(TagISRC, []string{isrc})
}

func (f *Flac) SetMedia(value string) error {
	return f.SetTag(TagMedia, []string{value})
}

func (f *Flac) SetDescription(value string) error {
	return f.SetTag(TagDescription, []string{value})
}

func (f *Flac) SetDuration(value string) error {
	return f.SetTag(TagDuration, []string{value})
}

func (f *Flac) SetReleaseDate(value string) error {
	if err := f.SetTag(TagDate, []string{value}); err != nil {
		return err
	}
	releaseDate, err := time.Parse("2006-01-02", value)
	if err == nil {
		return f.SetTag(TagYear, []string{releaseDate.Format("2006")})
	}
	return nil
}

func (f *Flac) ApplyTags(t *Tags) error {
	var errs []error
	title := t.Title
	// adding guests
	if len(t.Guest) != 0 {
		title += " (feat. " + SmartArtistList(t.Guest) + ")"
	}
	// adding remixers only if the title does not mention either "Remix" or the remixers themselves
	if len(t.Remixer) != 0 {
		var mentionsRemixer bool
		for _, r := range t.Remixer {
			// TODO add checks about *where* it is contained?
			if strings.Contains(strings.ToLower(title), strings.ToLower(r)) {
				mentionsRemixer = true
			}
		}
		if !mentionsRemixer && !strings.Contains(strings.ToLower(title), "remix") {
			title += " (" + SmartArtistList(t.Remixer) + " Remix)"
		}
	}
	errs = append(errs, f.SetTitle(title, t.Version))
	errs = append(errs, f.SetAlbum(t.Album))
	errs = append(errs, f.SetGenre(t.Genre))
	errs = append(errs, f.SetArtists(t.Artist, false))
	errs = append(errs, f.SetAlbumArtists(t.AlbumArtist, false))
	errs = append(errs, f.SetPerformers(t.Performer, false))
	errs = append(errs, f.SetComposers(t.Composer, false))
	errs = append(errs, f.SetConductors(t.Conductor, false))
	errs = append(errs, f.SetAuthors(t.Author, false))
	errs = append(errs, f.SetRemixers(t.Remixer, false))
	errs = append(errs, f.SetTrackNumber(t.TrackNumber, t.TotalTracks))
	errs = append(errs, f.SetDiscNumber(t.DiscNumber, t.TotalDiscs))
	errs = append(errs, f.SetRecordLabel(t.Label))
	errs = append(errs, f.SetUPC(t.UPC))
	errs = append(errs, f.SetISRC(t.ISRC))
	errs = append(errs, f.SetMedia(t.Media))
	errs = append(errs, f.SetReleaseDate(t.Date))
	errs = append(errs, f.SetDescription(t.Description))
	errs = append(errs, f.SetDuration(t.Duration))

	for _, e := range errs {
		if e != nil {
			return e
		}
	}
	return nil
}

func (f *Flac) OverwriteTags(t *Tags) error {
	f.ClearMetadata()
	return f.ApplyTags(t)
}

func (f *Flac) getNumberEvenIfCombinedWithTotal(tagTitlesMap map[string]string, tag string) string {
	re := regexp.MustCompile(regexpCombinedTrackNumber)
	// using that map to get the relevant information
	if len(f.metadata.Tags[tagTitlesMap[tag]]) != 0 {
		number := f.metadata.Tags[tagTitlesMap[tag]][0]
		// if track number is combined with track total: 01/10
		if re.MatchString(number) {
			hits := re.FindAllStringSubmatch(number, -1)
			if len(hits) != 0 && len(hits[0]) > 1 {
				return hits[0][1]
			}
		} else {
			return number
		}
	}
	return ""
}

func (f *Flac) CommonTags() *Tags {
	// tag titles are case insensitive. To correctly identify existing tags,
	// building a map of CANONICAL:FOUND IN TRACK tag titles
	tagTitlesMap := make(map[string]string)
	for tagTitle := range f.metadata.Tags {
		for _, t := range validTags {
			if strings.EqualFold(tagTitle, t) {
				tagTitlesMap[t] = tagTitle
				break
			}
		}
	}

	// using that map to get the relevant information
	t := &Tags{}
	t.TrackNumber = f.getNumberEvenIfCombinedWithTotal(tagTitlesMap, TagTrackNumber)
	if len(f.metadata.Tags[tagTitlesMap[TagTrackTotal]]) != 0 {
		t.TotalTracks = f.metadata.Tags[tagTitlesMap[TagTrackTotal]][0]
	}
	if len(f.metadata.Tags[tagTitlesMap[TagTrackTotal2]]) != 0 {
		t.TotalTracks = f.metadata.Tags[tagTitlesMap[TagTrackTotal2]][0]
	}
	t.DiscNumber = f.getNumberEvenIfCombinedWithTotal(tagTitlesMap, TagDiscNumber)
	if len(f.metadata.Tags[tagTitlesMap[TagDiscTotal]]) != 0 {
		t.TotalDiscs = f.metadata.Tags[tagTitlesMap[TagDiscTotal]][0]
	}
	if len(f.metadata.Tags[tagTitlesMap[TagDiscTotal2]]) != 0 {
		t.TotalDiscs = f.metadata.Tags[tagTitlesMap[TagDiscTotal2]][0]
	}
	t.Artist = f.metadata.Tags[tagTitlesMap[TagArtist]]
	t.AlbumArtist = f.metadata.Tags[tagTitlesMap[TagAlbumArtist]]
	t.Performer = f.metadata.Tags[tagTitlesMap[TagPerformer]]
	t.Conductor = f.metadata.Tags[tagTitlesMap[TagConductor]]
	t.Author = f.metadata.Tags[tagTitlesMap[TagAuthor]]
	t.Composer = f.metadata.Tags[tagTitlesMap[TagComposer]]
	t.Remixer = f.metadata.Tags[tagTitlesMap[TagRemixer]]
	// Guest       []string

	if len(f.metadata.Tags[tagTitlesMap[TagTitle]]) != 0 {
		t.Title = f.metadata.Tags[tagTitlesMap[TagTitle]][0]
	}
	if len(f.metadata.Tags[tagTitlesMap[TagDescription]]) != 0 {
		t.Description = f.metadata.Tags[tagTitlesMap[TagDescription]][0]
	}
	if len(f.metadata.Tags[tagTitlesMap[TagDate]]) != 0 {
		t.Date = f.metadata.Tags[tagTitlesMap[TagDate]][0]
	}
	if len(f.metadata.Tags[tagTitlesMap[TagYear]]) != 0 {
		t.Year = f.metadata.Tags[tagTitlesMap[TagYear]][0]
	}
	if len(f.metadata.Tags[tagTitlesMap[TagGenre]]) != 0 {
		t.Genre = f.metadata.Tags[tagTitlesMap[TagGenre]][0]
	}
	if len(f.metadata.Tags[tagTitlesMap[TagAlbum]]) != 0 {
		t.Album = f.metadata.Tags[tagTitlesMap[TagAlbum]][0]
	}
	if len(f.metadata.Tags[tagTitlesMap[TagRecordLabel]]) != 0 {
		t.Label = f.metadata.Tags[tagTitlesMap[TagRecordLabel]][0]
	}
	if len(f.metadata.Tags[tagTitlesMap[TagRecordLabel2]]) != 0 {
		t.Label = f.metadata.Tags[tagTitlesMap[TagRecordLabel2]][0]
	}
	if len(f.metadata.Tags[tagTitlesMap[TagVersion]]) != 0 {
		t.Version = f.metadata.Tags[tagTitlesMap[TagVersion]][0]
	}
	if len(f.metadata.Tags[tagTitlesMap[TagISRC]]) != 0 {
		t.ISRC = f.metadata.Tags[tagTitlesMap[TagISRC]][0]
	}
	if len(f.metadata.Tags[tagTitlesMap[TagBarcode]]) != 0 {
		t.UPC = f.metadata.Tags[tagTitlesMap[TagBarcode]][0]
	}
	if len(f.metadata.Tags[tagTitlesMap[TagUPC]]) != 0 {
		t.UPC = f.metadata.Tags[tagTitlesMap[TagUPC]][0]
	}
	if len(f.metadata.Tags[tagTitlesMap[TagMedia]]) != 0 {
		t.Media = f.metadata.Tags[tagTitlesMap[TagMedia]][0]
	}
	return t
}
