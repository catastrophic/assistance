# assistance

[![Documentation](https://godoc.org/gitlab.com/catastrophic/assistance?status.svg)](https://godoc.org/gitlab.com/catastrophic/assistance)
 [![Go Report Card](https://goreportcard.com/badge/gitlab.com/catastrophic/assistance)](https://goreportcard.com/report/gitlab.com/catastrophic/assistance) [![codecov](https://codecov.io/gl/catastrophic/assistance/branch/master/graph/badge.svg)](https://codecov.io/gl/catastrophic/assistance) [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) 
[![Build status](https://gitlab.com/catastrophic/assistance/badges/master/build.svg)](https://gitlab.com/catastrophic/assistance/pipelines)

A number of generally helpful Go functions and packages.
