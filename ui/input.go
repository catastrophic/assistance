// Package ui helps get user input from the terminal, and formats output.
package ui

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/strslice"
)

const (
	questionPrefix = "▷▷ "
	headerPrefix   = "◎ "
	titlePrefix    = "△ "
	usagePrefix    = "  ◦ "
)

var (
	ErrorSkipped = errors.New("skipped choice")
)

// GetInput from user.
func GetInput(in *os.File) (string, error) {
	if in == nil {
		in = os.Stdin
	}
	scanner := bufio.NewReader(in)
	choice, scanErr := scanner.ReadString('\n')
	return strings.TrimSpace(choice), scanErr
}

// Accept asks a question and returns the answer.
func Accept(question string) bool {
	for {
		fmt.Printf(BlueBold(questionPrefix+question) + BlueBold("? [y/N]: "))
		input, err := GetInput(nil)
		if err == nil {
			switch strings.TrimSpace(input) {
			case "y", "Y", "yes":
				return true
			case "n", "N", "no":
				return false

			}
		}
	}
}

func SelectValue(title, usage string, options []string) (string, error) {
	return selectOption(title, usage, options, nil, false, false, true)
}

func SelectOptionalValue(title, usage string, options []string) (string, error) {
	return selectOption(title, usage, options, nil, true, false, true)
}

func SelectLongValue(title, usage string, options []string) (string, error) {
	return selectOption(title, usage, options, nil, false, true, true)
}

func SelectLongOptionalValue(title, usage string, options []string) (string, error) {
	return selectOption(title, usage, options, nil, true, true, true)
}

func SelectAmongValues(title, usage string, options []string, optionTags map[string]string) (string, error) {
	return selectOption(title, usage, options, optionTags, false, false, true)
}

func SelectAmongOptions(title, usage string, options []string, optionTags map[string]string) (string, error) {
	return selectOption(title, usage, options, optionTags, true, false, true)
}

func SelectAmongStrictOptions(title, usage string, options []string, optionTags map[string]string) (string, error) {
	return selectOption(title, usage, options, optionTags, false, false, false)
}

// selectOption among several, or input a new one, and return user input.
func selectOption(title, usage string, options []string, optionTags map[string]string, canBeBlank, longValue, canBeEdited bool) (string, error) {
	Title(strings.TrimSpace(title))
	if usage != "" {
		Usage(usage)
	}

	// remove duplicates from options and display them
	strslice.RemoveDuplicates(&options)
	for i, o := range options {
		tag, ok := optionTags[o]
		if ok {
			tag = "[" + tag + "] "
		}
		fmt.Printf("  %d. %s\n", i+1, tag+o)
	}

	// available user actions
	var choiceActions string
	var editText string
	if canBeEdited {
		editText = " or [E]dit"
	} else {
		editText = " or [S]kip"
	}
	if canBeBlank {
		switch len(options) {
		case 0:
			choiceActions += "Leave [B]lank" + editText
		case 1:
			choiceActions += "Option [1], Leave [B]lank" + editText
		default:
			choiceActions += fmt.Sprintf("Option [1-%d], leave [B]lank"+editText, len(options))
		}
	} else {
		switch len(options) {
		case 0:
			return "", errors.New("no options for required field")
		case 1:
			choiceActions += "Option [1]" + editText
		default:
			choiceActions += fmt.Sprintf("Option [1-%d]"+editText, len(options))
		}
	}

	errs := 0
	for {
		UserChoice(choiceActions)
		choice, scanErr := GetInput(nil)
		if scanErr != nil {
			return "", scanErr
		}
		choice = strings.ToUpper(choice)

		switch {
		case choice == "E" && canBeEdited:
			var edited string
			var scanErr error

			if longValue {
				allVersions := ""
				for i, o := range options {
					allVersions += fmt.Sprintf("--- %d ---\n%s\n", i+1, o)
				}
				edited, scanErr = Edit(allVersions)
			} else {
				UserChoice("Enter the new value")
				edited, scanErr = GetInput(nil)
			}

			if scanErr != nil {
				return "", scanErr
			}
			if edited == "" {
				RedBold("Empty value!")
			} else {
				for strings.HasPrefix(edited, "/") {
					edited = edited[1:]
				}
				if Accept("Confirm: " + YellowBold(edited)) {
					return edited, nil
				}
				RedBold("Not confirmed.")
			}
		case choice == "A" && len(options) == 1:
			return options[0], nil
		case choice == "B" && canBeBlank:
			return "", nil
		case choice == "S" && !canBeEdited:
			return "", ErrorSkipped
		default:
			if index, err := strconv.Atoi(choice); err == nil && 0 < index && index <= len(options) {
				return options[index-1], nil
			}
		}

		// if we get here, wrong choice
		RedBold("Invalid choice.")
		errs++
		if errs > 10 {
			RedBold("Too many errors")
			return "", errors.New("Invalid choice")
		}
	}
}

// Edit long value using external $EDITOR.
func Edit(oldValue string) (string, error) {
	// create temp file
	content := []byte(oldValue)
	tempfile, err := ioutil.TempFile("", "edit")
	if err != nil {
		return oldValue, err
	}
	// clean up
	defer tempfile.Close()
	defer os.Remove(tempfile.Name())

	// write input string inside
	if _, err := tempfile.Write(content); err != nil {
		return oldValue, err
	}
	if err := tempfile.Close(); err != nil {
		return oldValue, err
	}

	// find $EDITOR
	editor := os.Getenv("EDITOR")
	if editor == "" {
		RedBold("$EDITOR not set, falling back to nano")
		editor = "nano"
	}

	// open it with $EDITOR
	cmd := exec.Command(editor, tempfile.Name())
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	if err := cmd.Run(); err != nil {
		return oldValue, err
	}

	// read file back, set output string
	newContent, err := ioutil.ReadFile(tempfile.Name())
	if err != nil {
		return oldValue, err
	}
	return strings.TrimSpace(string(newContent)), nil
}
