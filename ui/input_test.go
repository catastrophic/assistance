package ui

import (
	"fmt"
	"io"
	"io/ioutil"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInput(t *testing.T) {
	fmt.Println("+ Testing Input...")
	check := assert.New(t)

	in, err := ioutil.TempFile("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer in.Close()

	_, err = io.WriteString(in, "4\n")
	if err != nil {
		t.Fatal(err)
	}
	_, err = in.Seek(0, io.SeekStart)
	if err != nil {
		t.Fatal(err)
	}
	s, err := GetInput(in)
	check.Nil(err)
	check.Equal("4", s)

	// testing windows
	_, err = io.WriteString(in, "4\r\n")
	if err != nil {
		t.Fatal(err)
	}
	_, err = in.Seek(0, io.SeekStart)
	if err != nil {
		t.Fatal(err)
	}
	s, err = GetInput(in)
	check.Nil(err)
	check.Equal("4", s)

	m := make(map[string]string)
	m["option1"] = "http://some.thing/1"

	choice, _ := SelectAmongOptions("title", "usage string", []string{"option1", "option2", "option3"}, m)
	fmt.Println(choice)
	choice, _ = SelectAmongStrictOptions("title", "usage string", []string{"option1", "option2", "option3"}, m)
	fmt.Println(choice)
}
