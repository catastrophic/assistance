package ui

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestOutput(t *testing.T) {
	fmt.Println("\n --- Testing ui/Output. ---")
	check := assert.New(t)

	red := Red("TEST")
	check.Equal("\x1b[91mTEST\x1b[0m", red)
	check.Equal("TEST", RemoveColor(red))

	check.Equal("\x1b[91mTEST\x1b[0m", Red("TEST"))
	check.Equal("\x1b[1;94mTEST\x1b[0m", BlueBold("TEST"))
	check.Equal("\x1b[94mTEST\x1b[0m", Blue("TEST"))
	check.Equal("\x1b[1;92mTEST\x1b[0m", GreenBold("TEST"))
	check.Equal("\x1b[1;91mTEST\x1b[0m", RedBold("TEST"))
	check.Equal("\x1b[93mTEST\x1b[0m", Yellow("TEST"))
	check.Equal("\x1b[4;93mTEST\x1b[0m", YellowUnderlined("TEST"))
	check.Equal("\x1b[4;93mTEST\x1b[0m", Warning("TEST"))

	row1 := []string{"1", "2"}
	row2 := []string{"3", "SOMETHING"}
	rows := [][]string{}
	rows = append(rows, row1)
	rows = append(rows, row2)
	fmt.Println(TabulateRows(rows, "first", "second"))
}
