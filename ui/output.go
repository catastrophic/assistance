package ui

import (
	"fmt"
	"regexp"
	"runtime"
	"sort"
	"strconv"
	"time"

	"github.com/mgutz/ansi"
	spin "github.com/tj/go-spin"
	"gitlab.com/catastrophic/gotabulate"
)

const (
	colorCodesRegexp = "[\u001B\u009B][[\\]()#;?]*(?:(?:(?:[a-zA-Z\\d]*(?:;[a-zA-Z\\d]*)*)?\u0007)|(?:(?:\\d{1,4}" +
		"(?:;\\d{0,4})*)?[\\dA-PRZcf-ntqry=><~]))"
)

var (
	colorRemover = regexp.MustCompile(colorCodesRegexp)
)

//SpinWhileThingsHappen is a way to launch a function and display a spinner while it is being executed.
func SpinWhileThingsHappen(title string, f func() error) error {
	c1 := make(chan bool)
	c2 := make(chan error)

	// first routine for the spinner
	ticker := time.NewTicker(time.Millisecond * 100)
	go func() {
		for range ticker.C {
			c1 <- true
		}
	}()
	// second routine deals with the function
	go func() {
		// run function
		c2 <- f()
	}()

	// await both of these values simultaneously,
	// dealing with each one as it arrives.
	functionDone := false
	s := spin.New()
	for !functionDone {
		select {
		case <-c1:
			fmt.Printf("\r%s... %s ", title, s.Next())
		case err := <-c2:
			if err != nil {
				fmt.Printf("\r%s... KO.\n", title)
				return err
			}
			fmt.Printf("\r%s... Done.\n", title)
			functionDone = true
		}
	}
	return nil
}

func colorIfLinux(format, in string) string {
	if runtime.GOOS == "windows" {
		return in
	}
	return ansi.ColorFunc(format)(in)
}

// BlueBold outputs a string in blue bold.
func BlueBold(in string) string {
	return colorIfLinux("blue+hb", in)
}

// Blue outputs a string in blue.
func Blue(in string) string {
	return colorIfLinux("blue+h", in)
}

// Blue outputs a string in blue.
func BlueDark(in string) string {
	return colorIfLinux("blue", in)
}

// BlueUnderlined outputs a string in blue, underlined.
func BlueUnderlined(in string) string {
	return colorIfLinux("blue+hu", in)
}

// BlueBold outputs a string in blue bold.
func BlueBoldUnderlined(in string) string {
	return colorIfLinux("blue+hbu", in)
}

// GreenBold outputs a string in green bold.
func GreenBold(in string) string {
	return colorIfLinux("green+hb", in)
}

// Green outputs a string in green.
func Green(in string) string {
	return colorIfLinux("green+h", in)
}

// GreenUnderlined outputs a string in green, underlined.
func GreenUnderlined(in string) string {
	return colorIfLinux("green+hu", in)
}

// RedBold outputs a string in red bold.
func RedBold(in string) string {
	return colorIfLinux("red+hb", in)
}

// Red outputs a string in red.
func Red(in string) string {
	return colorIfLinux("red+h", in)
}

// RedUnderlined outputs a string in red, underlined.
func RedUnderlined(in string) string {
	return colorIfLinux("red+hu", in)
}

// Yellow outputs a string in yellow.
func Yellow(in string) string {
	return colorIfLinux("yellow+h", in)
}

// YellowBold outputs a string in bold yellow.
func YellowBold(in string) string {
	return colorIfLinux("yellow+hb", in)
}

// YellowUnderlined outputs a string in yellow, underlined.
func YellowUnderlined(in string) string {
	return colorIfLinux("yellow+hu", in)
}

func RemoveColor(str string) string {
	return colorRemover.ReplaceAllString(str, "")
}

// UserChoice message logging.
func UserChoice(msg string, args ...interface{}) {
	msg = fmt.Sprintf(questionPrefix+msg+": ", args...)
	fmt.Print(BlueBold(msg))
}

// Header message logging.
func Header(msg string, args ...interface{}) {
	msg = fmt.Sprintf(msg, args...)
	fmt.Print("\n" + YellowBold(headerPrefix) + YellowUnderlined(msg) + YellowBold(":\n"))
}

// Title message logging.
func Title(msg string, args ...interface{}) {
	msg = fmt.Sprintf(msg, args...)
	fmt.Print("\n" + YellowBold(titlePrefix) + YellowUnderlined(msg) + YellowBold(":\n"))
}

// Usage message logging.
func Usage(msg string, args ...interface{}) {
	msg = fmt.Sprintf(msg, args...)
	fmt.Print(Green(usagePrefix) + Green(msg) + Green("\n"))
}

// Warning output.
func Warning(in string) string {
	return YellowUnderlined(in)
}

// TabulateRows of map[string]int.
func TabulateRows(rows [][]string, headers ...string) (table string) {
	if len(rows) == 0 {
		return
	}
	t := gotabulate.Create(rows)
	t.SetHeaders(headers)
	t.SetEmptyString("N/A")
	t.SetAlign("left")
	t.SetAutoSize(true)
	t.SetWrapDelimiter(' ')
	return t.Render("border")
}

// TabulateMap of map[string]int.
func TabulateMap(input map[string]int, firstHeader string, secondHeader string) (table string) {
	if len(input) == 0 {
		return
	}
	// building first column list for sorting
	keys := make([]string, len(input))
	for key := range input {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	// building table
	rows := make([][]string, len(keys))
	for _, key := range keys {
		rows = append(rows, []string{key, strconv.Itoa(input[key])})
	}
	return TabulateRows(rows, firstHeader, secondHeader)
}
