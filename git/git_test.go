package git

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"gitlab.com/catastrophic/assistance/fs"

	"github.com/stretchr/testify/assert"
)

func TestGit(t *testing.T) {
	fmt.Println("\n --- Testing Git. ---")
	check := assert.New(t)

	testDir := "/tmp/testingGit"
	dummyFile := filepath.Join(testDir, "file.txt")

	// create struct before creating the folder
	git, err := New(testDir, "user", "user@mail.com")
	check.Nil(err)
	check.NotNil(git)
	// test if in git dir, expect false
	check.False(git.Exists())

	// create git repo
	check.Nil(git.Init())
	check.True(fs.DirExists(filepath.Join(testDir, ".git")))

	// create dummy file
	if err := ioutil.WriteFile(dummyFile, []byte("Nothing interesting."), 0777); err != nil {
		panic(err)
	}
	// remove everything once the test is over
	defer func() {
		check.Nil(os.RemoveAll(testDir))
	}()

	out, err := git.runCommand("config", "user.name")
	check.Nil(err)
	check.Equal("user", out)
	out, err = git.runCommand("config", "user.email")
	check.Nil(err)
	check.Equal("user@mail.com", out)

	// test if in git dir, expect true
	check.True(git.Exists())

	// commit before adding, expect err
	check.NotNil(git.Commit("commit"))

	// add dummy file, expect nil
	err = git.Add(dummyFile)
	check.Nil(err)
	// add fake dummy file, expect err
	err = git.Add(dummyFile + "_")
	check.NotNil(err)

	// commit, expect nil
	check.Nil(git.Commit("commit"))

	// lookup origin remote, expect false
	hasRemote := git.HasRemote("origin")
	check.False(hasRemote)

	// add remote origin, expect nil
	err = git.AddRemote("origin", "https://example/repository.git")
	check.Nil(err)
	out, err = git.runCommand("config", "credential.helper")
	check.Nil(err)
	check.Equal("store --file=.git-credentials", out)
	check.Equal(filepath.Join(git.path, gitCredentials), git.credentialsFile())

	_, err = git.runCommand("config", "credential.helper", "store --file="+filepath.Join(git.path, ".git-credentials!"))
	check.Nil(err)
	check.Equal(filepath.Join(git.path, ".git-credentials!"), git.credentialsFile())

	// lookup origin remote, expect true
	hasRemote = git.HasRemote("origin")
	check.True(hasRemote)

	// check compress
	check.Nil(git.Compress())

	// credentials
	check.Nil(git.writeCredentials("https://example/repository.git", "user!", "password!"))
	check.True(fs.FileExists(filepath.Join(git.path, ".git-credentials!")))
	data, err := ioutil.ReadFile(filepath.Join(git.path, ".git-credentials!"))
	check.Nil(err)
	check.Equal("https://user%21:password%21@example/repository.git", string(data))
	check.Nil(os.Remove(filepath.Join(git.path, ".git-credentials!")))

	// how to test successful push?
	err = git.Push("NOPE", "https://example/repository.git", "user", "password")
	check.NotNil(err)
	check.Equal(errorGitUnknownRemote+"NOPE", err.Error())
	check.False(fs.FileExists(filepath.Join(git.path, ".git-credentials!")))

	err = git.Push("origin", "https://example/repository.git", "user", "password")
	check.NotNil(err)
	check.True(strings.HasPrefix(err.Error(), "error git pushing"))
	check.False(fs.FileExists(filepath.Join(git.path, ".git-credentials!")))
}
