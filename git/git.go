// Package git provides a very basic wrapper around git.
// Using git2go would be better but requires libgit2 to be installed
// This assumes git, however, is available.
package git

import (
	"fmt"
	"io/ioutil"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/fs"
	"gitlab.com/catastrophic/assistance/strslice"
)

const (
	gitCmd         = "git"
	gitCredentials = ".git-credentials"

	errorGitNotAvailable       = "git is not available on this system"
	errorGitRepositoryNotFound = "git repository path does not exist"
	errorGitUnknownRemote      = "unknown git remote "
)

var (
	gitExistsArgs    = []string{"rev-parse", "--is-inside-work-tree"}
	gitInitArgs      = []string{"init", "--quiet"}
	gitSetUser       = []string{"config", "user.name"}
	gitSetMail       = []string{"config", "user.email"}
	gitAdd           = []string{"add"}
	gitCommit        = []string{"commit", "-m"}
	gitPush          = []string{"push"}
	gitRemote        = []string{"remote"}
	gitRemoteAdd     = []string{"remote", "add"}
	gitSetCredential = []string{"config", "credential.helper", "store --file=" + gitCredentials}
	gitGc            = []string{"gc", "--aggressive", "--prune=all"}
	gitRepack        = []string{"repack", "-Ad"}
)

// Git holds the necessary information to make commits.
type Git struct {
	user  string
	email string
	path  string
}

// New Git struct.
func New(root, user, email string) (*Git, error) {
	// checking git is available
	_, err := exec.LookPath(gitCmd)
	if err != nil {
		return nil, errors.Wrap(err, errorGitNotAvailable)
	}
	return &Git{path: root, user: user, email: email}, nil
}

func (g *Git) runCommand(args ...string) (string, error) {
	cmd := exec.Command(gitCmd, args...)
	cmd.Dir = g.path
	cmdOut, err := cmd.CombinedOutput()
	return strings.TrimSpace(string(cmdOut)), err
}

// Exists checks if this is a true git repository.
func (g *Git) Exists() bool {
	cmdOut, err := g.runCommand(gitExistsArgs...)
	if err != nil {
		return false
	}
	if cmdOut == "true" {
		return true
	}
	return false
}

// Init a new git repository.
func (g *Git) Init() error {
	// check root is dir and exists
	if !fs.DirExists(g.path) {
		if err := os.MkdirAll(g.path, 0777); err != nil {
			return errors.Wrap(err, errorGitRepositoryNotFound)
		}
	}

	if _, err := g.runCommand(gitInitArgs...); err != nil {
		return err
	}
	args := append(gitSetUser, g.user)
	if _, err := g.runCommand(args...); err != nil {
		return err
	}
	args2 := append(gitSetMail, g.email)
	_, err := g.runCommand(args2...)
	return err
}

// Add files to commit.
func (g *Git) Add(files ...string) error {
	args := append(gitAdd, files...)
	_, err := g.runCommand(args...)
	return err
}

// Commit previously added files.
func (g *Git) Commit(message string) error {
	args := append(gitCommit, message)
	cmdOut, err := g.runCommand(args...)
	if err != nil {
		return errors.Wrap(err, "error git committing ["+cmdOut+"]")
	}
	return err
}

// HasRemote checks if the repository has a specific remote.
func (g *Git) HasRemote(remote string) bool {
	cmdOut, err := g.runCommand(gitRemote...)
	if err != nil {
		return false
	}
	return strslice.Contains(strings.Split(cmdOut, "\n"), remote)
}

// AddRemote to the git repository.
func (g *Git) AddRemote(remoteName, remoteURL string) error {
	args := append(gitRemoteAdd, remoteName, remoteURL)
	_, err := g.runCommand(args...)
	if err == nil {
		// activate credential storing
		if _, err := g.runCommand(gitSetCredential...); err != nil {
			return err
		}
	}
	return err
}

func (g *Git) credentialsFile() string {
	out, err := g.runCommand("config", "credential.helper")
	if err != nil || out == "store" {
		return filepath.Join(g.path, gitCredentials)
	}
	finder := regexp.MustCompile("store --file=(.*)")
	files := finder.FindAllStringSubmatch(out, -1)
	if len(files) != 1 {
		return filepath.Join(g.path, gitCredentials)
	}
	if filepath.IsAbs(files[0][1]) {
		return files[0][1]
	}
	return filepath.Join(g.path, files[0][1])
}

func (g *Git) writeCredentials(remoteURL, remoteUser, remotePassword string) error {
	shortRemoteURL := strings.Replace(remoteURL, "https://", "", -1)
	fullURL := fmt.Sprintf("https://%s:%s@%s", url.PathEscape(remoteUser), url.PathEscape(remotePassword), shortRemoteURL)
	return ioutil.WriteFile(g.credentialsFile(), []byte(fullURL), 0700)
}

// Push to a registered git remote.
func (g *Git) Push(remoteName, remoteURL, remoteUser, remotePassword string) error {
	if !g.HasRemote(remoteName) {
		return errors.New(errorGitUnknownRemote + remoteName)
	}

	// write its contents: https://user:pass@example.com
	// not ideal because the file will be readable until the push is over.
	if err := g.writeCredentials(remoteURL, remoteUser, remotePassword); err != nil {
		return err
	}
	defer os.Remove(g.credentialsFile())

	args := append(gitPush, remoteName, "master")
	cmdOut, err := g.runCommand(args...)
	if err != nil {
		return errors.Wrap(err, "error git pushing ["+cmdOut+"]")
	}
	return err
}

// Compress the git repository.
func (g *Git) Compress() error {
	// garbage collection
	if _, err := g.runCommand(gitGc...); err != nil {
		return err
	}
	// repack
	_, err := g.runCommand(gitRepack...)
	return err
}
