package privatebin

// forked from https://github.com/matthewpi/privatebin

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/sha256"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/btcsuite/btcutil/base58"
	"golang.org/x/crypto/pbkdf2"
)

const (
	specIterations       = 100000
	specKeySize          = 256
	specTagSize          = 128
	specAlgorithm        = "aes"
	specMode             = "gcm"
	specCompression      = "none"
	defaultPasteDuration = "1week"
)

// PasteRequest .
type PasteRequest struct {
	V     int              `json:"v"`
	AData []interface{}    `json:"adata"`
	Meta  PasteRequestMeta `json:"meta"`
	CT    string           `json:"ct"`
}

// PasteRequestMeta .
type PasteRequestMeta struct {
	Expire string `json:"expire"`
}

// PasteResponse .
type PasteResponse struct {
	Status      int    `json:"status"`
	ID          string `json:"id"`
	URL         string `json:"url"`
	DeleteToken string `json:"deletetoken"`
}

// PasteContent .
type PasteContent struct {
	Paste string `json:"paste"`
}

// PasteSpec .
type PasteSpec struct {
	IV          string
	Salt        string
	Iterations  int
	KeySize     int
	TagSize     int
	Algorithm   string
	Mode        string
	Compression string
}

// SpecArray .
func (spec *PasteSpec) SpecArray() []interface{} {
	return []interface{}{
		spec.IV,
		spec.Salt,
		spec.Iterations,
		spec.KeySize,
		spec.TagSize,
		spec.Algorithm,
		spec.Mode,
		spec.Compression,
	}
}

// PasteData .
type PasteData struct {
	*PasteSpec
	Data []byte
}

// adata .
func (paste *PasteData) adata() []interface{} {
	return []interface{}{
		paste.SpecArray(),
		"plaintext",
		0,
		0,
	}
}

func Post(input []byte, host string) (string, error) {
	if !isValidUrl(host) {
		return "", errors.New("invalid URL: " + host)
	}

	// removing extra line breaks to prevent PrivateBin from breaking.
	if bytes.HasSuffix(input, []byte("\n")) {
		input = input[:len(input)-1]
	}

	// marshalling the paste content to escape JSON characters.
	pasteContent, err := json.Marshal(&PasteContent{Paste: StripANSI(string(input))})
	if err != nil {
		return "", err
	}
	// generating a master key
	masterKey, err := GenRandomBytes(32)
	if err != nil {
		return "", err
	}
	// encrypting the unput
	pasteData, err := encrypt(masterKey, pasteContent)
	if err != nil {
		return "", err
	}

	// creating a new Paste Request and marshalling as JSON
	pasteRequest := &PasteRequest{
		V:     2,
		AData: pasteData.adata(),
		Meta: PasteRequestMeta{
			Expire: defaultPasteDuration,
		},
		CT: Base64(pasteData.Data),
	}
	body, err := json.Marshal(pasteRequest)
	if err != nil {
		return "", err
	}

	// making the request to send the input
	client := &http.Client{}
	req, err := http.NewRequest("POST", host, bytes.NewBuffer(body))
	if err != nil {
		return "", err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Content-Length", strconv.Itoa(len(body)))
	req.Header.Set("X-Requested-With", "JSONHttpRequest")
	res, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()

	// parsing the response
	response, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}
	var pasteResponse *PasteResponse
	err = json.Unmarshal(response, &pasteResponse)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%s%s#%s", host, pasteResponse.URL, base58.Encode(masterKey)), nil
}

func encrypt(master []byte, message []byte) (*PasteData, error) {
	// Generate a initialization vector.
	iv, err := GenRandomBytes(12)
	if err != nil {
		return nil, err
	}
	// Generate salt.
	salt, err := GenRandomBytes(8)
	if err != nil {
		return nil, err
	}

	// Create the Paste Data and generate a key.
	paste := &PasteData{
		PasteSpec: &PasteSpec{
			IV:          Base64(iv),
			Salt:        Base64(salt),
			Iterations:  specIterations,
			KeySize:     specKeySize,
			TagSize:     specTagSize,
			Algorithm:   specAlgorithm,
			Mode:        specMode,
			Compression: specCompression,
		},
	}
	key := pbkdf2.Key(master, salt, paste.Iterations, 32, sha256.New)

	// Get the "adata" for the paste.
	adata, err := json.Marshal(paste.adata())
	if err != nil {
		return nil, err
	}

	// Create a new Cipher
	c, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	// Create a new GCM.
	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return nil, err
	}

	// Sign the message.
	data := gcm.Seal(nil, iv, message, adata)

	// Update and return the paste data.
	paste.Data = data

	return paste, nil
}
