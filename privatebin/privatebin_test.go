package privatebin

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPrivateBin(t *testing.T) {
	fmt.Println("+ Testing PrivateBin...")
	check := assert.New(t)

	privateBinURL := "https://privatebin.net"

	url, err := Post([]byte("hello there!"), privateBinURL)
	check.Nil(err)
	fmt.Println(url)
}
