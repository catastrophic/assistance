package privatebin

import (
	"crypto/rand"
	"encoding/base64"
	"net/url"
	"regexp"
)

const ansi = "[\u001B\u009B][[\\]()#;?]*(?:(?:(?:[a-zA-Z\\d]*(?:;[a-zA-Z\\d]*)*)?\u0007)|(?:(?:\\d{1,4}(?:;\\d{0,4})*)?[\\dA-PRZcf-ntqry=><~]))"

var regex = regexp.MustCompile(ansi)

// StripANSI strips ansi out of a string.
func StripANSI(str string) string {
	return regex.ReplaceAllString(str, "")
}

// Base64 encodes a byte slice to a base64 string.
func Base64(src []byte) string {
	return base64.RawStdEncoding.EncodeToString(src)
}

// GenRandomBytes generates crypto-secure random bytes.
func GenRandomBytes(n uint32) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}

	return b, nil
}

// isValidUrl tests a string to determine if it is a well-structured url or not.
func isValidUrl(toTest string) bool {
	_, err := url.ParseRequestURI(toTest)
	if err != nil {
		return false
	}

	u, err := url.Parse(toTest)
	if err != nil || u.Scheme == "" || u.Host == "" {
		return false
	}
	return true
}
