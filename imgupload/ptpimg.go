package imgupload

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"golang.org/x/net/publicsuffix"
)

const (
	ptpImgURL          = "https://ptpimg.me"
	ErrorCouldNotLogIn = "could not log in"
)

type PTPIMGUploader struct {
	client *http.Client
	apiKey string
}

type PTPIMGResponse struct {
	Code string `json:"code"`
	Ext  string `json:"ext"`
}

func NewWithLogin(user, password string) (*PTPIMGUploader, error) {
	// create with blank api key
	p, err := NewWithAPIKey("")
	if err != nil {
		return nil, err
	}
	// logging in to get the api key
	if err := p.Login(user, password); err != nil {
		return nil, err
	}
	return p, nil
}

func NewWithAPIKey(key string) (*PTPIMGUploader, error) {
	options := cookiejar.Options{
		PublicSuffixList: publicsuffix.List,
	}
	jar, err := cookiejar.New(&options)
	if err != nil {
		return nil, err
	}
	client := &http.Client{Jar: jar}
	return &PTPIMGUploader{apiKey: key, client: client}, nil
}

func (p *PTPIMGUploader) Login(username, password string) error {
	form := url.Values{}
	form.Add("email", username)
	form.Add("pass", password)
	req, err := http.NewRequest("POST", ptpImgURL+"/login.php?1", strings.NewReader(form.Encode()))
	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err := p.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return errors.New("Returned status: " + resp.Status)
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	// extract api_key
	r := regexp.MustCompile(`name='api_key' value='(.*)'`)
	if r.MatchString(string(data)) {
		p.apiKey = r.FindStringSubmatch(string(data))[1]
		return nil
	}
	return errors.New("could not find api_key: " + ErrorCouldNotLogIn)
}

func (p *PTPIMGUploader) UploadLocalFileWithContext(ctx context.Context, image string) (string, error) {
	links, err := p.upload(ctx, []string{image}, true)
	if err != nil || len(links) == 0 {
		return "", err
	}
	return links[0], err
}

func (p *PTPIMGUploader) UploadLocalFile(image string) (string, error) {
	links, err := p.upload(context.Background(), []string{image}, true)
	if err != nil || len(links) == 0 {
		return "", err
	}
	return links[0], err
}

func (p *PTPIMGUploader) UploadLocalFiles(images []string) ([]string, error) {
	return p.upload(context.Background(), images, true)
}

func (p *PTPIMGUploader) UploadURL(url string) (string, error) {
	links, err := p.upload(context.Background(), []string{url}, false)
	if err != nil || len(links) == 0 {
		return "", err
	}
	return links[0], err
}

func (p *PTPIMGUploader) UploadURLWithContext(ctx context.Context, url string) (string, error) {
	links, err := p.upload(ctx, []string{url}, false)
	if err != nil || len(links) == 0 {
		return "", err
	}
	return links[0], err
}

func (p *PTPIMGUploader) UploadURLs(urls []string) ([]string, error) {
	return p.upload(context.Background(), urls, false)
}

func (p *PTPIMGUploader) upload(ctx context.Context, targets []string, isLocal bool) ([]string, error) {
	if p.client == nil || p.apiKey == "" {
		return []string{}, errors.New("not logged in")
	}
	// preparing a form
	b := new(bytes.Buffer)
	w := multipart.NewWriter(b)

	if isLocal {
		for _, t := range targets {
			// adding image to form
			f, err := os.Open(t)
			if err != nil {
				return []string{}, err
			}
			fw, err := w.CreateFormFile("file-upload[]", filepath.Base(t))
			if err != nil {
				return []string{}, err
			}
			if _, err = io.Copy(fw, f); err != nil {
				return []string{}, err
			}
			if err = f.Close(); err != nil {
				return []string{}, err
			}
		}
		// else adding url to form
	} else if err := w.WriteField("link-upload", strings.Join(targets, "\n")); err != nil {
		return []string{}, err
	}
	if err := w.WriteField("api_key", p.apiKey); err != nil {
		return []string{}, err
	}
	w.Close()

	req, err := http.NewRequestWithContext(ctx, "POST", ptpImgURL+"/upload.php", b)
	if err != nil {
		return []string{}, err
	}
	req.Header.Set("Content-Type", w.FormDataContentType())

	resp, err := p.client.Do(req)
	if err != nil {
		return []string{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return []string{}, errors.New("Returned status: " + resp.Status)
	}

	// reading response to get the links
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return []string{}, err
	}
	var ptpimgJSON []PTPIMGResponse
	if err := json.Unmarshal(data, &ptpimgJSON); err != nil {
		return []string{}, err
	}
	var links []string
	for _, l := range ptpimgJSON {
		links = append(links, fmt.Sprintf("%s/%s.%s", ptpImgURL, l.Code, l.Ext))
	}
	return links, nil
}
