package music

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/catastrophic/assistance/flac"
	"gitlab.com/catastrophic/assistance/fs"
)

func TestRelease(t *testing.T) {
	fmt.Println("+ Testing release...")
	check := assert.New(t)

	_, err := exec.LookPath("metaflac")
	if err != nil {
		fmt.Println("metaflac required, not running those tests.")
		return
	}
	// saving to different location, to prevent other tests using that flac from failing.
	check.Nil(os.MkdirAll("../testing_release é!/", 0777))
	check.Nil(fs.CopyFile("../testing/sample.flac", "../testing_release é!/sample.flac", false))
	check.Nil(fs.CopyFile("../testing/sample_without_tags.flac", "../testing_release é!/sample_without_tags.flac", false))
	defer os.RemoveAll("../testing_release é!")

	r := New("../testing_release é!")
	check.Equal(ErrorNoCover, r.Analyze())
	check.Equal(2, r.NumberOfDiscs())
	check.Equal(2, len(r.Flacs))
	check.Nil(r.Recompress())

	rawTags := r.GetRawTags()
	check.NotEmpty(rawTags)
	fmt.Println(rawTags)

	_, err = exec.LookPath("sox")
	if err == nil {
		links, err := r.GenerateSpectrograms("testing this thing", false)
		check.Nil(err)
		check.Len(links, 2)
	}

	// checking tags
	check.Nil(r.CheckEncoding())
	errs := r.CheckMinimalTags()
	check.NotEmpty(errs)
	check.Equal(1, len(errs))
	check.Equal("../testing_release é!/sample_without_tags.flac: missing minimal tag(s) ARTIST, TITLE, ALBUM, TRACKNUMBER", errs[0].Error())
	check.True(errors.Is(errs[0], flac.ErrorMinimalTags))

	// removing the offending file
	check.Nil(os.Remove("../testing_release é!/sample_without_tags.flac"))
	// checking again
	check.Equal(ErrorNoCover, r.Analyze())
	check.Equal(1, len(r.Flacs))
	check.Nil(r.CheckMinimalTags())
	check.Empty(r.CheckMinimalTags())
	check.Nil(r.Check())
	// checking with incorrect file with ID3 tags
	check.Nil(fs.CopyFile("../testing/sample_with_id3_tags.flac", "../testing_release é!/sample_with_id3_tags.flac", false))
	check.NotNil(r.Analyze())
	check.Equal(1, len(r.Flacs))
	check.Nil(r.Check())

	// checking transcoding
	check.Nil(r.transcode(true, "../testing_release320"))
	check.Nil(r.transcode(false, "../testing_releaseV0"))
	check.NotNil(r.transcode(false, "../testing_releaseV0"))
	defer os.RemoveAll("../testing_release320")
	defer os.RemoveAll("../testing_releaseV0")

	// checking embedding artwork
	track, err := flac.New(filepath.Join(r.Path, "sample.flac"))
	check.Nil(err)
	check.False(track.HasCover)
	check.Nil(r.EmbedArtwork("../testing/image.jpg", 150))
	track, err = flac.New(filepath.Join(r.Path, "sample.flac"))
	check.Nil(err)
	check.True(track.HasCover)
	check.True(track.CoverSize < 150*1024)
	track.ClearMetadata()
	check.NotNil(track.HasMinimalTags())

	num, err := getNumberFromVinylTrackNumbers("A")
	check.Nil(err)
	check.Equal(1, num)

	num, err = getNumberFromVinylTrackNumbers("B1")
	check.NotNil(err)

	num, err = getNumberFromVinylTrackNumbers("C")
	check.Nil(err)
	check.Equal(3, num)
}
