package music

import (
	"errors"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/bogem/id3v2"
	"gitlab.com/catastrophic/assistance/flac"
	"gitlab.com/catastrophic/assistance/fs"
)

// TranscodeTo320 a given Release's FLAC files.
func (r *Release) TranscodeTo320(cbr320FolderName string) error {
	return r.transcode(true, cbr320FolderName)
}

// TranscodeToV0 a given Release's FLAC files.
func (r *Release) TranscodeToV0(v0FolderName string) error {
	return r.transcode(false, v0FolderName)
}

// transcode all FLACs.
func (r *Release) transcode(cbr320 bool, folderName string) error {
	// create dest directory
	if fs.DirExists(folderName) {
		return errors.New(folderName + "already exists, stopping")
	}
	if err := os.MkdirAll(folderName, 0755); err != nil {
		return err
	}
	// copy the cover
	if fs.FileExists(filepath.Join(r.Path, r.Cover)) {
		if err := fs.CopyFile(filepath.Join(r.Path, r.Cover), filepath.Join(folderName, r.Cover), false); err != nil {
			return err
		}
	}
	var quality, title string
	if cbr320 {
		title = "Transcoding to 320"
		quality = "-b 320 -h"
	} else {
		title = "Transcoding to V0"
		quality = "-V0"
	}
	// generate transcoded versions of each track
	var cmds []*exec.Cmd
	var transcodedFiles []string
	for _, t := range r.Flacs {
		filename := filepath.Join(folderName, strings.Replace(filepath.Base(t.Path), FlacExt, Mp3Ext, -1))
		transcodedFiles = append(transcodedFiles, filename)
		cmd := exec.Command("bash", "-c", `flac --decode --stdout "`+t.Path+`" | lame `+quality+` - `+strconv.Quote(filename))
		cmds = append(cmds, cmd)
	}
	if len(transcodedFiles) != len(r.Flacs) {
		return errors.New("all files should be transcoded")
	}

	//  apply commands
	if err := fs.ApplyCommands(cmds, title, false); err != nil {
		return err
	}
	// apply tags
	for i, t := range r.Flacs {
		tags := t.CommonTags()
		title := tags.Title
		if len(tags.Guest) != 0 {
			title += " (feat. " + flac.SmartArtistList(tags.Guest) + ")"
		}
		currentFilename := transcodedFiles[i]
		// opening the transcoded files, overwriting tags
		tag, err := id3v2.Open(currentFilename, id3v2.Options{Parse: false})
		if err != nil {
			return err
		}
		defer tag.Close()

		// setting minimal tags
		tag.SetTitle(title)
		tag.SetAlbum(tags.Album)
		tag.SetArtist(flac.SmartArtistList(tags.Artist))
		tag.SetGenre(tags.Genre)
		tag.SetYear(tags.Date)
		tag.AddTextFrame("TPE2", tag.DefaultEncoding(), flac.SmartArtistList(tags.AlbumArtist))
		tag.AddTextFrame("TPOS", tag.DefaultEncoding(), tags.DiscNumber+"/"+tags.TotalDiscs)
		tag.AddTextFrame("TRCK", tag.DefaultEncoding(), tags.TrackNumber+"/"+tags.TotalTracks)
		tag.AddTextFrame(tag.CommonID("Composer"), tag.DefaultEncoding(), flac.SmartArtistList(tags.Composer))
		
		// writing the tags.
		if err = tag.Save(); err != nil {
			return err
		}
	}
	return nil
}
