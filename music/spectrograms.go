package music

import (
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"image/png"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/fs"
	"golang.org/x/image/font"
	"golang.org/x/image/font/basicfont"
	"golang.org/x/image/math/fixed"
)

const (
	combinedSliceWindowS      = 10
	combinedSliceWindowWidth  = 250
	combinedSliceWindowHeight = 855
)

// GenerateSpectrograms for all FLACs.
func (r *Release) GenerateSpectrograms(title string, verbose bool) ([]string, error) {
	// create metadata dir if necessary
	if mkErr := os.MkdirAll(r.MetadataPath, 0777); mkErr != nil {
		return []string{}, errors.Wrap(mkErr, errorCreatingMetadataDir)
	}
	// generate spectrals for each track
	var cmds []*exec.Cmd
	var paths []string
	for _, t := range r.Flacs {
		spectralName := filepath.Join(r.MetadataPath, strings.Replace(filepath.Base(t.Path), filepath.Ext(t.Path), "", -1)+".spectral.full.png")
		if !fs.FileExists(spectralName) {
			cmds = append(cmds, exec.Command("sox", t.Path, "-n", "remix", "1", "spectrogram", "-x", "1710", "-Y", "855", "-z", "120", "-w", "Kaiser", "-t", filepath.Base(t.Path), "-c", title, "-o", spectralName))
		}
		paths = append(paths, spectralName)
	}
	//  apply commands
	return paths, fs.ApplyCommands(cmds, "Generating spectrograms", verbose)
}

// GenerateCombinedSpectrogram for all FLACs
// This generates slices of combinedSliceWindowS seconds centered around the middle of each song, then combines
// all slices in a single png.
// It should allow seeing at a glance of a single file is something looks like a lossy mastered flac.
func (r *Release) GenerateCombinedSpectrogram(verbose bool) (string, error) {
	// create metadata dir if necessary
	if mkErr := os.MkdirAll(r.MetadataPath, 0777); mkErr != nil {
		return "", errors.Wrap(mkErr, errorCreatingMetadataDir)
	}
	// generate spectrals for each track
	// to try to be representative of the whole song, a 10s slice in the middle of the song is used
	var cmds []*exec.Cmd
	var filenames []string
	for i, t := range r.Flacs {
		// the idea is to get the slice centered around the middle of the song
		halfTime := t.DurationSeconds/2 - combinedSliceWindowS/2
		halfTimeMinutes := int(halfTime / 60)
		halfTimeSeconds := int(halfTime) - 60*halfTimeMinutes
		halfTimeStart := fmt.Sprintf("%2d:%02d", halfTimeMinutes, halfTimeSeconds)
		filename := filepath.Join(r.MetadataPath, strconv.Itoa(i+1)+"_zoom.png")
		if !fs.FileExists(filename) {
			if t.DurationSeconds < combinedSliceWindowS {
				// use everything if the track is too short
				cmds = append(cmds, exec.Command("sox", t.Path, "-n", "remix", "1", "spectrogram", "-r", "-x", strconv.Itoa(combinedSliceWindowWidth), "-y", strconv.Itoa(combinedSliceWindowHeight), "-z", "120", "-w", "Kaiser", "-o", filename))
			} else {
				cmds = append(cmds, exec.Command("sox", t.Path, "-n", "remix", "1", "spectrogram", "-r", "-x", strconv.Itoa(combinedSliceWindowWidth), "-y", strconv.Itoa(combinedSliceWindowHeight), "-z", "120", "-w", "Kaiser", "-S", halfTimeStart, "-d", fmt.Sprintf("0:%02d", combinedSliceWindowS), "-o", filename))
			}
		}
		filenames = append(filenames, filename)
	}
	//  apply commands
	if err := fs.ApplyCommands(cmds, "Generating spectrogram slices for overview", verbose); err != nil {
		return "", err
	}

	// rectangle for the combined image
	combined_r := image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: len(filenames) * combinedSliceWindowWidth, Y: combinedSliceWindowHeight}}
	// new image
	rgba := image.NewRGBA(combined_r)
	currentX := 0

	// add #Number watermark on each
	for i, imgFilename := range filenames {
		// opening the slice
		imgFile, err := os.Open(imgFilename)
		if err != nil {
			return "", err
		}
		img, _, err := image.Decode(imgFile)
		if err != nil {
			return "", err
		}
		// writing the watermark to new image of the same size
		size := img.Bounds()
		watermark := image.NewRGBA(size)
		point := fixed.Point26_6{X: fixed.Int26_6(combinedSliceWindowWidth / 2 * 64), Y: fixed.Int26_6(30 * 64)}
		d := &font.Drawer{
			Dst:  watermark,
			Src:  image.NewUniform(color.White),
			Face: basicfont.Face7x13,
			Dot:  point,
		}
		d.DrawString(fmt.Sprintf("#%02d", i+1))
		// drawing borders to visually separate the slices
		for x := 0; x < combinedSliceWindowWidth; x++ {
			watermark.Set(x, 0, color.White)
			watermark.Set(x, 1, color.White)
			watermark.Set(x, combinedSliceWindowHeight-2, color.White)
			watermark.Set(x, combinedSliceWindowHeight-1, color.White)
		}
		for y := 0; y < combinedSliceWindowHeight; y++ {
			watermark.Set(0, y, color.White)
			watermark.Set(combinedSliceWindowWidth-1, y, color.White)
		}
		// creating watermarked file by copying the watermark over the source
		m := image.NewRGBA(size)
		draw.Draw(m, size, img, image.Point{}, draw.Src)
		offset := image.Pt(0, 0)
		draw.Draw(m, watermark.Bounds().Add(offset), watermark, image.Point{}, draw.Over)
		// combining with the other images
		sp := image.Point{X: currentX, Y: 0}
		draw.Draw(rgba, image.Rectangle{sp, sp.Add(m.Bounds().Size())}, m, image.Point{X: 0, Y: 0}, draw.Src)
		currentX += img.Bounds().Dx()

		// cleanups
		imgFile.Close()
		os.Remove(imgFilename)
	}
	// saving the combined image
	combinedPngPath := filepath.Join(r.MetadataPath, filepath.Base(r.Path)+" overview.png")
	imgw, err := os.Create(combinedPngPath)
	if err != nil {
		return "", err
	}
	if err := png.Encode(imgw, rgba); err != nil {
		return "", err
	}
	return combinedPngPath, imgw.Close()
}
