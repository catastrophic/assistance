package music

import (
	"errors"
	"fmt"
	"math"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"sync"
	"unicode/utf8"

	"github.com/mewkiz/pkg/natsort"

	"gitlab.com/catastrophic/assistance/flac"
	"gitlab.com/catastrophic/assistance/intslice"
	"gitlab.com/catastrophic/assistance/strslice"
)

const (
	acceptableDiscOrTrackNumberFormat = `^(0?\d{1,3}|\w\d*)$`
)

// Check the flac md5.
func (r *Release) Check() error {
	var wg sync.WaitGroup
	var lastErr error
	wg.Add(len(r.Flacs))
	// checking flacs in parallel
	for _, f := range r.Flacs {
		go func(f *flac.Flac) {
			defer wg.Done()
			err := f.Check()
			if err != nil {
				lastErr = err
				return
			}
		}(f)
	}
	// waiting for all tracks to be checked
	wg.Wait()
	return lastErr
}

// CheckForID3v1Tags in the first flac file.
func (r *Release) CheckForID3v1Tags() error {
	// assuming all tracks are encoded the same way, only checking the first track
	if len(r.Flacs) == 0 {
		return errors.New("release contains no tracks")
	}
	return r.Flacs[0].CheckForID3v1Tags()
}

// CheckCompression in the first flac file, to detect uncompressed flacs.
func (r *Release) CheckCompression() error {
	// assuming all tracks are encoded the same way, only checking the first track
	if len(r.Flacs) == 0 {
		return errors.New("release contains no tracks")
	}
	return r.Flacs[0].CheckCompression()
}

// CheckEncoding will return an error if the flac encoding (bit depth/Hz) varies among the Release's FLACS.
func (r *Release) CheckEncoding() error {
	if len(r.Flacs) == 0 {
		return errors.New("release contains no tracks")
	}
	// check all have same audio format
	// Rule 2.1.6
	sameEncoding := true
	for i, t := range r.Flacs {
		if i != 0 {
			sameEncoding = sameEncoding && t.CompareEncoding(r.Flacs[0])
		}
	}
	if !sameEncoding {
		return errors.New("the files do not have the same bit depth and/or sample rate")
	}
	return nil
}

// CheckConsistentBitDepth among tracks.
func (r *Release) CheckConsistentBitDepth() (bool, string) {
	if len(r.Flacs) == 0 {
		return false, "unknown"
	}
	sameBitDepth := true
	for i, t := range r.Flacs {
		if i != 0 {
			sameBitDepth = sameBitDepth && t.CompareBitDepth(r.Flacs[0])
		}
	}
	return sameBitDepth, strconv.Itoa(r.Flacs[0].BitDepth)
}

// CheckConsistentSampleRate among tracks.
func (r *Release) CheckConsistentSampleRate() (bool, string) {
	if len(r.Flacs) == 0 {
		return false, "unknown"
	}
	sameSampleRate := true
	for i, t := range r.Flacs {
		if i != 0 {
			sameSampleRate = sameSampleRate && t.CompareSampleRate(r.Flacs[0])
		}
	}
	return sameSampleRate, strconv.Itoa(r.Flacs[0].SampleRate)
}

func (r *Release) CheckMinMaxBitrates() (int, int) {
	var min, max int
	min = math.MaxInt32
	for _, t := range r.Flacs {
		if t.AverageBitRate < min {
			min = t.AverageBitRate
		}
		if t.AverageBitRate > max {
			max = t.AverageBitRate
		}
	}
	return min, max
}

// Has24bitTracks returns true if at least one track is 24bit.
func (r *Release) Has24bitTracks() bool {
	for _, t := range r.Flacs {
		if t.BitDepth == 24 {
			return true
		}
	}
	return false
}

// CheckMinimalTags from FLACs.
func (r *Release) CheckMinimalTags() []error {
	var errs []error
	switch len(r.Flacs) {
	case 0:
		errs = append(errs, errors.New("release contains no tracks"))
	case 1:
		// check for required tags    Artist    Album
		if err := r.Flacs[0].CheckMinimalTagsForSingle(); err != nil {
			errs = append(errs, fmt.Errorf("%s: %w", r.Flacs[0].Path, err))
		}
	default:
		// check for required tags    Artist    Album    Title    Track Number
		for _, t := range r.Flacs {
			if err := t.CheckMinimalTags(); err != nil {
				errs = append(errs, fmt.Errorf("%s: %w", t.Path, err))
			}
		}
	}
	return errs
}

func (r *Release) CheckConsistentTags() error {
	var problems []string
	totalTracksPerDisc := make(map[string]int)
	firstFlacTags := r.Flacs[0].CommonTags()
	for _, t := range r.Flacs {
		tags := t.CommonTags()
		if tags.Album != firstFlacTags.Album {
			problems = append(problems, "inconsistent album titles")
		}
		if strings.Join(tags.AlbumArtist, "") != strings.Join(firstFlacTags.AlbumArtist, "") {
			problems = append(problems, "inconsistent album artists")
		}
		if tags.TotalDiscs != firstFlacTags.TotalDiscs {
			problems = append(problems, "inconsistent total discs")
		}
		// disc numbers are either all absent or all filled
		if (tags.DiscNumber == "" && firstFlacTags.DiscNumber != "") || (tags.DiscNumber != "" && firstFlacTags.DiscNumber == "") {
			problems = append(problems, "some tracks have a disc number tag, and some do not")
		}
		totalTracksPerDisc[tags.DiscNumber]++
	}

	// checking total tracks
	for _, t := range r.Flacs {
		tags := t.CommonTags()
		if tags.TotalTracks != "" {
			totalTracks, err := strconv.Atoi(tags.TotalTracks)
			if err != nil {
				problems = append(problems, "total tracks tags is not a number")
			}
			// if the total tracks number is the same as in the first track, is OK
			// but for multi-disc releases, total tracks can also be disc-specific
			if totalTracks != totalTracksPerDisc[tags.DiscNumber] && tags.TotalTracks != firstFlacTags.TotalTracks {
				problems = append(problems, "incorrect total tracks")
			}
		}
	}

	if len(problems) != 0 {
		strslice.RemoveDuplicates(&problems)
		return errors.New(strings.Join(problems, ", "))
	}
	return nil
}

func (r *Release) CheckAlbumArtist() error {
	var allArtists []string
	var allAlbumArtists []string
	for _, t := range r.Flacs {
		tags := t.CommonTags()
		allArtists = append(allArtists, tags.Artist...)
		allAlbumArtists = append(allAlbumArtists, strings.Join(tags.AlbumArtist, ""))
	}
	strslice.RemoveDuplicates(&allArtists)
	strslice.RemoveDuplicates(&allAlbumArtists)
	if len(allAlbumArtists) > 1 {
		return errors.New("found conflicting values for album artists")
	}
	if len(allArtists) > 1 && len(r.Flacs[0].CommonTags().AlbumArtist) == 0 {
		return errors.New("multiple artists in release, but the album artist tag is not set")
	}
	return nil
}

// CheckMaxCoverAndPaddingSize among all tracks.
func (r *Release) CheckMaxCoverAndPaddingSize() int {
	var maxSize int
	for _, t := range r.Flacs {
		size := t.CoverSize + t.TotalPaddingSize()
		if size > maxSize {
			maxSize = size
		}
	}
	return maxSize
}

// CheckMaxMetadataSize among all tracks.
func (r *Release) CheckMaxMetadataSize(maxSize int) []error {
	var errs []error
	for _, t := range r.Flacs {
		size := t.CoverSize + t.TotalPaddingSize()
		if size > maxSize {
			errs = append(errs, fmt.Errorf("%s: %w", filepath.Base(t.Path), fmt.Errorf("%w (%dKiB)", flac.ErrorMetadataSize, size/1024)))
		}
	}
	return errs
}

// CheckTrackNumbersInFilenames among all tracks.
func (r *Release) CheckTrackNumbersInFilenames() bool {
	for _, t := range r.Flacs {
		if !t.CheckTrackNumberInFilename() {
			return false
		}
	}
	return true
}

// CheckDiscNumbersInFilenames among all tracks.
func (r *Release) CheckDiscNumbersInFilenames() bool {
	for _, t := range r.Flacs {
		if !t.CheckDiscNumberInFilename() {
			return false
		}
	}
	return true
}

// CheckFilenameContainsStartOfTitle among all tracks.
func (r *Release) CheckFilenameContainsStartOfTitle(minSize int) bool {
	for _, t := range r.Flacs {
		if !t.CheckFilenameContainsStartOfTitle(minSize) {
			return false
		}
	}
	return true
}

func getNumberFromVinylTrackNumbers(in string) (int, error) {
	// if here, then the track number is not a simple integer
	runeA := 'A'
	// A, B, C, D
	if len(in) == 1 {
		// avoiding case issues
		in = strings.ToUpper(in)
		// decoding the first character
		r, _ := utf8.DecodeRuneInString(in)
		number := int32(r-runeA) + 1
		return int(number), nil
	}
	// A1, A2, ..., B1, B2, ... C1, C2, ...
	// no way to know how many tracks each disc side contains...
	return -1, errors.New("cannot check with A1, A2, ..., B1, B2, ... vinyl numbering")
}

func (r *Release) CheckForMissingTracks() error {
	totalTracksByDisc := make(map[string]int)
	tracksByDisc := make(map[string][]int)
	var totalDiscsNumber int
	var additionalDiscFound bool

	// Here we are assuming all tags are consistent and correct
	// If they're not, other checks should find out
	for _, f := range r.Flacs {
		tags := f.CommonTags()
		trackNumber := tags.TrackNumber
		discNumber := tags.DiscNumber
		totalTracks := tags.TotalTracks
		totalDiscs := tags.TotalDiscs

		totalTracksInt, err := strconv.Atoi(totalTracks)
		if err != nil && totalTracks != "" {
			// instead of instantly returning an error (which will be picked up in other checks, silently defaulting to 0
			totalTracksInt = 0
		}

		if val, ok := totalTracksByDisc[discNumber]; !ok {
			totalTracksByDisc[discNumber] = totalTracksInt
		} else {
			if val != totalTracksInt {
				// allowing for a second standalone disc... example: ALBUM + EP releases, each has 1 disc, but different number of tracks.
				totalTracksByDisc[discNumber+"bis"] = totalTracksInt
				additionalDiscFound = true
			}
		}

		tracksInt, err := strconv.Atoi(trackNumber)
		if err != nil || tracksInt == 0 {
			numberEquivalent, attemptErr := getNumberFromVinylTrackNumbers(trackNumber)
			if attemptErr != nil {
				return attemptErr
			} else {
				tracksInt = numberEquivalent
			}
		}

		if additionalDiscFound {
			tracksByDisc[discNumber+"bis"] = append(tracksByDisc[discNumber+"bis"], tracksInt)
		} else {
			tracksByDisc[discNumber] = append(tracksByDisc[discNumber], tracksInt)
		}

		if totalDiscsNumber == 0 {
			totalDiscsInt, err := strconv.Atoi(totalDiscs)
			if err == nil {
				totalDiscsNumber = totalDiscsInt
			}
		}
	}
	if additionalDiscFound {
		totalDiscsNumber++
	}

	var discOrder []string
	for d, _ := range totalTracksByDisc {
		discOrder = append(discOrder, d)
	}
	sort.Strings(discOrder)
	isMultiDisc := len(discOrder) > 1

	// slice of all track numbers
	var allNbTracks []int
	for _, t := range tracksByDisc {
		allNbTracks = append(allNbTracks, t...)
	}
	// getting name of first disc & a slice of all total tracks tags
	var allTotalTracks []int
	var firstDisc string
	for d, tt := range totalTracksByDisc {
		if firstDisc == "" {
			firstDisc = d
		}
		allTotalTracks = append(allTotalTracks, tt)
	}

	// checking if there is continuous ordering of tracks over multiple discs or not
	var isContinuous bool
	if isMultiDisc {
		// if there are no duplicate track numbers, it has continuous numbering or is missing a lot of tracks...
		if !intslice.ContainsDuplicates(allNbTracks) {
			isContinuous = true
			// checking additionally that max(disc N) < min (disc N+1)
			for i := 0; i < len(discOrder); i++ {
				if i == 0 {
					continue
				}
				if intslice.Min(tracksByDisc[discOrder[i]]) <= intslice.Max(tracksByDisc[discOrder[i-1]]) {
					isContinuous = false
				}
			}
		}
	}

	// checking if the TotalTracks tag is
	var isOverallTotalTracks bool
	if isMultiDisc {
		// if they're all the same, we can try to guess if the total tracks info is global or per disc
		isTotalTrackInfoTheSame := intslice.Max(allTotalTracks) == intslice.Min(allTotalTracks)
		if isTotalTrackInfoTheSame {
			var overallNbTracks int
			for _, t := range tracksByDisc {
				overallNbTracks += len(t)
			}
			if overallNbTracks-totalTracksByDisc[firstDisc] <= totalTracksByDisc[firstDisc]-len(tracksByDisc[firstDisc]) {
				isOverallTotalTracks = true
			}
		}
	}

	var missing []int
	var errorMsg []string
	// comparing
	if !isContinuous {
		missingPerDisc := make(map[string][]int)
		var missingTextIntro string
		var missingText []string

		if isOverallTotalTracks {
			// the information about how many tracks per disc there are supposed to be is missing
			// comparing the overall number of tracks vs the total tracks from the first disc
			missingNb := allTotalTracks[0] - len(allNbTracks)
			if missingNb != 0 {
				missingTextIntro = fmt.Sprintf("missing %d track(s) ", missingNb)
			}
			// trying to find missing tracks for each disc (impossible to know if the last track(s) are missing
			for discNo, tracksNo := range tracksByDisc {
				sort.Ints(tracksNo)
				// checking numbers are continuous
				missing = intslice.Difference(intslice.CreateContinuousIntSlice(1, intslice.Max([]int{len(tracksByDisc[discNo]), intslice.Max(tracksByDisc[discNo])})), tracksNo)
				if missing != nil {
					missingPerDisc[discNo] = missing
				}
			}
		} else {
			for discNo, totalTracksNo := range totalTracksByDisc {
				sort.Ints(tracksByDisc[discNo])
				// checking numbers are continuous
				missing = intslice.Difference(intslice.CreateContinuousIntSlice(1, intslice.Max([]int{totalTracksNo, len(tracksByDisc[discNo]), intslice.Max(tracksByDisc[discNo])})), tracksByDisc[discNo])
				if missing != nil {
					missingPerDisc[discNo] = missing
				}
			}
		}

		if len(missingPerDisc) != 0 || missingTextIntro != "" {
			if missingTextIntro == "" {
				missingTextIntro = "missing track(s)"
			}
			for disc, list := range missingPerDisc {
				missingText = append(missingText, fmt.Sprintf("#%s from disc %s", strings.Join(intslice.ToStringSlice(list), ", #"), disc))
			}
			errorMsg = append(errorMsg, fmt.Sprintf("%s %s", missingTextIntro, strings.Join(missingText, ",")))
		}
	}

	if isContinuous {
		sort.Ints(allNbTracks)
		if isOverallTotalTracks {
			// comparing the slice of all track numbers combined vs an incremental list from 1 to the overall total track tags found in all discs
			missing = intslice.Difference(intslice.CreateContinuousIntSlice(1, allTotalTracks[0]), allNbTracks)
		} else {
			// comparing the slice of all track numbers combined vs an incremental list from 1 to the sum of all total track tags from all discs
			missing = intslice.Difference(intslice.CreateContinuousIntSlice(1, intslice.Sum(allTotalTracks)), allNbTracks)
		}
		if missing != nil {
			errorMsg = append(errorMsg, fmt.Sprintf("missing track(s) %s", strings.Join(intslice.ToStringSlice(missing), ", ")))
		}
	}

	// checking if a disc is missing, disregarding if totalDiscs not provided
	if totalDiscsNumber != 0 && (len(tracksByDisc) != totalDiscsNumber || len(totalTracksByDisc) != totalDiscsNumber) {
		errorMsg = append(errorMsg, "missing (at least) a disc")
	}

	// sending the summary of errors
	if len(errorMsg) != 0 {
		return errors.New(strings.Join(errorMsg, ", "))
	}
	return nil
}

func (r *Release) CheckFilenameOrder(withDisc bool) (bool, error) {
	var keys, orderedFilenames, alphabeticallyOrderedFilenames []string
	// making a list of flacs in order of disc+track number according to tags
	filesMap := make(map[string]string)
	// disc and track numbers can have letters in vinyl releases
	rg := regexp.MustCompile(acceptableDiscOrTrackNumberFormat)
	for _, f := range r.Flacs {
		var key string
		tags := f.CommonTags()
		if withDisc {
			if tags.DiscNumber != "" && !rg.MatchString(tags.DiscNumber) {
				return false, errors.New("disc number is not a number")
			}
			// adding an extra space for natsort to do its thing correctly
			key += tags.DiscNumber + " "
		}
		// track numbers can also have vinyl notation
		if !rg.MatchString(tags.TrackNumber) {
			return false, errors.New("track number is not a number")
		}
		key += tags.TrackNumber
		filesMap[key] = f.Path
		keys = append(keys, key)
		alphabeticallyOrderedFilenames = append(alphabeticallyOrderedFilenames, f.Path)
	}
	// checking (disc#, track#) couples are unique
	keysCheck := keys
	strslice.RemoveDuplicates(&keysCheck)
	if len(keys) != len(keysCheck) {
		return false, errors.New("release has tracks with the same number or is missing disc numbers")
	}
	// natural sorting the keys
	natsort.Strings(keys)
	// natural sorting the list of files alphabetically
	natsort.Strings(alphabeticallyOrderedFilenames)

	// generating the list of files ordered using disc/track numbers
	for _, k := range keys {
		orderedFilenames = append(orderedFilenames, filesMap[k])
	}
	// checking they are the same
	return strslice.Equal(orderedFilenames, alphabeticallyOrderedFilenames), nil
}

func (r *Release) HasTracksInSubfolders() bool {
	// checking if it does not look like a multidisc release
	// specifically, if there are no subfolders with flacs inside
	// otherwise it might be a multi-disc release with no disc numbers in tags!
	for _, f := range r.Flacs {
		relativePath, err := filepath.Rel(r.Path, f.Path)
		if err != nil {
			continue
		}
		// would be equal in top folder
		if relativePath != filepath.Base(relativePath) {
			return true
		}
	}
	return false
}

func (r *Release) IsMultiDisc() (bool, error) {
	// checking disc/track numbers are good.
	ordered, err := r.CheckFilenameOrder(true)
	if err != nil {
		return false, err
	}
	if !ordered {
		return false, errors.New("files not ordered according to play order")
	}

	// will not detect if tracks from several discs are in the top folder without disc numbers.
	return r.NumberOfDiscs() > 1 || r.HasTracksInSubfolders(), nil
}

// CheckMultiDiscOrganization checks if multi-disc release are using subfolder or have the disc number in the filenames.
func (r *Release) CheckMultiDiscOrganization() error {
	// if not multidisc *according to tags*, returning
	isMultiDisc, err := r.IsMultiDisc()
	if err != nil {
		return err
	}
	if !isMultiDisc {
		if r.HasTracksInSubfolders() {
			return errors.New("release is organized in subfolders but file tags do not contain the disc number")
		}
		return nil
	}
	// checking if disc number in filenames, if everything is in the top folder
	if !r.HasTracksInSubfolders() && r.CheckDiscNumbersInFilenames() {
		return nil
	}
	// checking if in subfolders
	// keeping a map of subfolder:list of disc numbers as found in tracks inside
	filesInSubfolders := make(map[string][]string)
	var filesInTopFolder []string
	for _, f := range r.Flacs {
		relativePath, err := filepath.Rel(r.Path, f.Path)
		if err != nil {
			return err
		}

		if relativePath == filepath.Base(relativePath) {
			filesInTopFolder = append(filesInTopFolder, f.Path)
		} else {
			filesInSubfolders[filepath.Dir(relativePath)] = append(filesInSubfolders[filepath.Dir(relativePath)], f.CommonTags().DiscNumber)
		}
	}
	if len(filesInTopFolder) != 0 && len(filesInSubfolders) != 0 {
		return errors.New("release has files both in top folder and in subfolders")
	}
	if len(filesInTopFolder) != 0 && len(filesInSubfolders) == 0 {
		ordered, err := r.CheckFilenameOrder(false)
		if !ordered || err != nil {
			return errors.New("release has all files in top folder, but no disc number in filenames")
		}
	}
	if len(filesInTopFolder) == 0 && len(filesInSubfolders) != 0 {
		// checking there are the right number of folders
		if len(filesInSubfolders) != r.NumberOfDiscs() {
			return errors.New("number of disc subfolders not equal to number of discs")
		}

		// checking filesInSubFolders
		var allDiscs []string
		for k, v := range filesInSubfolders {
			vCopy := v
			strslice.RemoveDuplicates(&vCopy)
			if len(vCopy) != 1 {
				return errors.New("subfolder +" + k + " has tracks from more than one disc")
			}
			allDiscs = append(allDiscs, vCopy[0])
		}
		// checking allDiscs elements are unique
		if strslice.HasDuplicates(allDiscs) {
			return errors.New("tracks from the same disc are spread in more than one subfolder")
		}
		// now reasonably confident things are good.
	}
	return nil
}

func (r *Release) CheckVendor() error {
	var vendors []string
	for _, f := range r.Flacs {
		if !strslice.Contains(vendors, f.Vendor()) {
			vendors = append(vendors, f.Vendor())
		}
	}
	if len(vendors) != 1 {
		return errors.New("found different vendor strings")
	}
	return nil
}
