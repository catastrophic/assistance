package music

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"sort"
	"strings"
	"sync"

	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/flac"
	"gitlab.com/catastrophic/assistance/fs"
	"gitlab.com/catastrophic/assistance/strslice"
)

const (
	AdditionalMetadataDir = "Metadata"

	errorCreatingMetadataDir = "Error creating metadata directory"
)

var (
	ErrorMaxLengthExceeded = errors.New("max length of path exceeds 180 characters")
	ErrorNoCover           = errors.New("no cover found")
)

// Release struct to manipulate a folder containing FLAC files and a cover.
type Release struct {
	Path         string
	MetadataPath string
	Flacs        []*flac.Flac
	Cover        string
}

func New(path string) *Release {
	return &Release{Path: path, MetadataPath: filepath.Join(path, AdditionalMetadataDir)}
}

func NewWithExternalMetadata(path, metadataPath string) *Release {
	if metadataPath == "" {
		return New(path)
	}
	return &Release{Path: path, MetadataPath: metadataPath}
}

// Analyze the release folder.
func (r *Release) Analyze() error {
	// parse flacs files
	if err := r.ParseFiles(); err != nil {
		return err
	}
	// check max path length < 180
	// Rule 2.3.12
	if fs.GetMaxPathLength(r.Path) > 180 {
		return ErrorMaxLengthExceeded
	}
	// get cover
	if fs.FileExists(filepath.Join(r.Path, DefaultCover)) {
		r.Cover = DefaultCover
	} else {
		return ErrorNoCover
		// TODO: try to find other pictures?
	}
	return nil
}

func (r *Release) HasCover() bool {
	if fs.FileExists(filepath.Join(r.Path, DefaultCover)) {
		return true
	}
	// checking if cover in subfolders, for multi-disc releases
	// where each disc might have a cover.jpg
	var musicSubFolders []string
	for _, f := range r.Flacs {
		subFolder, err := filepath.Rel(r.Path, filepath.Dir(f.Path))
		if err == nil && subFolder != "" && !strslice.Contains(musicSubFolders, subFolder) {
			musicSubFolders = append(musicSubFolders, subFolder)
		}
	}
	// in that case, all subfolders should have a cover.jpg.
	for _, sbf := range musicSubFolders {
		if !fs.FileExists(filepath.Join(r.Path, sbf, DefaultCover)) {
			return false
		}
	}
	return true
}

func (r *Release) ParseFiles() error {
	// clearing the flacs if running analyse a second time
	r.Flacs = make([]*flac.Flac, 0)
	// finding the tracks again
	flacs := GetAllFLACs(r.Path)
	if len(flacs) == 0 {
		return errors.New("no flac files in directory")
	}
	for i := range flacs {
		nt, err := flac.New(flacs[i])
		if err != nil || nt == nil {
			return err
		}
		r.Flacs = append(r.Flacs, nt)
	}
	return nil
}

// NumberOfDiscs in the release.
func (r Release) NumberOfDiscs() int {
	var allDiscs []string
	for _, t := range r.Flacs {
		tags := t.CommonTags()
		if !strslice.Contains(allDiscs, tags.DiscNumber) {
			allDiscs = append(allDiscs, tags.DiscNumber)
		}
	}
	return len(allDiscs)
}

// Recompress FLACs.
func (r *Release) Recompress() error {
	// recompressing flacs using -8 (multithreaded)
	var cmds []*exec.Cmd
	for _, t := range r.Flacs {
		cmds = append(cmds, exec.Command("flac", "--no-utf8-convert", "-f", "-8", "-V", t.Path))
	}
	//  apply commands
	return fs.ApplyCommands(cmds, "Recompressing flacs", false)
}

// ClearMetadata from FLACs.
func (r *Release) ClearMetadata() error {
	// embedding in all tracks
	for _, t := range r.Flacs {
		t.ClearMetadata()
		if err := t.SaveTags(); err != nil {
			return err
		}
	}
	return nil
}

// ClearMemory from FLACs content.
func (r *Release) ClearMemory() {
	// ClearMemory in all tracks
	for _, t := range r.Flacs {
		t.ClearMemory()
	}
}

// EmbedArtwork in all FLACs.
func (r *Release) EmbedArtwork(cover string, maxSizeKb int) error {
	// resizing cover if necessary, doing this here so that any resizing only occurs once per release
	embeddedArtworkFile, resized, err := fs.ResizeJPGIfNecessary(cover, maxSizeKb)
	if err != nil {
		return err
	}
	if resized {
		defer os.Remove(embeddedArtworkFile)
	}
	imgData, err := ioutil.ReadFile(embeddedArtworkFile)
	if err != nil {
		return err
	}

	// embedding in all tracks, multithreaded
	var wg sync.WaitGroup
	var lastErr error
	wg.Add(len(r.Flacs))
	// checking flacs in parallel
	for _, f := range r.Flacs {
		go func(f *flac.Flac) {
			defer wg.Done()
			err := f.AddCoverFromBytes(imgData)
			if err != nil {
				lastErr = err
				return
			}
			err = f.SaveTags()
			if err != nil {
				lastErr = err
				return
			}
		}(f)
	}
	// waiting for all tracks to be checked
	wg.Wait()
	return lastErr
}

func (r *Release) GetRawTags() string {
	rawTags := make(map[string][]string)
	for _, f := range r.Flacs {
		fTags := f.RawTags()
		var tags []string
		for k, v := range fTags {
			tags = append(tags, fmt.Sprintf("%s=%s", k, strings.Join(v, ",")))
		}
		sort.Strings(tags)
		rawTags[f.Path] = tags
	}
	keys := make([]string, 0, len(rawTags))
	for k := range rawTags {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	var output string
	for _, k := range keys {
		output += k + ":\n"
		if len(rawTags[k]) == 0 {
			output += "- MISSING ALL TAGS!\n"
		} else {
			for _, j := range rawTags[k] {
				output += "- " + j + "\n"
			}
		}
		output += "\n"
	}
	return output
}
