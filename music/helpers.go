package music

import (
	"errors"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/catastrophic/assistance/fs"
	"gitlab.com/catastrophic/assistance/logthis"
	"gitlab.com/catastrophic/assistance/strslice"
)

const (
	FlacExt      = ".flac"
	Mp3Ext       = ".mp3"
	M3uExt       = ".m3u"
	OpusExt      = ".opus"
	JpgExt       = ".jpg"
	WavExt       = ".wav"
	DefaultCover = "cover.jpg"

	foundMusic = "found music"
)

// ContainsMusic returns true if it contains mp3 or flac files.
func ContainsMusic(directoryPath string) bool {
	if err := filepath.Walk(directoryPath, func(path string, f os.FileInfo, err error) error {
		if strslice.Contains([]string{Mp3Ext, FlacExt}, strings.ToLower(filepath.Ext(path))) {
			// stop walking the directory as soon as a track is found
			return errors.New(foundMusic)
		}
		return nil
	}); err == nil || err.Error() != foundMusic {
		return false
	}
	return true
}

// GetFirstFLACFound returns the first FLAC file found in a directory.
func GetFirstFLACFound(directoryPath string) string {
	var firstPath string
	err := filepath.Walk(directoryPath, func(path string, f os.FileInfo, err error) error {
		if strings.ToLower(filepath.Ext(path)) == FlacExt {
			// stop walking the directory as soon as a track is found
			firstPath = path
			return errors.New(foundMusic)
		}
		return nil
	})
	if err != nil && err.Error() == foundMusic {
		return firstPath
	}
	return ""
}

// GetAllFLACs returns all FLAC files found in a directory.
func GetAllFLACs(directoryPath string) []string {
	files, err := fs.GetFilesByExt(directoryPath, FlacExt)
	if err != nil {
		logthis.Error(err, logthis.NORMAL)
	}
	return files
}

// GetAllPlaylists returns all m3u files found in a directory.
func GetAllPlaylists(directoryPath string) []string {
	files, err := fs.GetFilesByExt(directoryPath, M3uExt)
	if err != nil {
		logthis.Error(err, logthis.NORMAL)
	}
	return files
}
