package music

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMusicHelpers(t *testing.T) {
	fmt.Println("+ Testing Music/helpers...")
	check := assert.New(t)

	localFiles := []string{"../testing/mqa16bit.flac", "../testing/mqa24bit.flac", "../testing/notmqa.flac", "../testing/sample.flac", "../testing/sample_with_id3_tags.flac", "../testing/sample_with_id3v1_tags.flac", "../testing/sample_without_tags.flac"}

	check.False(ContainsMusic("."))
	check.False(ContainsMusic("../NOPE"))
	check.True(ContainsMusic("../testing/"))

	check.Equal("", GetFirstFLACFound("."))
	check.Equal("", GetFirstFLACFound("...NOPE"))
	check.Equal(localFiles[0], GetFirstFLACFound("../testing"))

	check.Nil(GetAllFLACs("."))
	check.Nil(GetAllFLACs("../NOPE"))
	check.Equal(localFiles, GetAllFLACs("../testing"))

	check.Nil(GetAllPlaylists("."))
	check.Nil(GetAllPlaylists("../NOPE"))
	check.Equal([]string{"../testing/list.m3u"}, GetAllPlaylists("../testing"))
}
