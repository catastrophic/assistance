package epub

//Container META-INF/container.xml file.
type Container struct {
	Rootfile Rootfile `xml:"rootfiles>rootfile" `
}

// Rootfile root file.
type Rootfile struct {
	Path string `xml:"full-path,attr"`
	Type string `xml:"media-type,attr" `
}

//Opf content.opf.
type Opf struct {
	Metadata Metadata   `xml:"metadata"`
	Manifest []Manifest `xml:"manifest>item"`
	Spine    Spine      `xml:"spine"`
}

// Metadata contains epub metadata.
type Metadata struct {
	Title       []string     `xml:"title"`
	Language    []string     `xml:"language"`
	Identifier  []Identifier `xml:"identifier"`
	Creator     []Author     `xml:"creator" `
	Subject     []string     `xml:"subject"`
	Description []string     `xml:"description"`
	Publisher   []string     `xml:"publisher"`
	Contributor []Author     `xml:"contributor" `
	Date        []Date       `xml:"date" `
	Type        []string     `xml:"type"`
	Format      []string     `xml:"format" `
	Source      []string     `xml:"source" `
	Relation    []string     `xml:"relation" `
	Coverage    []string     `xml:"coverage"`
	Rights      []string     `xml:"rights" `
	Meta        []MetaField  `xml:"meta" `
}

// Identifier identifier.
type Identifier struct {
	Data   string `xml:",chardata" `
	ID     string `xml:"id,attr"`
	Scheme string `xml:"scheme,attr"`
}

// Author author.
type Author struct {
	Data   string `xml:",chardata" `
	FileAs string `xml:"file-as,attr" `
	Role   string `xml:"role,attr" `
}

// Date date.
type Date struct {
	Data  string `xml:",chardata" `
	Event string `xml:"event,attr" `
}

// MetaField metafield.
type MetaField struct {
	Data     string `xml:",chardata" `
	Name     string `xml:"name,attr" `
	Content  string `xml:"content,attr" `
	ID       string `xml:"id,attr" `
	Property string `xml:"property,attr" `
}

//Manifest manifest.
type Manifest struct {
	ID           string `xml:"id,attr" `
	Href         string `xml:"href,attr" `
	MediaType    string `xml:"media-type,attr" `
	Fallback     string `xml:"media-fallback,attr" `
	Properties   string `xml:"properties,attr" `
	MediaOverlay string `xml:"media-overlay,attr" `
}

// Spine spine.
type Spine struct {
	ID              string      `xml:"id,attr" `
	Toc             string      `xml:"toc,attr" `
	PageProgression string      `xml:"page-progression-direction,attr"`
	Items           []SpineItem `xml:"itemref" `
}

// SpineItem spine item.
type SpineItem struct {
	IDref      string `xml:"idref,attr"`
	Linear     string `xml:"linear,attr" `
	ID         string `xml:"id,attr"`
	Properties string `xml:"properties,attr" `
}

//Ncx OPS/toc.ncx.
type Ncx struct {
	Meta   []MetaField `xml:"head>meta"`
	Points []NavPoint  `xml:"navMap>navPoint"`
}

//NavPoint nav point.
type NavPoint struct {
	Text    string     `xml:"navLabel>text"`
	Content Content    `xml:"content"`
	Points  []NavPoint `xml:"navPoint"`
}

//Content nav-point content.
type Content struct {
	Src string `xml:"src,attr"`
}
