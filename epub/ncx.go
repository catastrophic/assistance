package epub

import (
	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/ebook"
)

func (n *Ncx) getISBN() ([]ebook.ISBN, error) {
	for _, m := range n.Meta {
		// looking in content attr
		isbn, err := ebook.CleanISBN(m.Content)
		if err == nil {
			return isbn, err
		}
		// looking in data
		isbn, err = ebook.CleanISBN(m.Data)
		if err == nil {
			return isbn, err
		}
	}
	return nil, errors.New("could not find ISBN")
}
