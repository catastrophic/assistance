package epub

import (
	"archive/zip"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"path"
	"path/filepath"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/ebook"
	"gitlab.com/catastrophic/assistance/fs"
	"gitlab.com/catastrophic/assistance/goodreads"
	"gitlab.com/catastrophic/assistance/strslice"
	"gitlab.com/catastrophic/assistance/ui"
	"golang.org/x/net/html/charset"
)

const (
	mimeType          = "application/epub+zip"
	epubExt           = ".epub"
	internalFiles     = "file contents"
	internalFilenames = "filenames"
)

var uninterestingExtensions = []string{".jpeg", ".jpg", ".png", ".gif", ".otf", ".ttf", ".css", ".plist"}

// Epub book metadata.
type Epub struct {
	Path      string
	Ncx       Ncx
	Opf       Opf
	Container Container
	Mimetype  string
	Hash      string
	Metadata  *ebook.Metadata

	zip *zip.ReadCloser
	gr  *goodreads.GoodReads
}

func New(path string) (*Epub, error) {
	// checks
	if !fs.FileExists(path) {
		return nil, errors.New("file " + path + " does not exist")
	}
	if strings.ToLower(filepath.Ext(path)) != epubExt {
		return nil, errors.New("file " + path + " is not an epub")
	}
	return &Epub{Path: path}, nil
}

func NewWithGoodReads(path, grAPIKey string) (*Epub, error) {
	ep, err := New(path)
	if err != nil {
		return nil, err
	}
	ep.gr = goodreads.New(grAPIKey)
	return ep, nil
}

// GetHash calculates an epub's current hash.
func (e *Epub) GetHash() error {
	hash, err := fs.CalculateSHA256(e.Path)
	if err != nil {
		return err
	}
	e.Hash = hash
	return nil
}

// Check the retail epub integrity.
func (e *Epub) Check() (bool, error) {
	// get current hash
	currentHash, err := fs.CalculateSHA256(e.Path)
	if err != nil {
		return false, err
	}
	// compare with old
	if currentHash != e.Hash {
		return true, nil
	}
	return false, nil
}

// Open and parse an epub file.
func (e *Epub) Parse() error {
	// opening the zip
	z, err := zip.OpenReader(e.Path)
	if err != nil {
		return err
	}
	e.zip = z
	defer e.zip.Close()

	// check mimetype
	mt, err := e.readZippedFile("mimetype")
	if err != nil {
		return err
	}
	e.Mimetype = strings.TrimSpace(string(mt))
	if e.Mimetype != mimeType {
		return errors.New("wrong mimetype")
	}
	// get container contents
	if err := e.parseZippedXMLFile("META-INF/container.xml", &e.Container); err != nil {
		return err
	}
	// get opf
	if err := e.parseZippedXMLFile(e.Container.Rootfile.Path, &e.Opf); err != nil {
		return err
	}
	// get ncx
	for _, mf := range e.Opf.Manifest {
		if mf.ID == e.Opf.Spine.Toc {
			return e.parseZippedXMLFile(e.zipPath(mf.Href), &e.Ncx)
		}
	}
	return nil
}

func (e *Epub) openZippedFile(zippedFile string) (io.ReadCloser, error) {
	for _, f := range e.zip.File {
		if f.Name == zippedFile {
			return f.Open()
		}
	}
	return nil, fmt.Errorf("file %s cannot be found in the epub", zippedFile)
}

func (e *Epub) readZippedFile(path string) ([]byte, error) {
	fd, err := e.openZippedFile(path)
	if err != nil {
		return nil, nil
	}
	defer fd.Close()
	return ioutil.ReadAll(fd)
}

func (e *Epub) parseZippedXMLFile(path string, v interface{}) error {
	fd, err := e.openZippedFile(path)
	if err != nil {
		return nil
	}
	defer fd.Close()
	decoder := xml.NewDecoder(fd)
	decoder.CharsetReader = charset.NewReaderLabel
	return decoder.Decode(&v)
}

func (e *Epub) zipPath(filename string) string {
	return path.Join(path.Dir(e.Container.Rootfile.Path), filename)
}

func (e *Epub) getYear() (string, error) {
	return e.Opf.Metadata.getYear()
}

func (e *Epub) getISBN(askForISBN, useFirstFound bool) (string, error) {
	candidates := new(ebook.ISBNs)
	// look in opf
	opfISBN, err := e.Opf.Metadata.getISBN()
	if err == nil {
		if useFirstFound {
			return opfISBN[0].ISBN13, nil
		}
		for i := range opfISBN {
			opfISBN[i].Location = "OPF"
			candidates.Add(opfISBN[i])
		}
	}
	// else look in ncx
	ncxISBN, err := e.Ncx.getISBN()
	if err == nil {
		if useFirstFound {
			return ncxISBN[0].ISBN13, nil
		}
		for i := range ncxISBN {
			ncxISBN[i].Location = "NCX"
			candidates.Add(ncxISBN[i])
		}
	}

	// this is now desperate. regexp over all files is now the only way.
	// (quick check if epub exists for simpler autotests)
	if fs.FileExists(e.Path) {
		z, err := zip.OpenReader(e.Path)
		if err != nil {
			return "", err
		}
		e.zip = z
		defer e.zip.Close()
		// considering all files
		for _, f := range e.zip.File {
			// try to find in filename
			isbn, err := ebook.CleanISBN(f.Name)
			if err == nil {
				if useFirstFound {
					return isbn[0].ISBN13, nil
				}
				for i := range isbn {
					isbn[i].Location = internalFilenames
					candidates.Add(isbn[i])
				}
			}
			// otherwise, parse all contents
			// ignoring files of no interest.
			if strslice.Contains(uninterestingExtensions, strings.ToLower(filepath.Ext(f.Name))) {
				continue
			}
			// reading contents
			rc, err := f.Open()
			if err != nil {
				fmt.Println("could not read " + f.Name)
				break
			}
			defer rc.Close()
			data, err := ioutil.ReadAll(rc)
			if err != nil {
				fmt.Println("could not read " + f.Name)
				break
			}
			isbn, err = ebook.CleanISBN(string(data))
			if err == nil {
				if useFirstFound {
					return isbn[0].ISBN13, nil
				}
				for i := range isbn {
					isbn[i].Location = internalFiles
					candidates.Add(isbn[i])
				}
			}
		}
	}

	// if nothing was found
	if candidates.Len() == 0 {
		if askForISBN {
			fmt.Println(ui.Yellow("No ISBN could be found in epub " + e.Path))
			if choice, err := ebook.AskForISBN(); err == nil {
				return choice, err
			}
		}
		return "", errors.New("ISBN-13 not found")
	}
	// if more than one candidate was found
	if candidates.Len() > 1 && !useFirstFound {
		var candidateISBNDescriptions []string
		for _, c := range *candidates {
			var isbnInfo string
			if e.gr != nil {
				grInfo, err := e.gr.GetBookInfoByISBN(c.ISBN13)
				if err == nil {
					isbnInfo = " -- " + ui.RedUnderlined("GR:") + " " + ui.Red(grInfo)
				}
			}
			candidateISBNDescriptions = append(candidateISBNDescriptions, ui.GreenBold(c.ISBN13)+isbnInfo+c.Info())
		}
		candidateISBNDescriptions = append(candidateISBNDescriptions, ui.RedBold("None of the above"))
		choice, err := ui.SelectValue("Found several candidates for "+filepath.Base(e.Path)+"\n", "", candidateISBNDescriptions)
		if err != nil {
			fmt.Println("Invalid answer, defaulting to first choice.")
			return (*candidates)[0].ISBN13, nil
		}
		isbnCandidate, err := ebook.CleanISBN(choice)
		if err != nil {
			return "", errors.New("ISBN-13 not found")
		}
		return isbnCandidate[0].ISBN13, nil
	}

	// exactly one candidate was found, here.
	return (*candidates)[0].ISBN13, nil
}

func (e *Epub) ReadMetadata(askForISBN, useFirstISBN bool) error {
	md := &ebook.Metadata{}

	// title
	if len(e.Opf.Metadata.Title) > 0 {
		md.Title = strings.TrimSpace(e.Opf.Metadata.Title[0])
	}

	// authors
	md.Authors = []ebook.Author{}
	for _, a := range e.Opf.Metadata.Creator {
		md.Authors = append(md.Authors, ebook.Author{Name: strings.TrimSpace(a.Data)})
	}

	// isbn
	isbn, err := e.getISBN(askForISBN, useFirstISBN)
	if err == nil {
		md.ISBN = isbn
	}

	// year
	year, err := e.getYear()
	if err == nil {
		md.EditionYear = year
		// assuming this is the first edition, by default
		md.OriginalYear = year
	}

	// description
	if len(e.Opf.Metadata.Description) > 0 {
		md.Description = ebook.CleanDescription(strings.Join(e.Opf.Metadata.Description, "\n"))
	}

	// TODO md.Series        Series

	// tags
	var tags ebook.Tags
	for _, t := range e.Opf.Metadata.Subject {
		tags = append(tags, ebook.Tag{Name: strings.TrimSpace(t)})
	}
	md.Tags = tags

	// TODO md.Category = (fiction/nonfiction)
	// TODO md.Type = (novel/anthology/etc)

	// genre = first tag
	if len(e.Opf.Metadata.Subject) > 0 {
		md.Genre = strings.TrimSpace(e.Opf.Metadata.Subject[0])
	}

	// language
	if len(e.Opf.Metadata.Language) > 0 {
		// TODO what if there are several languages?
		md.Language = ebook.CleanLanguage(e.Opf.Metadata.Language[0])
	}

	// publisher
	if len(e.Opf.Metadata.Publisher) > 0 {
		// TODO gutenberg use "source"
		// TODO what if there are several
		md.Publisher = strings.TrimSpace(e.Opf.Metadata.Publisher[0])
	}

	// saving to struct
	md.Clean()
	e.Metadata = md

	return nil
}

func (e *Epub) GetOnlineMetadata(showDiff bool, forbiddenTags []string, tagAliases map[string][]string) error {
	if e.gr == nil {
		return errors.New("goodreads key has not been provided")
	}
	grID, err := e.gr.GetBookID(e.Metadata)
	if err != nil {
		return err
	}
	onlineMetadata, err := e.gr.GetBookSpecificClean(grID, forbiddenTags, tagAliases)
	if err != nil {
		return err
	}
	onlineMetadata.ImageURL = goodreads.LargeImageURL(onlineMetadata.ImageURL)

	if showDiff {
		ui.Header("Epub vs Online Metadata")
		var rows [][]string
		rows = append(rows, []string{"", ebook.LocalSource, ebook.OnlineSource})
		rows = append(rows, e.Metadata.Diff(onlineMetadata, false)...)
		fmt.Println(ui.TabulateRows(rows))
	}

	ui.Header("Merging Epub & Online Metadata")
	if err := e.Metadata.Merge(onlineMetadata, false); err != nil {
		return err
	}
	return nil
}

// ShowInfo returns a table with relevant information about a book.
func (e *Epub) ShowInfo(fields ...string) string {
	if e.Metadata == nil {
		return ""
	}
	return ui.TabulateRows(e.Metadata.GetInfo(fields...), "Info", "Book")
}

func (e *Epub) SaveMetadataTo(filename string) error {
	if e.Metadata == nil {
		return errors.New("no metadata to save")
	}
	data, err := json.MarshalIndent(e.Metadata, "  ", "    ")
	if err != nil {
		return err
	}
	return ioutil.WriteFile(filename, data, 0666)
}

func (e *Epub) SaveMetadata() error {
	if e.Metadata == nil {
		return errors.New("no metadata to save")
	}
	var filename string
	if e.Metadata.ISBN != "" {
		filename = e.Metadata.ISBN + ". "
	}
	filename += e.Metadata.Title + " - " + e.Metadata.Author()
	return e.SaveMetadataTo(filename)
}

func (e *Epub) LoadMetadata(file string) error {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return errors.Wrap(err, "error loading metadata")
	}
	return json.Unmarshal(data, &e.Metadata)
}
