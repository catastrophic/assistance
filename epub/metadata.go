package epub

import (
	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/ebook"
)

func (m *Metadata) getYear() (string, error) {
	var year string
	var found bool
	if len(m.Date) > 0 {
		for _, d := range m.Date {
			if d.Event == "publication" && len(d.Data) >= 4 {
				found = true
				year = d.Data[0:4]
				break
			}
		}
		if !found {
			// if no particular event found, using the first by default
			if len(m.Date[0].Data) >= 4 {
				year = m.Date[0].Data[0:4]
			}
		}
	} else {
		// go through the meta fields
		for _, f := range m.Meta {
			if f.Property == "dcterms:date" && len(f.Data) >= 4 {
				found = true
				year = f.Data[0:4]
				break
			}
		}
		if !found {
			return "", errors.New("could not find edition year")
		}
	}
	return year, nil
}

func (m *Metadata) getISBN() ([]ebook.ISBN, error) {
	if len(m.Identifier) > 0 {
		for _, i := range m.Identifier {
			isbn, err := ebook.CleanISBN(i.Data)
			if err == nil {
				return isbn, err
			}
			// checking the attributes, some metadata is really awful.
			isbn, err = ebook.CleanISBN(i.ID)
			if err == nil {
				return isbn, err
			}
			isbn, err = ebook.CleanISBN(i.Scheme)
			if err == nil {
				return isbn, err
			}
		}
	}
	// go through the meta fields
	for _, f := range m.Meta {
		if f.Property == "source" {
			isbn, err := ebook.CleanISBN(f.Data)
			if err == nil {
				return isbn, err
			}
		}
	}
	return nil, errors.New("could not find ISBN")
}
