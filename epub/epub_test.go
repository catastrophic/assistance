package epub

import (
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/catastrophic/assistance/ebook"
	"gitlab.com/catastrophic/assistance/fs"
	"gitlab.com/catastrophic/assistance/ui"
)

type testEpub struct {
	opfURL              string
	ncxURL              string
	epub                string
	opf                 string
	ncx                 string
	expectedHash        string
	expectedTitle       string
	expectedAuthors     []ebook.Author
	expectedYear        string
	expectedISBN        string
	expectedDescription string
	expectedTags        []string
	expectedSubject     string
	expectedPublisher   string
	expectedLanguage    string
}

var testEpubs = []testEpub{
	{
		epub:                "../testing/cc.epub",
		expectedTitle:       "Made with Creative Commons",
		expectedAuthors:     []ebook.Author{{Name: "Paul Stacey and Sarah Hinchliff Pearson"}},
		expectedHash:        "bb09b9b71e3c306b0e698f4f8a55718820c2137c5ecf7d2b58dfce11e3df8d6b",
		expectedYear:        "2017",
		expectedISBN:        "9788799873333",
		expectedDescription: "Part analysis, part handbook, part collection of case studies, this book is a guide to sharing your knowl-edge and creativity with the world, and sustaining your operation while you do. Going from a propri-etary all-rights-reserved model to one that lets others copy, reuse, and modify your work is a big change. Made with Creative Commons describes the mindshift, the benefits, and the practices that come with going “open.”\nIt makes the case that sharing is good for business, especially for companies, organizations, and cre-ators who care about more than just the bottom line. Full of practical advice and inspiring stories, Made with Creative Commons is a book that will show you what it really means to share.",
		expectedTags:        []string{"this is a book about sharing. it is about sharing textbooks, music, data, art, more. people, organizations, and businesses all over the world are sharing their work using creative commons licenses because they want to encourage the public to reuse their works, to copy them, to modify them. they are made with creative commons."},
		expectedSubject:     "this is a book about sharing. it is about sharing textbooks, music, data, art, more. people, organizations, and businesses all over the world are sharing their work using creative commons licenses because they want to encourage the public to reuse their works, to copy them, to modify them. they are made with creative commons.",
		expectedPublisher:   "Ctrl+Alt+Delete Books http://www.cadb.dk",
		expectedLanguage:    "en",
	},
	{
		epub:                "../testing/pg16328.epub",
		expectedTitle:       "Beowulf / An Anglo-Saxon Epic Poem",
		expectedHash:        "dc325b3aceb77d9f943425728c037fdcaf4af58e3abd771a8094f2424455cc03",
		expectedYear:        "2005",
		expectedISBN:        "",
		expectedDescription: "",
		expectedTags:        []string{"epic poetry, english (old)", "monsters -- poetry", "dragons -- poetry"},
		expectedSubject:     "epic poetry, english (old)",
		expectedPublisher:   "",
		expectedLanguage:    "en",
	},
	{
		opfURL:              "https://pastebin.com/raw/SXvsjf5t",
		opf:                 "../testing/1.opf",
		ncx:                 "",
		expectedTitle:       "Into the Gray",
		expectedAuthors:     []ebook.Author{{Name: "Margaret Killjoy"}},
		expectedHash:        "",
		expectedYear:        "2018",
		expectedISBN:        "9781250198174",
		expectedDescription: "\"You're using me,\" I said.\n\"That might be true, but I also love you.\"\nOne is the Lady of the Waking Waters, an immortal mermaid. The other is a thief, who steals lives until a wish can be fulfilled, and a life-changing choice must be made, in Margaret Killjoys Tor.com Original Into the Gray. \nAt the Publisher's request, this title is being sold without Digital Rights Management Software (DRM) applied.",
		expectedSubject:     "",
		expectedPublisher:   "Tom Doherty Associates",
		expectedLanguage:    "en",
	},
	{
		opfURL:              "https://pastebin.com/raw/uiHZ1Tyi",
		opf:                 "../testing/2.opf",
		ncx:                 "",
		expectedTitle:       "1Q84",
		expectedAuthors:     []ebook.Author{{Name: "Haruki Murakami"}},
		expectedHash:        "",
		expectedYear:        "2011",
		expectedISBN:        "9780307957023",
		expectedDescription: "\"Murakami is like a magician who explains what he's doing as he performs the trick and still makes you believe he has supernatural powers . . . But while anyone can tell a story that resembles a dream, it's the rare artist, like this one, who can make us feel that we are dreaming it ourselves.\" --The New York Times Book Review\n  \n The year is 1984 and the city is Tokyo.\nA young woman named Aomame follows a taxi driver's enigmatic suggestion and begins to notice puzzling discrepancies in the world around her. She has entered, she realizes, a parallel existence, which she calls 1Q84 --\"Q is for 'question mark.' A world that bears a question.\" Meanwhile, an aspiring writer named Tengo takes on a suspect ghostwriting project. He becomes so wrapped up with the work and its unusual author that, soon, his previously placid life begins to come unraveled. \n As Aomame's and Tengo's narratives converge over the course of this single year, we learn of the...",
		expectedSubject:     "",
		expectedPublisher:   "Knopf Doubleday Publishing Group",
		expectedLanguage:    "en",
	},
	{
		opfURL:              "https://pastebin.com/raw/2vqMALBc",
		opf:                 "../testing/3.opf",
		ncx:                 "",
		expectedTitle:       "L'Argent",
		expectedAuthors:     []ebook.Author{{Name: "Emile Zola"}},
		expectedHash:        "",
		expectedYear:        "2012",
		expectedISBN:        "9782081236981",
		expectedDescription: "Version 27946 - 2012-01-06 11:57:56 +0100",
		expectedPublisher:   "Editions Flammarion",
		expectedLanguage:    "fr",
	},
	{
		opfURL:        "https://pastebin.com/raw/DK0U3s6X",
		opf:           "../testing/4.opf",
		ncxURL:        "https://pastebin.com/raw/XfaLTbA9",
		ncx:           "../testing/4.ncx",
		expectedTitle: "La Naissance de la tragédie",
		// no author in metadata...
		expectedHash:        "",
		expectedYear:        "2015",
		expectedISBN:        "9782081374065",
		expectedDescription: "",
		expectedPublisher:   "Flammarion",
		expectedLanguage:    "fr",
	},
	{
		opfURL:              "https://pastebin.com/raw/Rci3QNV1",
		opf:                 "../testing/5.opf",
		ncx:                 "",
		expectedTitle:       "Last Song Before Night",
		expectedAuthors:     []ebook.Author{{Name: "Ilana C. Myer"}},
		expectedHash:        "",
		expectedYear:        "2015",
		expectedISBN:        "9781466861039",
		expectedDescription: "Her name was Kimbralin Amaristoth: sister to a cruel brother, daughter of a hateful family. But that name she has forsworn, and now she is simply Lin, a musician and lyricist of uncommon ability in a land where women are forbidden to answer such callings—a fugitive who must conceal her identity or risk imprisonment and even death.\nOn the eve of a great festival, Lin learns that an ancient scourge has returned to the land of Eivar, a pandemic both deadly and unnatural. Its resurgence brings with it the memory of an apocalypse that transformed half a continent. Long ago, magic was everywhere, rising from artistic expression—from song, from verse, from stories. But in Eivar, where poets once wove enchantments from their words and harps, the power was lost. Forbidden experiments in blood divination unleashed the plague that is remembered as the Red Death, killing thousands before it was stopped, and Eivar's connection to the Otherworld from which all...",
		expectedPublisher:   "Tom Doherty Associates",
		expectedLanguage:    "en",
	},
	{
		opfURL:              "https://pastebin.com/raw/YmLEiuvn",
		opf:                 "../testing/6.opf",
		ncx:                 "",
		expectedTitle:       "Lightspeed: Year One",
		expectedAuthors:     []ebook.Author{{Name: "Vylar Kaftan"}, {Name: "Jack McDevitt"}, {Name: "David Barr Kirtley"}, {Name: "Carrie Vaugh"}, {Name: "Carol Emshwiller"}, {Name: "Tobias S. Buckell"}, {Name: "Genevieve Valentine"}, {Name: "George R. R. Martin"}, {Name: "Catherynne M. Valente"}, {Name: "Tananaritive Due"}, {Name: "Adam-Troy Castro"}, {Name: "Joe Haldeman"}, {Name: "Yoon Ha Lee"}, {Name: "Geoffrey A. Landis"}, {Name: "Cat Rambo"}, {Name: "Robert Silverberg"}, {Name: "Sarah Langan"}, {Name: "Joe R. Lansdale"}, {Name: "John R. Fultz"}, {Name: "Stephen King"}, {Name: "Charles Yu"}, {Name: "Caitlin R. Kiernan"}, {Name: "Alice Sola Kim"}, {Name: "Nancy Kress"}, {Name: "Ted Kosmatka"}, {Name: "Kristine Kathryn Rusch"}, {Name: "David Tallerman"}, {Name: "Ursula K. Le Guin"}, {Name: "Corey Mariani"}, {Name: "Susan Palwick"}, {Name: "Tanith Lee"}, {Name: "Orson Scott Card"}, {Name: "Julie E. Czerneda"}, {Name: "Ken Liu"}, {Name: "James Patrick Kelly"}, {Name: "Maggie Clark"}, {Name: "Stephen Baxter"}, {Name: "Nnedi Okorafor"}, {Name: "Robert Reed"}, {Name: "An Owomoyela"}, {Name: "Bruce Sterling"}, {Name: "Tom Crosshill"}, {Name: "Anne McCaffrey"}, {Name: "Eric Gregory"}, {Name: "Tessa Mellas"}, {Name: "Alastair Reynolds"}},
		expectedHash:        "",
		expectedYear:        "2011",
		expectedISBN:        "9781607013334",
		expectedDescription: "",
		expectedTags:        []string{"science fiction", "anthology", "short stories"},
		expectedSubject:     "science fiction",
		expectedPublisher:   "Prime Books",
		expectedLanguage:    "en",
	},
	{
		opfURL:              "https://pastebin.com/raw/PDbMwFmP",
		opf:                 "../testing/7.opf",
		ncxURL:              "https://pastebin.com/raw/vbxXnZFW",
		ncx:                 "../testing/7.ncx",
		expectedTitle:       "Little Tales of Misogyny",
		expectedAuthors:     []ebook.Author{{Name: "Patricia Highsmith"}},
		expectedHash:        "",
		expectedYear:        "2015",
		expectedISBN:        "9780349004945",
		expectedDescription: "",
		expectedPublisher:   "virago",
		expectedLanguage:    "en",
	},
	{
		opfURL:              "https://pastebin.com/raw/9XktfhYu",
		opf:                 "../testing/8.opf",
		ncx:                 "",
		expectedTitle:       "Machine Learning with TensorFlow 1.x",
		expectedAuthors:     []ebook.Author{{Name: "Quan Hua"}},
		expectedHash:        "",
		expectedYear:        "2018",
		expectedISBN:        "9781786461988",
		expectedDescription: "TensorFlow 1.x is an open source software library for numerical computation using data flow graphs. This book approaches common commercial machine learning problems using Google’s TensorFlow 1.x library. It covers unique features of the library such as Data Flow Graphs, training, visualization of performance with TensorBoard—all within a context rich with examples, using problems from multiple industries.",
		expectedTags:        []string{"com021030 - computers / databases / data mining", "com018000 - computers / data processing", "com051360 - computers / programming languages / python"},
		expectedSubject:     "com021030 - computers / databases / data mining",
		expectedPublisher:   "Packt",
		expectedLanguage:    "en",
	},
	{
		epub:                "../testing/9.epub",
		ncx:                 "",
		expectedTitle:       "Title of this epub",
		expectedAuthors:     []ebook.Author{{Name: "Author"}},
		expectedHash:        "fbb0cad49431045bf63a9999f8cabb196a678cc5ab9e122e9b2905bdd8401a64",
		expectedYear:        "2019",
		expectedISBN:        "9781786461988",
		expectedDescription: "Sed tamen haec cum ita tutius observentur, quidam vigore artuum inminuto rogati ad nuptias ubi aurum dextris manibus cavatis offertur, inpigre vel usque Spoletium pergunt. haec nobilium sunt instituta.",
		expectedPublisher:   "publisher",
		expectedLanguage:    "en",
	},
	{
		epub:                "../testing/10.epub", // same, with IBSN10 in html
		ncx:                 "",
		expectedTitle:       "Title of this epub",
		expectedAuthors:     []ebook.Author{{Name: "Author"}},
		expectedHash:        "94e79210788aa1a0ad070137e35ab711ddd8ed911e75f67362d7f4b970ffad3d",
		expectedYear:        "2019",
		expectedISBN:        "9781786461988",
		expectedDescription: "Sed tamen haec cum ita tutius observentur, quidam vigore artuum inminuto rogati ad nuptias ubi aurum dextris manibus cavatis offertur, inpigre vel usque Spoletium pergunt. haec nobilium sunt instituta.",
		expectedPublisher:   "publisher",
		expectedLanguage:    "en",
	},
	{
		epub:                "../testing/11.epub", // same, with IBSN10 in filename
		ncx:                 "",
		expectedTitle:       "Title of this epub",
		expectedAuthors:     []ebook.Author{{Name: "Author"}},
		expectedHash:        "66a90e4819396348e373dce0b2ec268fd78d8dfed50bd591337ce21cc152e222",
		expectedYear:        "2019",
		expectedISBN:        "9781786461988",
		expectedDescription: "Sed tamen haec cum ita tutius observentur, quidam vigore artuum inminuto rogati ad nuptias ubi aurum dextris manibus cavatis offertur, inpigre vel usque Spoletium pergunt. haec nobilium sunt instituta.",
		expectedPublisher:   "publisher",
		expectedLanguage:    "en",
	},
}

func TestEpubMetadata(t *testing.T) {
	fmt.Println("+ Testing Epub Metadata...")
	check := assert.New(t)

	// error checks
	_, err := New("zzzz")
	check.NotNil(err)
	_, err = New("../testing/list.m3u")
	check.NotNil(err)

	for _, te := range testEpubs {
		var ep *Epub
		var err error
		if te.epub != "" {
			//  load epub
			ep, err = New(te.epub)
			check.Nil(err)
			check.Nil(ep.Parse())
			// check hash
			if te.expectedHash != "" {
				check.Nil(ep.GetHash())
				check.Equal(te.expectedHash, ep.Hash)
			}
		} else {
			var o Opf
			var n Ncx
			// load opf
			if !fs.FileExists(te.opf) && te.opfURL != "" {
				check.Nil(fs.DownloadFile(te.opf, te.opfURL))
			}
			data, err := ioutil.ReadFile(te.opf)
			check.Nil(err)
			check.Nil(xml.Unmarshal(data, &o))
			if te.ncx != "" {
				// load ncx too
				if !fs.FileExists(te.ncx) && te.ncxURL != "" {
					check.Nil(fs.DownloadFile(te.ncx, te.ncxURL))
				}
				data, err = ioutil.ReadFile(te.ncx)
				check.Nil(err)
				check.Nil(xml.Unmarshal(data, &n))
			}
			ep = &Epub{Opf: o, Ncx: n}
		}

		err = ep.ReadMetadata(false, true)
		check.Nil(err)

		// check title
		if te.expectedTitle != "" {
			check.Equal(te.expectedTitle, ep.Metadata.Title)
		}
		// check ISBN
		if te.expectedISBN != "" {
			check.Equal(te.expectedISBN, ep.Metadata.ISBN)
		}
		// check year
		if te.expectedYear != "" {
			check.Equal(te.expectedYear, ep.Metadata.EditionYear)
			check.Equal(te.expectedYear, ep.Metadata.OriginalYear)
		}
		// check authors
		if len(te.expectedAuthors) != 0 {
			check.Equal(te.expectedAuthors, ep.Metadata.Authors)
		}
		// check description
		if te.expectedDescription != "" {
			check.Equal(te.expectedDescription, ep.Metadata.Description)
		}
		// check tags
		if len(te.expectedTags) != 0 {
			var tagNames []string
			for _, tg := range ep.Metadata.Tags {
				tagNames = append(tagNames, tg.Name)
			}
			check.Equal(te.expectedTags, tagNames)
			// check subject
			if te.expectedSubject != "" {
				check.NotEmpty(tagNames)
				check.Equal(te.expectedSubject, tagNames[0])
			}
		}
		// check publisher
		if te.expectedPublisher != "" {
			check.Equal(te.expectedPublisher, ep.Metadata.Publisher)
		}
		// check language
		if te.expectedPublisher != "" {
			check.Equal(te.expectedLanguage, ep.Metadata.Language)
		}
	}

	key := os.Getenv("GR_API_KEY")
	require.NotEqual(t, 0, len(key), "Cannot get Goodreads API key")

	ep, err := NewWithGoodReads("../testing/cc.epub", key)
	check.Nil(err)
	check.Nil(ep.Parse())
	err = ep.ReadMetadata(false, true)
	check.Nil(err)

	// display only
	fmt.Println(ep.ShowInfo())

	// get online metadata and show diff
	err = ep.GetOnlineMetadata(true, nil, nil)
	// should be EOF because merge would require user input
	check.Equal(io.EOF, err)
	fmt.Println(ui.TabulateRows(ep.Metadata.Diff(ep.Metadata, false), ebook.LocalSource, ebook.OnlineSource))
	fmt.Println(ui.TabulateRows(ep.Metadata.Diff(ep.Metadata, true), ebook.LocalSource, ebook.OnlineSource))

	// merge
}
