package fs

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFileSizes(t *testing.T) {
	fmt.Println("+ Testing File Sizes...")
	check := assert.New(t)

	check.Equal("15.000B", FileSize(uint64(15)))
	check.Equal("1000.000B", FileSize(uint64(1000)))
	check.Equal("1.000KiB", FileSize(uint64(1024)))
	check.Equal("1.500KiB", FileSize(uint64(1.5*1024)))
	check.Equal("1.000MiB", FileSize(uint64(1024*1024)))
	check.Equal("1.000GiB", FileSize(uint64(1024*1024*1024)))
	check.Equal("1.000TiB", FileSize(uint64(1024*1024*1024*1024)))
	check.Equal("2.500TiB", FileSize(uint64(2.5*1024*1024*1024*1024)))
	check.Equal("2.500TiB", FileSize(uint64(2.5*1024*1024*1024*1024)))
	check.Equal("944.383KiB", FileSize(uint64(967048)))

	check.Equal("+15.000B", FileSizeDelta(int64(15)))
	check.Equal("+1000.000B", FileSizeDelta(int64(1000)))
	check.Equal("+1.000KiB", FileSizeDelta(int64(1024)))
	check.Equal("+1.500KiB", FileSizeDelta(int64(1.5*1024)))
	check.Equal("+1.000MiB", FileSizeDelta(int64(1024*1024)))
	check.Equal("+1.000GiB", FileSizeDelta(int64(1024*1024*1024)))
	check.Equal("+1.000TiB", FileSizeDelta(int64(1024*1024*1024*1024)))
	check.Equal("+2.500TiB", FileSizeDelta(int64(2.5*1024*1024*1024*1024)))
	check.Equal("+2.500TiB", FileSizeDelta(int64(2.5*1024*1024*1024*1024)))
	check.Equal("+944.383KiB", FileSizeDelta(int64(967048)))

	check.Equal("-15.000B", FileSizeDelta(int64(-15)))
	check.Equal("-1000.000B", FileSizeDelta(int64(-1000)))
	check.Equal("-1.000KiB", FileSizeDelta(int64(-1024)))
	check.Equal("-1.500KiB", FileSizeDelta(int64(-1.5*1024)))
	check.Equal("-1.000MiB", FileSizeDelta(int64(-1024*1024)))
	check.Equal("-1.000GiB", FileSizeDelta(int64(-1024*1024*1024)))
	check.Equal("-1.000TiB", FileSizeDelta(int64(-1024*1024*1024*1024)))
	check.Equal("-2.500TiB", FileSizeDelta(int64(-2.5*1024*1024*1024*1024)))
	check.Equal("-2.500TiB", FileSizeDelta(int64(-2.5*1024*1024*1024*1024)))
	check.Equal("-944.383KiB", FileSizeDelta(int64(-967048)))

	check.Equal("+", Sign(int64(15)))
	check.Equal("-", Sign(int64(-15)))
}
