package fs

import (
	"os"
	"path/filepath"
	"strings"

	"github.com/disintegration/imaging"
	"github.com/pkg/errors"
)

// ResizeJPGIfNecessary and return the path to the acceptable image,
// and whether it is the original or a resized version.
func ResizeJPGIfNecessary(image string, maxSizeKb int) (string, bool, error) {
	if !strings.EqualFold(filepath.Ext(image), ".jpg") {
		return "", false, errors.New("not a jpg image")
	}
	var resized bool
	info, err := os.Stat(image)
	if err != nil {
		return "", false, err
	}
	size := int(info.Size())
	compliantImage := image

	var attempts int
	for size > maxSizeKb*1024 {
		// resizing the image
		originalCover, err := imaging.Open(image)
		if err != nil {
			return "", false, err
		}
		originalWidth := originalCover.Bounds().Dx()
		// with each new attempt, resizing to 10% less width, keeping aspect ratio
		// first pass tries to just resave/recompress the jpg
		ratio := 1 - 0.1*float32(attempts)
		originalCover = imaging.Resize(originalCover, int(float32(originalWidth)*ratio), 0, imaging.Lanczos)
		// saving
		resizedCover := strings.Replace(image, filepath.Ext(image), "_resized"+filepath.Ext(image), -1)
		err = imaging.Save(originalCover, resizedCover, imaging.JPEGQuality(75))
		if err != nil {
			return "", false, err
		}
		// checking the size is now acceptable
		info, err := os.Stat(resizedCover)
		if err != nil {
			return "", false, err
		}
		size = int(info.Size())
		if size <= maxSizeKb*1024 {
			compliantImage = resizedCover
			resized = true
			break
		}
		attempts++
	}
	return compliantImage, resized, nil
}
