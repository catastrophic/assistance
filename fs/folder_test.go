package fs

import (
	"fmt"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestDirectories(t *testing.T) {
	fmt.Println("+ Testing Directories...")
	check := assert.New(t)

	// testing on non-empty dir
	currentdir, err := os.Getwd()
	require.Nil(t, err, "Error getting current directory")
	isEmpty, err := DirIsEmpty(currentdir)
	check.Nil(err, "Error opening current directory")
	check.False(isEmpty, "Current directory is not empty")
	exists := DirExists(currentdir)
	check.True(exists, "Current directory exists")

	// testing on non existing dir
	nonExistingDir := filepath.Join(currentdir, "doesnotexist")
	_, err = DirIsEmpty(nonExistingDir)
	check.NotNil(err, "Non existing directory should have triggered error")
	exists = DirExists(nonExistingDir)
	check.False(exists, "Directory does not exist")

	// testing on empty dir
	err = os.Mkdir(nonExistingDir, 0755)
	require.Nil(t, err, "Could not get create directory")
	isEmpty, err = DirIsEmpty(nonExistingDir)
	check.Nil(err, "Non existing directory should not have triggered error")
	check.True(isEmpty, "Directory should be empty")
	exists = DirExists(nonExistingDir)
	check.True(exists, "Directory now exists")

	// moving
	nonExistingDir2 := nonExistingDir + "2"
	check.True(DirExists(nonExistingDir))
	check.False(DirExists(nonExistingDir2))
	// do nothing, but pretend
	hasMoved, err1 := MoveDir(nonExistingDir, nonExistingDir2, true, false)
	check.Nil(err1)
	check.True(hasMoved)
	check.True(DirExists(nonExistingDir))
	check.False(DirExists(nonExistingDir2))
	// move
	hasMoved, err1 = MoveDir(nonExistingDir, nonExistingDir2, false, false)
	check.Nil(err1)
	check.True(hasMoved)
	check.False(DirExists(nonExistingDir))
	check.True(DirExists(nonExistingDir2))

	// cleanup
	check.Nil(DeleteEmptyDirs(currentdir, []string{nonExistingDir2}))
	check.False(DirExists(nonExistingDir))
	check.True(DirExists(nonExistingDir2))
	check.Nil(DeleteEmptyDirs(currentdir, []string{}))
	check.False(DirExists(nonExistingDir))
	check.False(DirExists(nonExistingDir2))

	// GetFiles
	files, err := GetFilesByExt("../testing/", ".m3u")
	check.Nil(err)
	check.Equal(1, len(files))
	check.Equal([]string{"../testing/list.m3u"}, files)

	// CopyDir
	check.False(DirExists("temp"))
	check.Nil(CopyDir("../testing/", "temp", false))
	check.True(DirExists("temp"))
	files, err = GetFilesByExt("temp", ".m3u")
	check.Nil(err)
	check.Equal(1, len(files))
	check.Equal([]string{"temp/list.m3u"}, files)
	check.Nil(os.RemoveAll("temp"))
}

func TestSanitizeFolder(t *testing.T) {
	fmt.Println("+ Testing SanitizePath...")
	check := assert.New(t)

	check.Equal("hop", SanitizePath("////hop"))
	check.Equal("hop∕hop", SanitizePath("////hop∕hop"))
}

func TestMaxLength(t *testing.T) {
	fmt.Println("+ Testing MaxLength...")
	check := assert.New(t)

	check.Equal(74, GetMaxPathLength("../testing"))
	tooLong := GetExceedinglyLongPaths("../testing", 70)
	check.Len(tooLong, 1)
	check.Equal("../testing/this_is_a_very_long_folder_name_for_testing_purposes/long_filename", tooLong[0])
	tooLong = GetExceedinglyLongPaths("../testing", 80)
	check.Len(tooLong, 0)
}

func TestHasEmptyNestedFolders(t *testing.T) {
	fmt.Println("+ Testing HasEmptyNestedFolders...")
	check := assert.New(t)

	// checking on testing dir
	check.False(HasEmptyNestedFolders("../testing"))

	empty := "../testing/empty"
	nested := "../testing/nested/notempty"

	// testing with an empty dir
	check.Nil(os.MkdirAll(empty, 0700))
	check.True(HasEmptyNestedFolders("../testing"))
	check.Nil(os.RemoveAll(empty))

	// checking cleanup
	check.False(HasEmptyNestedFolders("../testing"))

	// checking with useless nested dir
	check.Nil(os.MkdirAll(nested, 0700))
	check.Nil(CopyFile("../testing/1.opf", filepath.Join(nested, "file.file"), false))
	check.True(HasEmptyNestedFolders("../testing"))
	check.Nil(os.RemoveAll("../testing/nested"))
}

func TestGetForbiddenFiles(t *testing.T) {
	fmt.Println("+ Testing GetForbiddenFilesByExt & Allowed...")
	check := assert.New(t)

	// get all files
	forbidden := GetForbiddenFilesByExt("../testing", []string{".nope"})
	check.Equal(28, len(forbidden))
	// opf whitelisted (8 files)
	forbidden = GetForbiddenFilesByExt("../testing", []string{".nope", ".opf"})
	check.Equal(20, len(forbidden))
	// m3u whitelisted too (1 file)
	forbidden = GetForbiddenFilesByExt("../testing", []string{".nope", ".opf", ".m3u"})
	check.Equal(19, len(forbidden))

	// get no file
	allowed := GetAllowedFilesByExt("../testing", []string{".nope"})
	check.Equal(0, len(allowed))
	// opf whitelisted (8 files)
	allowed = GetAllowedFilesByExt("../testing", []string{".nope", ".opf"})
	check.Equal(8, len(allowed))
	// m3u whitelisted too (1 file)
	allowed = GetAllowedFilesByExt("../testing", []string{".nope", ".opf", ".m3u"})
	check.Equal(9, len(allowed))
}

func TestGetPrefix(t *testing.T) {
	fmt.Println("+ Testing GetFilesAndFoldersByPrefix + substring...")
	check := assert.New(t)

	// contents
	hasPrefix := GetFilesAndFoldersByPrefix("../testing", []string{"NOPE"})
	check.Equal(0, len(hasPrefix))
	hasPrefix = GetFilesAndFoldersByPrefix("../testing", []string{"NOPE", "9"})
	check.Equal(1, len(hasPrefix))
	hasPrefix = GetFilesAndFoldersByPrefix("../testing", []string{"NOPE", "9", "1"})
	check.Equal(4, len(hasPrefix))
	hasPrefix = GetFilesAndFoldersByPrefix("../testing", []string{"NOPE", "9", "1", "long"})
	check.Equal(5, len(hasPrefix))
	hasPrefix = GetFilesAndFoldersByPrefix("../testing", []string{"NOPE", "9", "1", "long", "this"})
	check.Equal(6, len(hasPrefix))
	fmt.Println(hasPrefix)
	// top level
	hasPrefix = GetFilesAndFoldersByPrefix("../testing", []string{"test"})
	check.Equal(1, len(hasPrefix))
	fmt.Println(hasPrefix)

	hasSubstrings := GetFilesAndFoldersBySubstring("../testing", []string{"g1"})
	check.Equal(2, len(hasSubstrings))
	hasSubstrings = GetFilesAndFoldersBySubstring("../testing", []string{"g1", "sti"})
	check.Equal(4, len(hasSubstrings))
	fmt.Println(hasSubstrings)
	hasSubstrings = GetFilesAndFoldersBySubstring("../testing", []string{"w"})
	check.Equal(3, len(hasSubstrings))
}

func TestGetSize(t *testing.T) {
	fmt.Println("+ Testing GetTotalSize...")
	check := assert.New(t)

	totalSize := GetTotalSize("../testing")
	fmt.Println(totalSize)
	// getting different results if compressed filesystem?
	if totalSize != 36357446 && totalSize != 36365605 {
		t.Errorf("wrong total size: %d", totalSize)
	}

	epubs, err := GetFilesByExt("../testing", ".epub")
	check.Nil(err)
	partialSize := GetPartialSize("../testing", epubs)
	check.Equal(1452155, partialSize)
	fmt.Println(partialSize)
}

func TestListSubdirs(t *testing.T) {
	fmt.Println("+ Testing GetTopLevelSubDirectories...")
	check := assert.New(t)

	subdirs, err := GetTopLevelSubDirectories("../testing")
	check.Nil(err)
	check.Equal([]string{"this_is_a_very_long_folder_name_for_testing_purposes"}, subdirs)
}
