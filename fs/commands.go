package fs

import (
	"errors"
	"fmt"
	"math"
	"os/exec"
	"runtime"
	"strings"
	"sync"
)

// ApplyCommands using goroutines. Can be applied to files.
func ApplyCommands(cmds []*exec.Cmd, title string, verbose bool) error {
	tasks := make(chan *exec.Cmd, len(cmds))
	var errs error
	var wg sync.WaitGroup
	var m sync.Mutex

	if verbose {
		fmt.Println(title)
	}

	numWorkers := int(math.Min(float64(runtime.NumCPU()), float64(8)))
	for i := 0; i < numWorkers; i++ {
		wg.Add(1)
		go func() {
			for cmd := range tasks {
				combined, err := cmd.CombinedOutput()
				if err != nil {
					err = errors.Join(errors.New(cmd.String()), errors.New(strings.TrimSpace(string(combined))), err)
				} else if verbose {
					fmt.Println(strings.TrimSpace(string(combined)))
				}
				m.Lock()
				errs = errors.Join(errs, err)
				m.Unlock()
			}
			wg.Done()
		}()
	}

	// generate some tasks
	for _, cmd := range cmds {
		tasks <- cmd
	}
	close(tasks)

	// wait for the workers to finish
	wg.Wait()
	return errs
}
