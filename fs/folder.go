package fs

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"unicode/utf8"

	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/logthis"
	"gitlab.com/catastrophic/assistance/strslice"
	"gitlab.com/catastrophic/assistance/ui"
)

// DirExists checks if a directory exists.
func DirExists(path string) bool {
	info, err := os.Stat(path)
	if err != nil {
		return false
	}
	if info.IsDir() {
		return true
	}
	return false
}

// DirIsEmpty checks if a directory is empty.
func DirIsEmpty(path string) (bool, error) {
	f, err := os.Open(path)
	if err != nil {
		return false, err
	}
	defer f.Close()

	_, err = f.Readdirnames(1)
	if err == io.EOF {
		return true, nil
	}
	// not empty or error
	return false, err
}

// CopyDir recursively copies a directory tree, attempting to preserve permissions.
// Source directory must exist, destination directory must *not* exist.
// Symlinks are ignored and skipped.
func CopyDir(src, dst string, useHardLinks bool) error {
	src = filepath.Clean(src)
	dst = filepath.Clean(dst)

	si, err := os.Stat(src)
	if err != nil {
		return err
	}
	if !si.IsDir() {
		return errors.New("source is not a directory")
	}
	_, err = os.Stat(dst)
	if err == nil {
		return errors.New("destination already exists")
	}
	if err != nil && !os.IsNotExist(err) {
		return err
	}
	if err = os.MkdirAll(dst, si.Mode()); err != nil {
		return err
	}
	entries, err := ioutil.ReadDir(src)
	if err != nil {
		return err
	}
	for _, entry := range entries {
		srcPath := filepath.Join(src, entry.Name())
		dstPath := filepath.Join(dst, entry.Name())
		if entry.IsDir() {
			if err = CopyDir(srcPath, dstPath, useHardLinks); err != nil {
				return err
			}
		} else {
			// Skip symlinks.
			if entry.Mode()&os.ModeSymlink != 0 {
				continue
			}
			if err = CopyFile(srcPath, dstPath, useHardLinks); err != nil {
				return err
			}
		}
	}
	return nil
}

// SanitizePath to have an acceptable path.
func SanitizePath(path string) string {
	// making sure the path is relative
	for strings.HasPrefix(path, "/") {
		path = path[1:]
	}
	// replacing internal / with an inoffensive utf8 variant
	path = strings.Replace(path, "/", "∕", -1)
	return path
}

// DeleteEmptyDirs recursively deletes empty folders in a given path.
func DeleteEmptyDirs(root string, whitelist []string) error {
	deletedDirectories := 0
	deletedDirectoriesThisTime := 0
	atLeastOnce := false

	// loops until all levels of empty directories are deleted
	for !atLeastOnce || deletedDirectoriesThisTime != 0 {
		atLeastOnce = true
		deletedDirectoriesThisTime = 0
		walkErr := filepath.Walk(root, func(path string, fileInfo os.FileInfo, walkError error) error {
			if path == root {
				// do not delete root, even if empty
				return nil
			}
			// when an directory has just been removed, Walk goes through it a second
			// time with an "fs does not exist" error
			if os.IsNotExist(walkError) {
				return nil
			}
			if fileInfo.IsDir() {
				isEmpty, err := DirIsEmpty(path)
				if err != nil {
					return nil
				}
				if isEmpty && !strslice.Contains(whitelist, path) {
					logthis.Info("Removing empty directory "+path, logthis.NORMAL)
					if err := os.Remove(path); err == nil {
						deletedDirectories++
						deletedDirectoriesThisTime++
					}
				}
			}
			return nil
		})
		if walkErr != nil {
			return walkErr
		}
	}
	logthis.Info(fmt.Sprintf("Removed %d empty directories.", deletedDirectories), logthis.VERBOSE)
	return nil
}

// MoveDir moves a directory to its new home.
func MoveDir(current, new string, doNothing, interactive bool) (bool, error) {
	if new == "" {
		return false, errors.New("no new path for this folder")
	}
	if new != current {
		// if different, move folder
		if !doNothing {
			// if interactive and not in simulation mode, must be accepted else we just move on.
			if interactive {
				if !ui.Accept("Move:\n  " + current + "\n->\n  " + new + "\n") {
					return false, nil
				}
			}
			newPathParent := filepath.Dir(new)
			if _, err := os.Stat(newPathParent); os.IsNotExist(err) {
				// newPathParent does not exist, creating
				err = os.MkdirAll(newPathParent, 0777)
				if err != nil {
					return false, err
				}
			}
			// move
			if err := os.Rename(current, new); err != nil {
				return false, err
			}
			return true, nil
		}
		// would have moved, but must do nothing, so here we pretend.
		return true, nil
	}
	return false, nil
}

// GetUniqueFolder in a target directory, append indices if folder already exists.
func GetUniqueFolder(basePath string) (string, error) {
	attempts := 1
	for attempts < 50 {
		suffix := ""
		if attempts > 1 {
			suffix = fmt.Sprintf("_%d", attempts)
		}
		candidate := fmt.Sprintf("%s%s", basePath, suffix)
		// if directory does not exist, return candidate
		if !DirExists(candidate) {
			return candidate, nil
		}
		// since candidate already exists, changing suffix and trying again.
		attempts++
	}
	return "", errors.New("could not find unique folder name")
}

// GetMaxPathLength inside a directory, including the name of the directory itself.
func GetMaxPathLength(dir string) int {
	if !DirExists(dir) {
		return 0
	}
	var maxLength int
	err := filepath.Walk(dir, func(path string, f os.FileInfo, err error) error {
		// get relative path
		rel, relErr := filepath.Rel(dir, path)
		if relErr != nil {
			return relErr
		}
		// add dir itself
		localPath := filepath.Join(filepath.Base(dir), rel)
		// checking length
		if utf8.RuneCountInString(localPath) > maxLength {
			maxLength = utf8.RuneCountInString(localPath)
		}
		return nil
	})
	if err != nil {
		return 0
	}
	return maxLength
}

// GetExceedinglyLongPaths returns paths longer that maxSize inside of a root directory.
func GetExceedinglyLongPaths(dir string, maxSize int) []string {
	if !DirExists(dir) {
		return nil
	}
	var longPaths []string
	err := filepath.Walk(dir, func(path string, f os.FileInfo, err error) error {
		// get relative path
		rel, relErr := filepath.Rel(dir, path)
		if relErr != nil {
			return relErr
		}
		// add dir itself
		localPath := filepath.Join(filepath.Base(dir), rel)
		// checking length
		if utf8.RuneCountInString(localPath) > maxSize {
			// keep the full path to return
			longPaths = append(longPaths, path)
		}
		return nil
	})
	if err != nil {
		return nil
	}
	return longPaths
}

func GetPathsWithNonStandardSpaces(dir string) []string {
	if !DirExists(dir) {
		return nil
	}
	var offendingPaths []string
	err := filepath.Walk(dir, func(path string, f os.FileInfo, err error) error {
		// get relative path
		rel, relErr := filepath.Rel(dir, path)
		if relErr != nil {
			return relErr
		}
		// add dir itself
		localPath := filepath.Join(filepath.Base(dir), rel)
		// checking for unicode almost-spaces
		if ContainsNonStandardSpace(localPath) {
			// keep the full path to return
			offendingPaths = append(offendingPaths, path)
		}
		return nil
	})
	if err != nil {
		return nil
	}
	return offendingPaths
}

// GetEmptyNestedFolders returns true if it contains empty dirs, or dirs that only contain other dirs.
func HasEmptyNestedFolders(dir string) bool {
	if !DirExists(dir) {
		return false
	}
	var hasEmptyOrUselesslyNestedFolders bool
	err := filepath.Walk(dir, func(path string, f os.FileInfo, err error) error {
		if hasEmptyOrUselesslyNestedFolders {
			return nil
		}
		if DirExists(path) {
			entries, err := ioutil.ReadDir(path)
			if err != nil {
				return err
			}
			if len(entries) == 0 || (len(entries) == 1 && entries[0].IsDir()) {
				hasEmptyOrUselesslyNestedFolders = true
				return nil
			}
		}
		return nil
	})
	if err != nil {
		return false
	}
	return hasEmptyOrUselesslyNestedFolders
}

// GetFilesByExt returns all files found in a directory with a specific extension.
func GetFilesByExt(directoryPath, extension string) ([]string, error) {
	var paths []string
	err := filepath.Walk(directoryPath, func(path string, f os.FileInfo, err error) error {
		if strings.ToLower(filepath.Ext(path)) == extension {
			paths = append(paths, path)
		}
		return nil
	})
	return paths, err
}

// GetForbiddenFilesByExt returns all files found in a directory with an extension that isn't whitelisted.
func GetForbiddenFilesByExt(directoryPath string, extensionWhitelist []string) []string {
	var paths []string
	err := filepath.Walk(directoryPath, func(path string, f os.FileInfo, err error) error {
		if FileExists(path) && !strslice.ContainsCaseInsensitive(extensionWhitelist, filepath.Ext(path)) {
			paths = append(paths, path)
		}
		return nil
	})
	if err != nil {
		// not the best default
		return []string{}
	}
	return paths
}

// GetAllowedFilesByExt returns all files found in a directory with an extension that is whitelisted.
func GetAllowedFilesByExt(directoryPath string, extensionWhitelist []string) []string {
	var paths []string
	err := filepath.Walk(directoryPath, func(path string, f os.FileInfo, err error) error {
		if FileExists(path) && strslice.ContainsCaseInsensitive(extensionWhitelist, filepath.Ext(path)) {
			paths = append(paths, path)
		}
		return nil
	})
	if err != nil {
		// not the best default
		return []string{}
	}
	return paths
}

// GetFilesAndFoldersByPrefix returns all files and folders found in a directory with specific prefixes.
func GetFilesAndFoldersByPrefix(directoryPath string, prefixWhitelist []string) []string {
	var paths []string
	err := filepath.Walk(directoryPath, func(path string, f os.FileInfo, err error) error {
		absPath, absErr := filepath.Abs(path)
		if absErr != nil {
			return absErr
		}
		for _, pref := range prefixWhitelist {
			if strings.HasPrefix(filepath.Base(absPath), pref) {
				paths = append(paths, path)
			}
		}
		return nil
	})
	if err != nil {
		// not the best default
		return []string{}
	}
	return paths
}

// GetFilesAndFoldersBySubstring returns all files and folders found in a directory with specific substrings.
func GetFilesAndFoldersBySubstring(directoryPath string, prefixWhitelist []string) []string {
	var paths []string
	err := filepath.Walk(directoryPath, func(path string, f os.FileInfo, err error) error {
		absPath, absErr := filepath.Abs(path)
		if absErr != nil {
			return absErr
		}
		for _, pref := range prefixWhitelist {
			if strings.Contains(filepath.Base(absPath), pref) {
				paths = append(paths, path)
			}
		}
		return nil
	})
	if err != nil {
		// not the best default
		return []string{}
	}
	strslice.RemoveDuplicates(&paths)
	return paths
}

// GetTotalSize returns the size of a folder.
func GetTotalSize(directoryPath string) int {
	var totalSize int
	err := filepath.Walk(directoryPath, func(path string, f os.FileInfo, err error) error {
		totalSize += int(f.Size())
		return nil
	})
	if err != nil {
		// not the best default
		return 0
	}
	return totalSize
}

// GetPartialSize returns the size of some of the contents in a folder.
func GetPartialSize(directoryPath string, whitelisted []string) int {
	var totalSize int
	err := filepath.Walk(directoryPath, func(path string, f os.FileInfo, err error) error {
		if strslice.Contains(whitelisted, path) {
			totalSize += int(f.Size())
		}
		return nil
	})
	if err != nil {
		// not the best default
		return 0
	}
	return totalSize
}

func GetTopLevelSubDirectories(root string) ([]string, error) {
	var subdirectories []string
	entries, err := ioutil.ReadDir(root)
	if err != nil {
		return []string{}, err
	}

	for _, entry := range entries {
		if entry.IsDir() {
			subdirectories = append(subdirectories, entry.Name())
		}
	}
	return subdirectories, nil
}
