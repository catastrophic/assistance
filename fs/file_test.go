package fs

import (
	"fmt"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const (
	testFilesDir = "../testing"
)

var epubs = []struct {
	filename           string
	expectedSha256     string
	expectedPartialMD5 string
	expectedMD5        string
}{
	{
		"pg16328.epub",
		"dc325b3aceb77d9f943425728c037fdcaf4af58e3abd771a8094f2424455cc03",
		"b0167664e2d9851a1bbd4cdffcec8c96",
		"af4ebfdce665ee9f19842fcf64b1e21b",
	},
	{
		"pg17989.epub",
		"acd2b8eba1b11456bacf11e690edf56bc57774053668644ef34f669138ebdd9a",
		"c74fc884c16b41074b88ad370cf15446",
		"8a1b864f02423719d0a2f2d1c7f693da",
	},
}

func TestFileCopy(t *testing.T) {
	fmt.Println("+ Testing CopyFile...")
	check := assert.New(t)
	// getting test directory
	testDir, err := os.Getwd()
	require.Nil(t, err, "Error getting current directory")
	testDir = filepath.Join(testDir, testFilesDir)

	for _, el := range epubs {
		// copy fs to _test
		origFilename := filepath.Join(testDir, el.filename)
		copyFilename := filepath.Join(testDir, el.filename+"_test")
		err := CopyFile(origFilename, copyFilename, false)
		require.Nil(t, err, "Could not copy file "+origFilename)
		// check copy exists
		check.True(FileExists(copyFilename), "Copy file %s should exist", copyFilename)
		// check if dst == src
		check.Nil(CopyFile(copyFilename, copyFilename, false))
		// check copy hash
		copyHash, err := CalculateSHA256(copyFilename)
		check.Nil(err, "Could not get hash from copy file %s", copyFilename)
		check.Equal(copyHash, el.expectedSha256, "Copy hash %s different from source %s", copyHash, el.expectedSha256)
		copyHash, err = CalculateMD5(copyFilename)
		check.Nil(err, "Could not get hash from copy file %s", copyFilename)
		check.Equal(copyHash, el.expectedMD5, "Copy hash %s different from source %s", copyHash, el.expectedMD5)
		// cleanup
		err = os.Remove(copyFilename)
		require.Nil(t, err, "Copy file %s could not be removed", copyFilename)
	}
	// check non regular file
	check.NotNil(CopyFile("/dev/random", filepath.Join(testDir, "random"), false))
}

func TestFileMD5(t *testing.T) {
	fmt.Println("+ Testing File MD5 partial...")
	check := assert.New(t)

	// getting test directory
	testDir, err := os.Getwd()
	require.Nil(t, err, "Error getting current directory")
	testDir = filepath.Join(testDir, testFilesDir)
	for _, el := range epubs {
		origFilename := filepath.Join(testDir, el.filename)
		check.Equal(el.expectedPartialMD5, CalculateMD5First4KB(origFilename))
	}
}

func TestFileDownload(t *testing.T) {
	fmt.Println("+ Testing FileDownload...")
	check := assert.New(t)

	// CC flac file from https://commons.wikimedia.org/wiki/Category:FLAC_files_of_music_by_Claude_Debussy
	flacFile := "Debussy_-_Pour_les_huit_doigts.flac"
	link := "https://upload.wikimedia.org/wikipedia/commons/2/22/" + flacFile
	check.Nil(DownloadFile(flacFile, link))
	check.FileExists(flacFile)

	// TODO check size

	check.Nil(os.Remove(flacFile))
}

func TestFileTimestamp(t *testing.T) {
	fmt.Println("+ Testing Timestamp...")
	check := assert.New(t)

	filename, err := GetUniqueTimestampedFilename("../testing", "log", "")
	check.Nil(err)
	fmt.Println(filename)
}
