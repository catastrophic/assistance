package fs

import (
	"fmt"
	"os"
	"os/exec"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCommands(t *testing.T) {
	fmt.Println("+ Testing commands...")
	check := assert.New(t)

	cmds := []*exec.Cmd{
		exec.Command("touch", "hi"),
		exec.Command("touch", "ho")}
	check.Nil(ApplyCommands(cmds, "touching", false))

	cmds = []*exec.Cmd{
		exec.Command("sleep", "1"),
		exec.Command("touch", "ho")}
	check.Nil(ApplyCommands(cmds, "touching again", true))

	defer func() {
		os.Remove("hi")
		os.Remove("ho")
	}()

	cmds = []*exec.Cmd{
		exec.Command("toul=l=lch", "hi"),
		exec.Command("touch", "ho")}
	err := ApplyCommands(cmds, "what", true)
	check.NotNil(err)
	fmt.Println(err.Error())
}
