// Package fs contains helper functions to deal with files and folders.
package fs

import (
	"crypto/md5"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/pkg/errors"
)

// AbsoluteFileExists checks if an absolute path is an existing file.
func AbsoluteFileExists(path string) (res bool) {
	info, err := os.Stat(path)
	if err != nil {
		return
	}
	if info.Mode().IsRegular() {
		return true
	}
	return
}

// FileExists checks if a path is valid.
func FileExists(path string) bool {
	absolutePath, err := filepath.Abs(path)
	if err != nil {
		return false
	}
	return AbsoluteFileExists(absolutePath)
}

// CopyFile copies a fs from src to dst. If src and dst files exist, and are
// the same, then return success. Copy the fs contents from src to dst.
func CopyFile(src, dst string, useHardLinks bool) (err error) {
	sfi, err := os.Stat(src)
	if err != nil {
		return
	}
	if !sfi.Mode().IsRegular() {
		// cannot copy non-regular files (e.g., directories,
		// symlinks, devices, etc.)
		return fmt.Errorf("CopyFile: non-regular source file %s (%q)", sfi.Name(), sfi.Mode().String())
	}
	dfi, err := os.Stat(dst)
	if err != nil {
		if !os.IsNotExist(err) {
			return
		}
	} else {
		if !(dfi.Mode().IsRegular()) {
			return fmt.Errorf("CopyFile: non-regular destination file %s (%q)", dfi.Name(), dfi.Mode().String())
		}
		if os.SameFile(sfi, dfi) {
			return
		}
	}
	if useHardLinks {
		return os.Link(src, dst)
	}
	return copyFileContents(src, dst)
}

// copyFileContents copies the contents of the src file to dst.
// The file will be created if it does not already exist.
// If the destination file exists, all of its contents will be replaced by the contents of the source file.
func copyFileContents(src, dst string) (err error) {
	in, err := os.Open(src)
	if err != nil {
		return
	}
	defer in.Close()
	out, err := os.Create(dst)
	if err != nil {
		return
	}
	defer func() {
		cerr := out.Close()
		if err == nil {
			err = cerr
		}
	}()
	if _, err = io.Copy(out, in); err != nil {
		return
	}
	err = out.Sync()
	return
}

// CalculateSHA256 calculates a file's current sh&256 hash.
func CalculateSHA256(filename string) (string, error) {
	file, err := os.Open(filename)
	if err != nil {
		return "", errors.Wrap(err, "cannot open file "+filename)
	}
	defer file.Close()

	hashBytes := sha256.New()
	_, err = io.Copy(hashBytes, file)
	if err != nil {
		return "", errors.Wrap(err, "cannot read file "+filename)
	}
	return hex.EncodeToString(hashBytes.Sum(nil)), err
}

// CalculateMD5 calculates a file's current md5 hash.
func CalculateMD5(filename string) (string, error) {
	file, err := os.Open(filename)
	if err != nil {
		return "", errors.Wrap(err, "cannot open file "+filename)
	}
	defer file.Close()

	hashBytes := md5.New()
	_, err = io.Copy(hashBytes, file)
	if err != nil {
		return "", errors.Wrap(err, "cannot read file "+filename)
	}
	return hex.EncodeToString(hashBytes.Sum(nil)), err
}

// CalculateMD5First4KB calculates the MD5 of the beginning of a file.
func CalculateMD5First4KB(path string) string {
	file, err := os.Open(path)
	if err != nil {
		fmt.Println("cannot open file ", path, ", error:", err.Error())
		return ""
	}
	defer file.Close()

	hash := md5.New()
	// only getting the hash of the first 4KB
	buff := make([]byte, 4096)

	n, err := file.Read(buff)
	if err != nil && err != io.EOF {
		fmt.Println("cannot read file ", path, ", error:", err.Error())
		return ""
	}
	_, err = hash.Write(buff[0:n])
	if err != nil {
		fmt.Println("cannot open file ", path, ", error:", err.Error())
		return ""
	}
	return hex.EncodeToString(hash.Sum(nil))
}

// DownloadFile will download a url to a local file.
// It will write as it downloads and not load the whole file into memory.
func DownloadFile(filepath string, url string) error {
	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}

	return nil
}

// GetUniqueTimestampedFilename for a given filename.
func GetUniqueTimestampedFilename(dir, filename, extension string) (string, error) {
	var uniqueFilename string
	// create dir if necessary
	if !DirExists(dir) {
		if err := os.MkdirAll(dir, 0700); err != nil {
			return "", err
		}
	}

	// Mon Jan 2 15:04:05 -0700 MST 2006
	currentTime := time.Now().Local()
	ext := filepath.Ext(filename)
	filenameBase := strings.TrimSuffix(filepath.Base(filename), ext)
	attempts := 0
	for attempts < 50 {
		suffix := ""
		if attempts > 0 {
			suffix = fmt.Sprintf("_%d", attempts)
		}
		candidate := fmt.Sprintf("%s - %s%s%s", currentTime.Format("2006-01-02 15h04m05"), filenameBase, suffix, extension)
		// while candidate already exists, change suffix.
		if !FileExists(filepath.Join(dir, candidate)) {
			return filepath.Join(dir, candidate), nil
		} else {
			attempts++
		}
	}
	return uniqueFilename, nil
}

var UnicodeNonStandardSpaces = []string{"\u00a0", "\u1680", "\u180e", "\u2000", "\u2001", "\u2002", "\u2003", "\u2004", "\u2005", "\u2006", "\u2007", "\u2008", "\u2009", "\u200a", "\u200b", "\u200c", "\u200d", "\u200e", "\u200f", "\u2063", "\u2028", "\u2029", "\u202a", "\u202b", "\u202c", "\u202d", "\u202e", "\u202f", "\u205f", "\u2060", "\u2061", "\u2062", "\u2063", "\u2064", "\u3000", "\ufeff"}

func ContainsNonStandardSpace(s string) bool {
	unicodeAlternateSpaces := strings.Join(UnicodeNonStandardSpaces, "")
	return strings.ContainsAny(s, unicodeAlternateSpaces)
}
