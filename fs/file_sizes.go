package fs

import (
	"fmt"
	"math"
)

type ByteSize float64

const (
	_            = iota // ignore first value by assigning to blank identifier
	KiB ByteSize = 1 << (10 * iota)
	MiB
	GiB
	TiB
)

func (b ByteSize) String() string {
	switch {
	case b >= TiB:
		return fmt.Sprintf("%.3fTiB", b/TiB)
	case b >= GiB:
		return fmt.Sprintf("%.3fGiB", b/GiB)
	case b >= MiB:
		return fmt.Sprintf("%.3fMiB", b/MiB)
	case b >= KiB:
		return fmt.Sprintf("%.3fKiB", b/KiB)
	}
	return fmt.Sprintf("%.3fB", b)
}

func FileSize(a uint64) string {
	return ByteSize(float64(a)).String()
}
func FileSizeDelta(a int64) string {
	return Sign(a) + ByteSize(math.Abs(float64(a))).String()
}

func Sign(a int64) string {
	if a >= 0 {
		return "+"
	}
	return "-"
}
