package fs

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestImage(t *testing.T) {
	fmt.Println("+ Testing Image...")
	check := assert.New(t)

	image := "../testing/image.jpg"
	imageResized := "../testing/image_resized.jpg"

	processedImage, resized, err := ResizeJPGIfNecessary(image, 1024)
	check.Nil(err)
	check.Equal(image, processedImage)
	check.False(resized)

	processedImage, resized, err = ResizeJPGIfNecessary(image, 300)
	check.Nil(err)
	check.Equal(imageResized, processedImage)
	check.True(resized)
	check.Nil(os.Remove(processedImage))

	processedImage, resized, err = ResizeJPGIfNecessary(image, 150)
	check.Nil(err)
	check.Equal(imageResized, processedImage)
	check.True(resized)
	check.Nil(os.Remove(processedImage))
}
